tjekmate
========

Utilities for runtime/dynamic type checking.

[[_TOC_]]

Motivation
----------

Type-checking boilerplate reduction and automatic generation of
human-readable error messages.

<table>

<tr><th> Before </th><th> After </th></tr>

<tr>
<td>

```python
from numbers import Integral
from typing import Optional


def foo(x: str, y, str, n: Optional[Integral] = None) -> str:
    if not isinstance(x, str):
        raise TypeError(f'x = {x!r}: expected a string')
    if not isinstance(y, str):
        raise TypeError(f'y = {y!r}: expected a string')
    if n is None:
        pass
    elif not (isinstance(n, Integral) and n > 0):
        raise TypeError(
            f'n = {n!r}: expected a positive integer or none'
        )
    ...
```

</td>
<td>

```python
from numbers import Integral
from typing import Optional

from tjekmate import (
    check, identical_to, is_instance, is_positive_integer
)


@check(
    x=is_instance(str),
    y=is_instance(str),
    n=(identical_to(None) | is_positive_integer),
)
def foo(x: str, y, str, n: Optional[Integral] = None) -> str:
    ...
```

</td>
</tr>

<tr>
<td>

```python
from typing import AnyStr, Optional


def join(*strings: AnyStr) -> AnyStr:
    if all(isinstance(s, str) for s in strings):
        sep = ''
    elif all(isinstance(s, bytes) for s in strings):
        sep = b''
    else:
        raise TypeError(
            f'*strings = {strings!r}: '
            'expected strings or `bytes` objects'
        )
    return sep.join(strings)
```

</td>
<td>

```python
from typing import AnyStr, Optional

from tjekmate import check, is_sequence


@check(
    strings=(is_sequence(item_type=str) | is_sequence(item_type=bytes)),
)
def join(*strings: AnyStr) -> AnyStr:
    if not strings:
        return ''
    return ('' if isinstance(strings[0], str) else b'').join(strings)
```

</td>
</tr>

<tr>
<td>

```python
import os
from typing import Literal

MODES = 'spam', 'ham', 'eggs'
EXTENSIONS = '.png', '.pdf', '.jpg', '.tiff'

def write_file(
    input_file: str,
    output_file: str,
    *,
    mode: Literal[MODES] = 'spam',
) -> None:
    if not (isinstance(input_file, str) and os.path.isfile(input_file)):
        raise TypeError(
            f'input_file = {input_file!r}: '
            'expected a string path to an existing file'
        )
    if not (
        isinstance(output_file, str) and
        output_file.endswith(EXTENSIONS)
    ):
        raise TypeError(
            f'output_file = {output_file!r}: '
            f'expected a string path ending in any of: {EXTENSIONS!r}'
        )
    if mode not in MODES:
        raise TypeError(f'mode = {mode!r}: expected any of: {MODES!r}')
    ...
```

</td>
<td>

```python
import os
from typing import Literal

from tjekmate import check, ends_with, is_in, is_instance

MODES = 'spam', 'ham', 'eggs'
EXTENSIONS = '.png', '.pdf', '.jpg', '.tiff'

@check(
    input_file=(is_instance(str) & os.path.isfile),
    output_file=ends_with(EXTENSIONS),
    mode=is_in(MODES),
)
def write_file(
    input_file: str,
    output_file: str,
    *,
    mode: Literal[MODES] = 'spam',
) -> None:
    ...
```

</td>
</tr>

</table>

API
---

### [Numeric-type checkers][numerics]

#### `is_finite_real()`

Check for finite (i.e. non-`nan`, non-`inf`) real numbers

#### `is_[non]{positive|negative}_{real|integer}()`

Checks for real numbers/integers to either side of the number line

### Container-type checkers

#### `is_collection()`

Check for collections with optional requirements on their items and
size:
```python
are_three_real_numbers = is_collection(length=3, item_type=(float, int))
are_three_real_numbers([1, 2, 3])  # True
are_three_real_numbers({4, 5, 6})  # True
are_three_real_numbers((7, 8))  # False
are_three_real_numbers('Foo')  # False
```

#### `is_sequence()`

Check for sequences, incl. `numpy` arrays
(see [`is_collection()`](#is-collection))

#### `is_mapping()`

Check for mappings with optional requirements on their keys and/or
values:
```python
is_foo_bar_to_bool_or_bool = is_mapping(
    key_checker=is_in(('foo', 'bar')),
    value_checker=is_strict_boolean,
    allow_value=True,
)
is_foo_bar_to_bool_or_bool({1: 2})  # False
is_foo_bar_to_bool_or_bool({'foo': True})  # True
is_foo_bar_to_bool_or_bool(
    {'foo': True, 'bar': False, 'baz': True},
)  # False
is_foo_bar_to_bool_or_bool(True)  # True
```

#### `is_keyword_mapping()`

Check for mappings which can be unpacked into e.g. the keyword arguments
to a function call

#### [`items_fulfill()`][tuples]

Build a check for tuple-likes whose items satisfy the respective checks:
```python
is_x_y_label = items_fulfill(float, float, str)
is_x_y_label([1., 1.5, 'Point'])  # True
is_x_y_label([1, None, 'Not a point'])  # False
```

### Class-relationship checkers

#### `is_instance()`

Check for instance–class relation

#### `is_subclass()`

Check for child–parent relation

### [Callable-type checkers][callables]

#### `takes_positional_args()`

Check for callables taking a given number of positional arguments

#### `takes_keyword_args()`

Check for callables taking a given collection of keyword arguments

### [Equivalence-relationship checkers][comparators]

#### `{eq|ne|gt|ge|lt|le}()`

Checks for `=`, `!=`, `>`, `>=`, `<`, and `<=`

### [Identity checkers][comparators]

#### `[not_]identical_to()`

Checks for object identity

### [Membership checkers][comparators]

#### `{is|not}_in()`

Checks for collection membership

#### `[not_]contains()`

Checks for collection content

### [Access-based checkers][accessors]

#### `check_{attr|item}()`

Checks for the results of attribute/item access

#### `check_call()`

Build a check for the results of method calls:
```python
has_three_ones = check_call((Sequence.count, eq(3)), 1)
has_three_ones([1, 2, 3])  # False
has_three_ones([1, 2, 1, 2, 1])  # True
```

### [String(-like) checkers][strings]

#### `is_keyword()`

Check for identifiers/keywords which can be used as e.g. attribute or
callable-parameter names

#### `is_{ascii|alphanumeric|alphabetic|digit|space}()`

Checks for character types (common to both `str` and `bytes`)

#### `is_{decimal|numeric|printable|identifier}()`

String-only character-type checks

#### `is_{lower|upper|title}case()`

Checks for character cases (common to both `str` and `bytes`)

#### `{starts|ends}_with()`

Checks for affixes (common to both `str` and `bytes`):
```python
ends_with_foo_bar = ends_with(('foo', 'bar'), case_sensitive=False)
ends_with_foo_bar('foo')  # True
ends_with_foo_bar('foobar')  # True
ends_with_foo_bar('FoObAr')  # True
ends_with_foo_bar('foobaz')  # False
```

#### `matches()`

Regex-based check (common to both `str` and `bytes`):
```python
is_a_something_b = matches('a.+b', full=True)
contains_a_sth_b = matches('a.+b', from_start=False)
is_a_something_b('a4b')  # True
is_a_something_b('a4B')  # False
is_a_something_b('AaaBb')  # False
is_a_something_b('ab')  # False
contains_a_sth_b('AaaBb')  # True
```

### Misc. checkers

#### `is_strict_boolean()`

Check for boolean types excluding integer aliases like 0 and 1

#### `is_numeric_format_spec()`

Check for format specifiers for numbers (e.g. `'.3f'`)

#### `is_type_checkable()`

Check for entities (e.g. types, tuples of types) which can be used in
type checks

### Decorator functions

#### [`@logical`][logical-ops]

Equip a checker callable with logical operations
(see [Logical operations](#logical-operations))

#### [`@type_checked()`][decorators]

Impose type checks, defaults, and/or parameter annotations on the
arguments passed to the callable:
```python
@type_checked(
    checkers=dict(x=is_instance(float), y=is_instance(float)),
    annotations={'x': float, 'y': float, 'return': float},
    defaults={'y': 2.},
)
def func(x, y):
    return x + y
```

This decorator has the following convenience wrappers:

##### `@check()`

Convenience function for applying checks,
e.g. the above is equivalent to
```python
@check(x=is_instance(float), y=is_instance(float))
def func(x: float, y: float = 2.) -> float:
    return x + y
```

##### `@annotate()`, `@add_annotations()`

Convenience function for adding/editing annotations,
e.g. the above is equivalent to
```python
@check(x=is_instance(float), y=is_instance(float))
@annotate(x=float, y=float)
def func(x, y=2.) -> float:
    return x + y
```
Note: `@add_annotations()` only adds annotation to a parameter when it
isn't already annotated (i.e. is non-overriding),
but `@annotate()` overrides annotations from the input function if
found.

##### `@set_defaults()`, `@add_defaults()`

Convenience function for adding/editing annotations,
e.g. the above is equivalent to
```python
@check(x=is_instance(float), y=is_instance(float))
@add_defaults(y=2.)
def func(x: float, y = float) -> float:
    return x + y
```
Note: `@add_defaults()` only adds default value to a parameter when it
doesn't already have one (i.e. is non-overriding),
but `@set_defaults()` overrides default values from the input function
if found.

Currying
--------

The following relations can be used to construct curried type checks:

```
is_coll(coll, **kwargs) == is_coll(**kwargs)(coll),
```
where `is_coll()` = `is_collection()`, `is_sequence()`,
or `is_mapping()`

```
is_instance(inst, cls) == is_instance(cls)(inst)
```

```
is_subclass(child, parent) == is_subclass(parent)(child)
```

```
takes_positional_args(func, n[, agg=...])
== takes_positional_args(n[, agg=...])(func)
```

```
takes_keyword_args(func, kwargs[, agg=...])
== takes_keyword_args(kwargs[, agg=...])(func)
```

```
equiv(x, y) == equiv(y)(x),
```
where `equiv()` = `eq()`, `ne()`, `gt()`, `ge()`, `lt()`, or `le()`

```
[not_]identical_to(x, y) == [not_]identical_to(y)(x)
```

```
{is|not}_in(x, y) == {is|not}_in(y)(x)
```

```
[not_]contains(x, y) == [not_]contains(y)(x)
```

```
check_{attr|item}(obj, acc[, checker=...])
== check_{attr|item}(acc[, checker=...])(obj)
```

```
str_check(string, type=...) == str_check(type=...)(string),
```
where `str_check`
= `is_ascii()`, `is_alphanumeric()`, `is_alphabetic()`, `is_digit()`,
`is_space()`,
`is_decimal()`, `is_numeric()`, `is_printable()`,
`is_identifier()`, `is_keyword()`,
`is_lowercase()`, `is_uppercase()`, or `is_titlecase()`

```
affix_check(string, affix[, **kwargs])
== affix_check(affix[, **kwargs])(string),
```
where `affix_check()` = `starts_with()` or `ends_with()`

```
matches(string, pattern[, **kwargs])
== matches(pattern[, **kwargs])(string)
```

Logical operations
------------------

All the final checker functions
(i.e. those with all other requisite arguments supplied via currying,
leaving only the argument to be checked)
are equipped with the following logical operations,
which return another checker function equivalent to doing the logical
operations on the return value(s) of the original checker(s):

### `NOT`

`~checker := (x: x -> not checker(x))`

### `AND`

`c1 & c2 := (x: x -> (c1(x) and c2(x)))`

### `XOR`

`c1 ^ c2 := (x: x -> (c1(x) ^ c2(x)))`

### `OR`

`c1 | c2 := (x: x -> (c1(x) or c2(x)))`

### Notes

- In the binary operations,
  the other operand can be an arbitrary checker callable
  (e.g. `math.isfinite()`),
  or even the string definition of the appropriate lambda function
  (e.g. `'lambda x: (x % 3) == 2'`).

- The AND and OR checks are by default lazy/short-circuited;
  to create eager (resp. lazy) versions of the current checker function,
  one can call its `.eager()` (resp. `.lazy()`) method.

- One can check the structure of composite checkers arising from logical
  operations on checkers by looking at its `.tree`;
  the truth table tabulating the output truth value as a function of the
  truth values of the component checkers can be generated by calling
  `.get_truth_table()`.

Examples
--------
Application of checks:

```python
from typing import Callable, Optional, Sequence, TypeVar
from tjekmate import check, takes_positional_args, is_sequence

T = TypeVar('T')
Addable = TypeVar('Addable', float, Sequence[T])

@check(func=takes_positional_args(2), seq=is_sequence)
def reduce(
    func: Callable[[T, T], T],
    seq: Sequence[T],
    init: Optional[T] = None,
) -> T:
    if init is not None:
        seq = (init, *seq)
    iterator = iter(seq)
    value = next(iterator)
    for next_value in iterator:
        value = func(value, next_value)
    return value

def add(x: Addable, y: Addable) -> Addable:
    return x + y

reduce(add, range(5))  # 10
reduce(add, ['a', 'b', 'c'])  # 'abc'
reduce(lambda x: x, range(5))
# Traceback (most recent call last):
#   ...
# TypeError: ...reduce(func=<function <lambda> at 0x...>):
#     expected type typing.Callable[[~T, ~T], ~T]
#     (type check <function takes_positional_args(2) at 0x...>
#     failed normally)
reduce(add, 1)
# Traceback (most recent call last):
#   ...
# TypeError: ...reduce(seq=1):
#     expected type typing.Sequence[~T]
#     (type check <function is_sequence at 0x...> failed normally)
```

Composition of checks:

```python
from tjekmate import is_instance, is_nonnegative_real, le
is_seq_of_strings = is_sequence(item_type=str)
is_between_0_and_1 = is_nonnegative_real & le(1)
is_in_unit_hypercube = is_sequence(item_checker=is_between_0_and_1)
my_checker = is_seq_of_strings | is_in_unit_hypercube
my_checker
# <function is_sequence(item_type=<class 'str'>) at 0x...>
# or <function
#    is_sequence(item_checker=<function is_nonnegative_real at 0x...>
#                             and <function le(1) at 0x...>)
#    at 0x...>
my_checker([.5, 1])  # Sequence of numbers between 0 and 1 -> True
my_checker(.1)  # False
my_checker('string')  # String -> sequence of strings -> True
my_checker(['string', .1])  # False
```

[accessors]: tjekmate/accessors.py
[callables]: tjekmate/callables.py
[comparators]: tjekmate/comparators.py
[decorators]: tjekmate/decorators.py
[logical-ops]: tjekmate/logical_ops.py
[numerics]: tjekmate/numerics.py
[strings]: tjekmate/strings.py
[tuples]: tjekmate/tuples.py
