"""
Checkers for (byte) strings
"""
import functools
import numbers
import operator
import re
import typing
try:  # python 3.8+
    from typing import TypedDict
except ImportError:
    from typing_extensions import TypedDict
try:  # python 3.11+
    from typing import Unpack
except ImportError:
    from typing_extensions import Unpack

from . import _helper_decorators
from .core import (
    is_collection, is_keyword as _is_keyword, is_mapping,
    is_instance, is_subclass,
)
from .comparators import identical_to, is_in, lt
from .decorators import check
from .logical_ops import logical
from .numerics import is_nonnegative_integer, is_strict_boolean

__all__ = (
    # Common methods
    'is_ascii',
    'is_alphanumeric', 'is_alphabetic', 'is_digit', 'is_space',
    'is_lowercase', 'is_uppercase', 'is_titlecase',
    'starts_with', 'ends_with',
    # String-only methods
    'is_decimal', 'is_numeric', 'is_printable', 'is_identifier', 'is_keyword',
    # Regex methods
    'matches',
)

REGEX_FLAG_ALIASES = {
    name.lower(): value
    for name, value in vars(re).items() if isinstance(value, re.RegexFlag)
}
REGEX_FLAG_VALUES = {
    flag.value for flag in re.RegexFlag.__members__.values()
    if flag.value  # Note: python3.11 added re.NOFLAG = 0
}
assert all(  # Check if the regex flags are binary
    brepr.startswith('0b1') and set(brepr[3:]) <= {'0'}
    for brepr in (bin(i) for i in REGEX_FLAG_VALUES)
)
assert max(REGEX_FLAG_VALUES) == 2 ** (len(REGEX_FLAG_VALUES) - 1)
REGEX_FLAG_LIMIT = max(REGEX_FLAG_VALUES) * 2

StringTypeOrTypes = typing.Union[
    typing.Type[str],
    typing.Type[bytes],
    typing.Tuple[
        typing.Union[typing.Type[str], typing.Type[bytes]], ...,
    ],
]
RegexFlag = typing.Union[
    re.RegexFlag,
    typing.TypeVar(
        f'NonnegIntBelow{REGEX_FLAG_LIMIT!r}', bound=numbers.Integral,
    ),
]
RegexFlagNamesToBools = TypedDict(
    'RegexFlagNamesToBools', dict.fromkeys(REGEX_FLAG_ALIASES, bool),
    total=False,
)

# ************************* Helper functions ************************* #

_is_regex_flag = (
    is_instance(re.RegexFlag) | (is_nonnegative_integer & lt(REGEX_FLAG_LIMIT))
)


def _check_affix(
    string: typing.Any,
    affix: typing.Union[typing.AnyStr, typing.Collection[typing.AnyStr]],
    type: typing.Union[StringTypeOrTypes, None],
    case_sensitive: bool,
    method_name: str,
) -> bool:
    """
    Helper function for `starts_with()` and `ends_with()`.
    """
    def reduce_case(string: typing.AnyStr) -> typing.AnyStr:
        try:
            return string.casefold()
        except AttributeError:
            return string.lower()

    if isinstance(affix, (str, bytes)):
        affix = affix,
    affix = tuple(set(affix))
    if not affix:  # No affixes -> no fulfilling affixes
        return False
    if type is None:
        type = str, bytes
    if not isinstance(string, type):
        return False
    if not case_sensitive:
        string = reduce_case(string)
        affix = tuple(reduce_case(a) for a in affix)
    try:
        return getattr(string, method_name)(affix)
    except TypeError:  # Type mismatch between `string` and `affix`
        return False


def _build_string_checker(
    checker_name: str,
    str_method_name: str,
    doc_example_values: typing.Sequence,
) -> logical:
    """
    Helper function for building string checkers.

    Example
    -------
    >>> import contextlib
    >>> import doctest
    >>> import io
    >>>
    >>> values = [1, None, 'Foo', 'FOO', 'bar', '']
    >>> expected_results = [False, False, False, True, False, False]
    >>> is_upper = _build_string_checker('is_upper', 'isupper', values)
    >>> # Check the docstrings
    >>> with io.StringIO() as error_stream:
    ...     with contextlib.redirect_stdout(error_stream):
    ...         doctest.run_docstring_examples(
    ...             is_upper, {is_upper.__name__: is_upper},
    ...         )
    ...     _ = error_stream.seek(0)
    ...     error_output = error_stream.read()
    ...     assert (not error_output), error_output
    >>> # Direct behavior checks
    >>> assert [is_upper(v) for v in values] == expected_results
    """
    assert callable(getattr(str, str_method_name, None))
    if callable(getattr(bytes, str_method_name, None)):
        string_name = '(byte) string'
        string_class_name = '{str|bytes}'
        string_classes_names = '`bytes` and/or `str`'
        subclass = bytes, str
        TypeHint = typing.Union[typing.Type[bytes], typing.Type[str]]
    else:
        string_name = 'string'
        string_class_name = 'str'
        string_classes_names = '`str`'
        subclass = str
        TypeHint = typing.Type[str]
    type_checker = is_collection(
        item_checker=is_subclass(subclass),
        allow_item=True,
        collection_type=tuple,
    )

    @logical
    @_helper_decorators.partial_sole_positional_arg
    @check(type=(identical_to(None) | type_checker))
    def checker(
        string: typing.Any,
        *,
        type: typing.Optional[
            typing.Union[TypeHint, typing.Tuple[TypeHint, ...]]
        ] = None,
    ) -> bool:
        if type is None:
            type = subclass
        if not isinstance(string, type):
            return False
        return getattr(string, str_method_name)()

    # Message the docs
    doc_example_values = list(doc_example_values)
    results = [
        isinstance(value, subclass) and getattr(value, str_method_name)()
        for value in doc_example_values
    ]
    string_indices = [
        i for i, value in enumerate(doc_example_values)
        if isinstance(value, str)
    ]
    checker.__name__ = checker.__qualname__ = checker_name
    checker.__doc__ = """
    Checker version of `{STR_CLS_NAME}.{STR_METHOD}()`.

    Parameters
    ----------
    string
        Supposed {STR_NAME}
    type
        Optional (tuple of) subclass(es) of {STR_CLS_NAMES} that
        `string` must be an instance of

    Return
    ------
    `string` passed
        Whether it is a {STR_NAME} passing the check and an instance
        of `type` (if supplied)
    else
        Checker with the supplied defaults

    Example
    -------
    >>> class MyStr(str):
    ...     pass
    ...
    >>> [
    ...     {CHECKER}(s)
    ...     for s in {VALUES!r}
    ... ]
    {RESULTS!r}
    >>> [
    ...     {CHECKER}(type=MyStr)(s)
    ...     for s in (
    ...         {STR_VALUES!r}
    ...         + [MyStr(s) for s in {STR_VALUES!r}]
    ...     )
    ... ]
    {MORE_RESULTS!r}
    """.format(
        STR_CLS_NAME=string_class_name,
        STR_METHOD=str_method_name,
        STR_NAME=string_name,
        STR_CLS_NAMES=string_classes_names,
        CHECKER=checker_name,
        VALUES=doc_example_values,
        RESULTS=results,
        STR_VALUES=[doc_example_values[i] for i in string_indices],
        MORE_RESULTS=(
            [False for _ in string_indices]
            + [results[i] for i in string_indices]
        ),
    )
    return checker


def _resolve_regex(
    pattern: typing.Union[typing.AnyStr, re.Pattern],
    flags: typing.Optional[RegexFlag] = None,
    **flags_by_name: Unpack[RegexFlagNamesToBools]
) -> re.Pattern:
    """
    Helper function for `matches` to resolve the regex.

    Examples
    --------
    >>> import re
    >>>
    >>> _resolve_regex('a.b', re.VERBOSE)
    re.compile('a.b', re.VERBOSE)
    >>> pat = _resolve_regex(re.compile('a.b', re.IGNORECASE), re.ASCII)
    >>> assert pat.pattern == 'a.b'
    >>> assert pat.flags == re.IGNORECASE | re.ASCII
    >>> pat2 = _resolve_regex(
    ...     re.compile('a.b', re.IGNORECASE | re.ASCII),
    ...     re.MULTILINE,
    ...     dotall=True,
    ...     ascii=False,
    ...     locale=False,  # No-op
    ... )
    >>> assert pat2.pattern == 'a.b'
    >>> assert pat2.flags == (
    ...     re.IGNORECASE | re.MULTILINE | re.DOTALL | re.UNICODE
    ... )
    """
    def join_flags(
        f: typing.Collection[re.RegexFlag],
        flags: typing.Optional[re.RegexFlag] = None,
    ) -> typing.Union[re.RegexFlag, None]:
        args = operator.or_, f,
        if flags is not None:
            args = (*args, flags)
        elif not f:
            return None
        return functools.reduce(*args)

    def decompose_flags(flags: RegexFlag) -> typing.Set[re.RegexFlag]:
        flags = getattr(flags, 'value', flags)
        return {re.RegexFlag(i) for i in REGEX_FLAG_VALUES if flags & i}

    add_flags, remove_flags = set(), set()
    for name, include in flags_by_name.items():
        (add_flags if include else remove_flags).add(REGEX_FLAG_ALIASES[name])
    # Collate additive flags
    if add_flags:
        flags = join_flags(add_flags, flags)
    if isinstance(pattern, re.Pattern):
        orig_flags, pattern = pattern.flags, pattern.pattern
        if orig_flags:
            # Notes:
            # - re.ASCII has to be explicitly handled since it is
            #   incompatible with re.UNICODE
            # - re.RegexFlag.__iter__() is only added 3.11, use
            #   something else
            orig_active_flags = decompose_flags(orig_flags)
            if flags is not None and flags & re.ASCII:
                orig_active_flags -= {re.UNICODE}
            flags = join_flags(orig_active_flags, flags)
    # Remove subtractive flags
    active_flags = set() if flags is None else decompose_flags(flags)
    flags = join_flags(active_flags - remove_flags, None)
    if flags is None:
        return re.compile(pattern)
    return re.compile(pattern, flags)


# ************************ Checker functions ************************* #


@logical
@_helper_decorators.partial_leading_args(min_nargs=1)
@check(
    prefix=(
        is_collection(item_type=str, allow_item=True)
        | is_collection(item_type=bytes, allow_item=True)
    ),
    type=(
        identical_to(None)
        | is_collection(
            item_checker=is_subclass((str, bytes)),
            allow_item=True,
            collection_type=tuple,
        )
    ),
    case_sensitive=is_strict_boolean,
)
def starts_with(
    string: typing.Any,
    prefix: typing.Union[typing.AnyStr, typing.Collection[typing.AnyStr]],
    *,
    type: typing.Optional[StringTypeOrTypes] = None,
    case_sensitive: bool = True,
) -> bool:
    """
    Checker version of `{str|bytes}.startswith()`.

    Call signatures
    ---------------
    (prefix[, **kwargs]) -> Callable[[object], is_string_with_prefix]
    (object, prefix[, **kwargs]) -> is_string_with_prefix

    Parameters
    ----------
    string
        Supposed (byte) string
    prefix
        (Byte) String prefix(es) with which to match `string`
    type
        Optional (tuple of) subclass(es) of `bytes` and/or `str` that
        `string` must be an instance of;
        default is to infer from `prefix`
    case_sensitive
        Whether to use case-sensitive matching;
        if false, use `str.casefold()` or `bytes.lower()` to regularize
        before the match

    Examples
    --------
    Standard use:

    >>> starts_with('1a', ('1', '2'))
    True
    >>> starts_with_foo = starts_with('foo')
    >>> [starts_with_foo(s) for s in [1, 'Foo', 'foo', 'foobar', 'f']]
    [False, False, True, True, False]
    >>> starts_with_foo_bar_bytes = starts_with((b'foo', b'bar'))
    >>> [
    ...     starts_with_foo_bar_bytes(s)
    ...     for s in ['foo', b'foo', b'barr', b'baz']
    ... ]
    [False, True, True, False]

    Case insensitivity:

    >>> starts_with_foo_ci = starts_with('foo', case_sensitive=False)
    >>> [starts_with_foo_ci(s) for s in ['Foo', 'foo', 'foobar', 'f']]
    [True, True, True, False]

    Subtype matching:

    >>> class MyStr(str):
    ...     pass
    ...
    >>> starts_with_foo_mystr = starts_with('foo', type=MyStr)
    >>> [
    ...     starts_with_foo_mystr(s)
    ...     for s in ['foo', 'bar', MyStr('foo'), MyStr('bar')]
    ... ]
    [False, False, True, False]
    """
    return _check_affix(string, prefix, type, case_sensitive, 'startswith')


@logical
@_helper_decorators.partial_leading_args(min_nargs=1)
@check(
    suffix=(
        is_collection(item_type=str, allow_item=True)
        | is_collection(item_type=bytes, allow_item=True)
    ),
    type=(
        identical_to(None)
        | is_collection(
            item_checker=is_subclass((str, bytes)),
            allow_item=True,
            collection_type=tuple,
        )
    ),
)
def ends_with(
    string: typing.Any,
    suffix: typing.Union[typing.AnyStr, typing.Collection[typing.AnyStr]],
    *,
    type: typing.Optional[StringTypeOrTypes] = None,
    case_sensitive: bool = True,
) -> bool:
    """
    Checker version of `{str|bytes}.endswith()`.

    Call signatures
    ---------------
    (suffix[, **kwargs]) -> Callable[[object], is_string_with_suffix]
    (object, suffix[, **kwargs]) -> is_string_with_suffix

    Parameters
    ----------
    string
        Supposed (byte) string
    suffix
        (Byte) String suffix(es) with which to match `string`
    type
        Optional (tuple of) subclass(es) of `bytes` and/or `str` that
        `string` must be an instance of;
        default is to infer from `suffix`
    case_sensitive
        Whether to use case-sensitive matching;
        if false, use `str.casefold()` or `bytes.lower()` to regularize
        before the match

    Examples
    --------
    Standard use:

    >>> ends_with('a1', ('1', '2'))
    True
    >>> ends_with_bar = ends_with('bar')
    >>> [ends_with_bar(s) for s in [1, 'Bar', 'bar', 'foobar', 'r']]
    [False, False, True, True, False]
    >>> ends_with_foo_bar_bytes = ends_with((b'foo', b'bar'))
    >>> [
    ...     ends_with_foo_bar_bytes(s)
    ...     for s in ['foo', b'foo', b'foobar', b'baz']
    ... ]
    [False, True, True, False]

    Case insensitivity:

    >>> ends_with_bar_ci = ends_with('bar', case_sensitive=False)
    >>> [ends_with_bar_ci(s) for s in ['Bar', 'bar', 'foobar', 'r']]
    [True, True, True, False]

    Subtype matching:

    >>> class MyStr(str):
    ...     pass
    ...
    >>> ends_with_foo_mystr = ends_with('foo', type=MyStr)
    >>> [
    ...     ends_with_foo_mystr(s)
    ...     for s in ['foo', 'bar', MyStr('foo'), MyStr('bar')]
    ... ]
    [False, False, True, False]
    """
    return _check_affix(string, suffix, type, case_sensitive, 'endswith')


@logical
@_helper_decorators.partial_leading_args(min_nargs=1)
@check(
    pattern=re.compile,
    flags=(identical_to(None) | _is_regex_flag),
    full=is_strict_boolean,
    from_start=is_strict_boolean,
    flags_by_name=is_mapping(
        key_checker=is_in(REGEX_FLAG_ALIASES), value_checker=is_strict_boolean,
    ),
)
def matches(
    string: typing.Any,
    pattern: typing.Union[typing.AnyStr, re.Pattern],
    *,
    flags: typing.Optional[RegexFlag] = None,
    full: bool = False,
    from_start: bool = True,
    **flags_by_name: Unpack[RegexFlagNamesToBools]
) -> bool:
    r"""
    Regex-matching checker.

    Call signatures
    ---------------
    (pattern[, **kwargs]) -> Callable[[object], is_string_matching_pat]
    (object, pattern[, **kwargs]) -> is_string_matching_pat

    Parameters
    ----------
    string
        (Byte) string
    pattern
        `re.Pattern` object or (byte) string compilable thereto
    flags
        `re.RegexFlag` object or integer evaluable thereto;
        if `pattern` is already a `re.Pattern`, its flags are combined
        with `flags` and `flags_by_name` with a logical OR
    full
        Whether to perform a full match (like `re.fullmatch()`);
        implies `from_start`
    from_start
        Whether to match from the start of the string (like
        `re.match()`) if true, or anywhere in it if false (like
        `re.search()`)
    **flags_by_name
        Optional additional flag names (lower-case; see the
        documentation for `re`) other than `flags` (and/or
        `pattern.flags`, where appropriate) to explicitly include or
        exclude;
        if true, said flag is included;
        if false, said flag is excluded if found (or it's a no-op if
        not)

    Examples
    --------
    >>> import re

    Basic use:

    >>> values = [None, 1, b'abc', 'abc', 'ABC']
    >>> [matches(s, 'a.c') for s in values]
    [False, False, False, True, False]
    >>> [matches('a.c')(s) for s in values]
    [False, False, False, True, False]
    >>> [matches(re.compile('a.c'))(s) for s in values]
    [False, False, False, True, False]

    Specifying regex flags:

    >>> [matches('a.c', ignorecase=True)(s) for s in values]
    [False, False, False, True, True]
    >>> [matches('a.c', flags=re.IGNORECASE)(s) for s in values]
    [False, False, False, True, True]
    >>> [
    ...     matches(
    ...         re.compile('a.c', re.IGNORECASE),
    ...         flags=re.DOTALL,  # Add re.DOTALL to pattern
    ...         ignorecase=False,  # Nullify re.IGNORECASE in pattern
    ...     )(s)
    ...     for s in [*values, 'a\nc']
    ... ]
    [False, False, False, True, False, True]

    Specifying matching scopes:

    >>> more_values = ['ab', 'abc', 'a', ' ac']
    >>> [  # ~ re.search()
    ...     matches(s, 'a.', from_start=False) for s in more_values
    ... ]
    [True, True, False, True]
    >>> [  # ~ re.match() (default)
    ...     matches(s, 'a.') for s in more_values
    ... ]
    [True, True, False, False]
    >>> [  # ~ re.fullmatch()
    ...     matches(s, 'a.', full=True) for s in more_values
    ... ]
    [True, False, False, False]
    """
    # Massage the pattern
    pattern = _resolve_regex(pattern, flags, **flags_by_name)
    # Choose matching strategy
    if full:
        matcher = pattern.fullmatch
    elif from_start:
        matcher = pattern.match
    else:
        matcher = pattern.search
    try:
        return matcher(string) is not None
    except TypeError:
        return False


@logical
@_helper_decorators.partial_sole_positional_arg
@check(
    type=(
        identical_to(None)
        | is_collection(
            item_checker=is_subclass(str),
            allow_item=True,
            collection_type=tuple,
        )
    ),
)
def is_keyword(
    string: typing.Any,
    *,
    type: typing.Optional[
        typing.Union[typing.Type[str], typing.Tuple[typing.Type[str], ...]]
    ] = None,
) -> bool:
    """
    Parameters
    ----------
    string
        Supposed string identifier
    type
        Optional (tuple of) subclass(es) of `str` that `string` must be
        an instance of

    Return
    ------
    `string` passed
        Whether it is a string identifier which can e.g. be an attribute
        or argument name, and is an instance of `type` (if supplied)
    else
        Checker with the supplied defaults

    Example
    -------
    >>> class MyStr(str):
    ...     pass
    ...
    >>> [
    ...     is_keyword(s)
    ...     for s in [None, 1, 'a b', '_', 'a', 'foo1', 'return']
    ... ]
    [False, False, False, True, True, True, False]
    >>> [
    ...     is_keyword(type=MyStr)(s)
    ...     for s in ['a b', MyStr('_'), 'a', 'foo1', MyStr('return')]
    ... ]
    [False, True, False, False, False]
    """
    # Note: this, being the more feature-full version of
    # `~.core.is_keyword()`, supersedes it in the top-level scope
    if not (type is None or isinstance(string, type)):
        return False
    return _is_keyword(string)


(
    # Common methods
    is_ascii, is_alphanumeric, is_alphabetic, is_digit, is_space,
    is_lowercase, is_uppercase, is_titlecase,
    # String-only methods
    is_decimal, is_numeric, is_printable, is_identifier,
) = (
    _build_string_checker(name, method_name, values)
    for name, method_name, values in [
        ('is_ascii', 'isascii', [None, '', 'abc', 'a\n1', b'1g', 'ä']),
        ('is_alphanumeric', 'isalnum', [None, b'', 'a2', b'b2', 'a b']),
        ('is_alphabetic', 'isalpha', [None, '', 'a', b'AB', 'a1']),
        ('is_digit', 'isdigit', [None, '1', '1.', 'a1', b'0']),
        ('is_space', 'isspace', [None, '', ' ', b'\t', '1 2']),
        ('is_lowercase', 'islower', [None, 'foo', 'FOO', 'Foo', b'f', b'1']),
        ('is_uppercase', 'isupper', [None, 'foo', 'FOO', 'Foo', b'F', b'1']),
        (
            'is_titlecase', 'istitle',
            [None, 'Foo Bar', 'foo bar', 'F B', 'FOO Bar'],
        ),
        ('is_decimal', 'isdecimal', [None, b'1', '12', '₂']),
        ('is_numeric', 'isnumeric', [None, b'1', '12', '₂']),
        ('is_printable', 'isprintable', [None, b'', '', '1\n2', '1\b2']),
        ('is_identifier', 'isidentifier', [None, b'a', 'a', 'a b', 'a_1']),
    ]
)
