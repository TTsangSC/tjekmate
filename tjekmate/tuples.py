"""
Checker for ordered tuples
"""
import numbers
import typing

from .callables import takes_positional_args
from .comparators import identical_to
from .core import is_instance, is_sequence, is_type_checkable
from .decorators import type_checked
from .logical_ops import logical
from .numerics import is_nonnegative_integer

__all__ = ('items_fulfill',)

TC_SUBMODULE = (lambda: None).__module__.rpartition('.')[0]


@logical
@type_checked(
    checkers=dict(
        checkers=is_sequence(
            item_checker=(takes_positional_args(1) | is_type_checkable),
        ),
        min_nitems=(identical_to(None) | is_nonnegative_integer),
    ),
)
def items_fulfill(
    *checkers: typing.Union[
        typing.Callable[[typing.Any], bool],
        type,
        typing.Tuple[type, ...],
    ],
    min_nitems: typing.Optional[numbers.Integral] = None,
) -> typing.Callable[[typing.Any], bool]:
    """
    Build a checker checking whether its argument is a tuple-like with
    the requisite items.

    Parameters
    ----------
    *checkers
        If checker functions, the checked argument is required to have
        that many items, and for each of its items to pass the
        respective checker (i.e. checker returns truey);
        if types and/or tuples or types, the checked argument is
        required to have that many items and for each of its items to
        pass the respective instance check
    min_nitems
        If a non-negative number (`<= `len(checkers)``), allow shorter
        tuple-like objects with at least that many items to pass the
        check, as long as each item passes its corresponding check;
        default is to require a tuple-like of exactly `len(checkers)`-
        items long

    Return
    ------
    Single-argument checker function

    Notes
    -----
    The argument to the returned checker function is not strictly
    required to be a tuple, only a sequence of the requisite dimensions
    and items.

    Examples
    --------
    >>> import numbers
    >>> import typing
    >>>
    >>> MyTuple = typing.Tuple[
    ...     numbers.Integral,
    ...     numbers.Integral,
    ...     typing.Union[numbers.Integral, str],
    ... ]
    >>> objs = [
    ...     (1,),  # Too short
    ...     (1, 2),  # Too short (for strict equivalence)
    ...     (1, 2, 3, 4),  # Too long
    ...     (1, 2, 3),
    ...     [1, 2, 3],  # Not a tuple, but that's ok
    ...     (1, 2, 'foo'),
    ...     (1, 'foo', 3),  # Wrong type at items[1]
    ...     (1, 'foo'),  # Wrong items[1], too short for strict equiv.
    ...     1,  # Not even a sequence
    ... ]

    Using instance checks:

    >>> is_my_tuple = items_fulfill(
    ...     numbers.Integral,
    ...     numbers.Integral,
    ...     (numbers.Integral, str),
    ... )
    >>> [is_my_tuple(obj) for obj in objs]
    [False, False, False, True, True, True, False, False, False]
    >>> is_my_tuple_last_optional = items_fulfill(
    ...     numbers.Integral,
    ...     numbers.Integral,
    ...     (numbers.Integral, str),
    ...     min_nitems=2,
    ... )
    >>> [is_my_tuple_last_optional(obj) for obj in objs]
    [False, True, False, True, True, True, False, False, False]

    Using checker functions:

    >>> def is_in_unit_interval(x: typing.Any) -> bool:
    ...     if not isinstance(x, numbers.Real):
    ...         return False
    ...     return 0 <= x <= 1
    ...
    >>> is_in_unit_square_or_cube = items_fulfill(
    ...     *((is_in_unit_interval,) * 3), min_nitems=2,
    ... )
    >>> [
    ...     is_in_unit_square_or_cube(obj) for obj in [
    ...         .5,  # Not a sequence
    ...         (.5,),  # Too short
    ...         (.5, .5),
    ...         (.5, .5, .5),
    ...         (.5, .5, .5, .5),  # Too long
    ...         (-1, 1),  # Out of range
    ...         ('foo', 'bar', 'baz'),  # Wrong types
    ...     ]
    ... ]
    [False, False, True, True, False, False, False]
    """
    def composite_checker(obj: typing.Any) -> bool:
        if not is_sequence(obj):
            return False
        if not (min_nitems <= len(obj) <= max_nitems):
            return False
        return all(check(item) for item, check in zip(obj, checkers))

    checkers = [
        is_instance(checker) if is_type_checkable(checker) else checker
        for checker in checkers
    ]
    nchecks = len(checkers)
    if min_nitems is None:
        min_nitems = max_nitems = nchecks
    elif min_nitems > nchecks:
        raise ValueError(
            f'*checkers = {checkers!r}, min_nitems = {min_nitems!r}: '
            f'expected the latter to be <= `len(checkers) = {nchecks}`'
        )
    else:
        max_nitems = nchecks
    return composite_checker
