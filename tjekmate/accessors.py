"""
Accessor-based checks.
"""
import functools
import operator
import typing
try:
    from typing import Concatenate, ParamSpec
except ImportError:
    from typing_extensions import Concatenate, ParamSpec

from ._helper_decorators import partial_leading_args
from .callables import takes_positional_args, takes_keyword_args
from .comparators import identical_to
from .core import is_keyword, is_instance, is_sequence
from .decorators import check
from .logical_ops import logical
from .tuples import items_fulfill

# Note: like how the comparison-based checkers in `~.comparators` are
# parallels of the comparators in `operator`, these functions are
# analogous to `operator.itemgetter()`, `operator.attrgetter()`, and
# `operator.methodcaller()`.

__all__ = (
    'check_item', 'check_attr', 'check_call',
)

PS = ParamSpec('PS')
T = typing.TypeVar('T')
Keyword = typing.TypeVar('Keyword', bound=str)
PeriodSeparatedKeywords = typing.TypeVar('PeriodSeparatedKeywords', bound=str)
Operand = typing.TypeVar('Operand')
Checker = typing.Callable[[T], bool]
Method = typing.Callable[Concatenate[Operand, PS], T]
MethodSpec = typing.Union[Keyword, Method]

_looks_like_method = is_keyword | callable


@logical
@check(
    method=(
        _looks_like_method
        | items_fulfill(_looks_like_method, takes_positional_args(1))
    ),
)
def check_call(
    method: typing.Union[MethodSpec, typing.Tuple[MethodSpec, Checker]],
    *args: PS.args,
    **kwargs: PS.kwargs,
) -> typing.Callable[[typing.Any], bool]:
    """
    Build a checker checking whether calling a method on its argument
    results in some value.

    Parameters
    ----------
    method = keyword
        Check the output of the bound method `getattr(argument, method)`
    method() = callable
        Check the output of the bound method
        `types.MethodType(method, argument)`
    method = (keyword or callable), checker()
        Check the return value of said bound method with `checker()`,
        instead of default behavior of only consulting its truthiness
    *args, **kwargs
        Passed to said bound method

    Return
    ------
    Single-argument checker function

    Notes
    -----
    - If `method()` is a callable (or a tuple of two callables), it is
      first vetted for whether it will be compatible with the call
      signature `method(argument, *args, **kwargs)`;
      if not, a `TypeError` is raised.

    - The returned checker function returns `False` if:
      - The requested bound method (if `method` is a keyword) doesn't
        exist (attribute lookup results in an `AttributeError`).
      - The bound-method call results in any exception.
      - Calling `checker()` on the bound-method call result results in
        an exception, or if a falsy value is returned.

    - It may still raise exceptions if:
      - Method lookup on the argument results in a non-`AttributeError`
        exception.
      - `method()` (or `checker()` if `method = (method(), checker())`)
        returns a value not castable to a boolean.

    Examples
    --------
    >>> import operator
    >>> from functools import partial
    >>> from typing import Callable, Iterable, Optional, TypeVar
    >>>
    >>> class Person:
    ...     name: str
    ...
    ...     def __init__(self, name: str) -> None:
    ...         self.name = name
    ...
    ...     def get_name(self) -> str:
    ...         return self.name
    ...
    >>> T = TypeVar('T')
    >>> Sortable = TypeVar('Sortable')
    >>>
    >>> def get_age(person: Person) -> int:
    ...     return ages[person.name]
    ...
    >>> def get_minimum(
    ...     items: Iterable[T],
    ...     key: Optional[Callable[[T], Sortable]] = None,
    ... ) -> T:  # Wrapper around `min()` providing a signature
    ...     return min(items, key=key)
    ...
    >>> people = tom, mary, null = [
    ...     Person(name) for name in ('tom', 'Mary', '')
    ... ]
    >>> ages = {'tom': 18, 'Mary': 25, '': 0}

    Normal method call:

    >>> check_name = check_call('get_name')
    >>> [check_name(person) for person in people]
    [True, True, False]

    External method call:

    >>> check_age = check_call(get_age)
    >>> [  # Last item is false since `get_age(None)` is an error
    ...     check_age(person) for person in (*people, None)
    ... ]
    [True, True, False, False]

    Method calls with extra arguments:

    >>> contains_three = check_call(operator.contains, 3)
    >>> [
    ...     contains_three(obj) for obj in [
    ...         None,  # Not a sequence -> error -> False
    ...         (1,),  # Doesn't contain `3` -> False
    ...         [3, 2, 1, 2, 3],  # contains two `3`s -> True
    ...     ]
    ... ]
    [False, False, True]

    Method calls with value checkers for the result:

    >>> is_even = check_call(  # x -> (x % 2) != 1
    ...     (operator.mod, check_call(operator.ne, 1)), 2,
    ... )
    >>> [
    ...     is_even(obj) for obj in [
    ...         '1',  # Not a number -> error -> False
    ...         *range(5),
    ...     ]
    ... ]
    [False, True, False, True, False, True]

    >>> min_is_bar = check_call(
    ...     (get_minimum, check_call(operator.eq, 'bar')),
    ...     key=partial(str.casefold),
    ... )
    >>> min_is_bar(  # BAR == bar < foo -> min: BAR
    ...     ['foo', 'BAR', 'bar'],
    ... )
    False
    >>> min_is_bar(  # bar == Bar < BAZ < FOO = Foo -> min: bar
    ...     ['FOO', 'BAZ', 'bar', 'Foo', 'Bar'],
    ... )
    True
    """
    if _looks_like_method(method):
        checker = None
    else:
        method, checker = method
        checker: Checker
    if callable(method):
        # Pre-vet the `method()` where possible
        method_checker = takes_positional_args(len(args) + 1)
        if kwargs:
            method_checker |= takes_keyword_args(set(kwargs), agg=all)
        if not method_checker(method):
            raise TypeError(
                f'method = {method!r}: failed check {method_checker!r}'
            )
        # (partial(partial, m))(a) = partial(m, a)
        bound_method_getter = functools.partial(functools.partial, method)
    else:
        bound_method_getter = operator.attrgetter(method)
    bound_method_getter: typing.Callable[[Operand], Method]

    def check_result_of_call(obj: Operand) -> bool:
        try:
            bound_method = bound_method_getter(obj)
        except AttributeError:
            return False
        try:
            result = bound_method(*args, **kwargs)
        except Exception:
            return False
        if checker is not None:
            try:
                result = checker(result)
            except Exception:
                return False
        return bool(result)

    return check_result_of_call


@logical
@partial_leading_args(min_nargs=1)
@check(checker=(identical_to(None) | takes_positional_args(1)))
def check_item(
    obj: Operand,
    key: PeriodSeparatedKeywords,
    *,
    checker: typing.Optional[Checker] = None,
) -> bool:
    """
    Check whether item access on an object results in some value.

    Call signatures
    ---------------
    (obj, key[, checker=checker]) -> bool
    (key[, checker=checker]) -> Callable[[obj], bool]

    Parameters
    ----------
    obj
        The object on which item access is to be done
    key
        Item-access key
    checker
        Optional checker taking the item and returning a truthy/falsy
        value

    Return
    ------
    `obj` not provided
        Checker `(obj) -> bool`
    `TypeError`, `IndexError`, or `KeyError` occurs during item access
        False
    Exception occurs during item-checking with `checker()`
        False
    Else
        Whether the item (or `checker(item)` if `checker()` provided) is
        truthy

    Notes
    -----
    Exception can still be raised if:
    - Item lookup on the argument results in an exception that is not a
      `TypeError`, `IndexError`, or a `KeyError`.
    - `item` (or `checker(item)` if `checker()` provided) is not
      castable to a boolean.

    Examples
    --------
    >>> from operator import not_
    >>>
    >>> first_item_is_truthy = check_item(0)
    >>> first_item_is_falsy = check_item(0, checker=not_)
    >>> items = [
    ...     None,  # TypeError
    ...     [],  # IndexError
    ...     {1: 1},  # KeyError
    ...     [0],  # -> 0
    ...     'abc',  # -> 'a'
    ... ]
    >>> [first_item_is_truthy(obj) for obj in items]
    [False, False, False, False, True]
    >>> [first_item_is_falsy(obj) for obj in items]
    [False, False, False, True, False]
    """
    item_getter: typing.Callable[[Operand], T] = operator.itemgetter(key)
    try:
        result = item_getter(obj)
    except (TypeError, IndexError, KeyError):
        return False
    if checker is not None:
        try:
            result = checker(result)
        except Exception:
            return False
    return bool(result)


@logical
@partial_leading_args(min_nargs=1)
@check(
    attr=(
        is_instance(str)
        & check_call(len)
        & check_call(('split', is_sequence(item_checker=is_keyword)), '.')
    ),
    checker=(identical_to(None) | takes_positional_args(1)),
)
def check_attr(
    obj: Operand,
    attr: PeriodSeparatedKeywords,
    *,
    checker: typing.Optional[Checker] = None,
) -> bool:
    """
    Check whether attribute access on an object results in some value.

    Call signatures
    ---------------
    (obj, attr[, checker=checker]) -> bool
    (attr[, checker=checker]) -> Callable[[obj], bool]

    Parameters
    ----------
    obj
        The object on which attribute access is to be done
    attr
        String key of period-separated attribute names with which to do
        (chained) attribute access
    checker
        Optional checker taking the attribute and returning a truthy/
        falsy value

    Return
    ------
    `obj` not provided
        Checker `(obj) -> bool`
    `AttributeError` occurs during attribute access
        False
    Exception occurs during attribute-checking with `checker()`
        False
    Else
        Whether the attribute (or `checker(attribute)` if `checker()`
        provided) is truthy

    Notes
    -----
    Exception can still be raised if:
    - Attribute lookup on the argument results in a non-`AttributeError`
      exception.
    - `attribute` (or `checker(attribute)` if `checker()` provided) is
      not castable to a boolean.

    Examples
    --------
    >>> import operator
    >>> from functools import partial
    >>> from numbers import Complex
    >>>
    >>> class NumberHolder:
    ...     x: Complex
    ...
    ...     def __init__(self, x: Complex) -> None:
    ...         self.x = x
    ...
    >>> non_zero = check_attr('x')
    >>> is_imag = check_attr(
    ...     'x.real', checker=partial(operator.eq, 0),
    ... )
    >>> [
    ...     non_zero(NumberHolder(obj)) for obj in [
    ...         None,  # AttributeError -> False
    ...         0,  # -> False
    ...         4,  # -> True
    ...         2+3j,  # -> True
    ...     ]
    ... ]
    [False, False, True, True]
    >>> [
    ...     is_imag(NumberHolder(obj)) for obj in [
    ...         None,  # AttributeError -> False
    ...         0,  # .real = 0 -> True
    ...         4,  # .real = 4 -> False
    ...         5j,  # .real = 0 -> True
    ...     ]
    ... ]
    [False, True, False, True]
    """
    attr_getter: typing.Callable[[Operand], T] = operator.attrgetter(attr)
    try:
        result = attr_getter(obj)
    except AttributeError:
        return False
    if checker is not None:
        try:
            result = checker(result)
        except Exception:
            return False
    return bool(result)
