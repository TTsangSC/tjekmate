"""
Checks for callables
"""
import inspect
import numbers
import typing
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

from ._helper_decorators import partial_leading_args
from .comparators import is_in
from .core import is_collection, is_keyword
from .decorators import type_checked
from .logical_ops import logical, UNSUPPORTED_CALLABLES_VALUE_ERROR_MESSAGES
from .numerics import is_nonnegative_integer

__all__ = ('takes_positional_args', 'takes_keyword_args')

Keyword = typing.TypeVar('Keyword', bound=str)


@logical
@partial_leading_args(placeholder=None, min_nargs=1)
@type_checked(
    checkers=dict(
        n=is_collection(item_checker=is_nonnegative_integer, allow_item=True),
        agg=is_in(('any', 'all', any, all)),
    ),
)
def takes_positional_args(
    obj: typing.Any,
    n: typing.Union[numbers.Integral, typing.Collection[numbers.Integral]],
    *,
    agg: Literal['any', 'all', any, all] = any,
) -> bool:
    """
    Check if the object is a callable which can be called with `n`-many
    positional arguments.

    Parameters
    ----------
    obj()
        Presumed callable to be type-checked
    n
        Non-negative number(s) of positional arguments `obj` should be
        able to take
    agg
        If there are multiple values for `n`, whether `obj` is to be
        compatible with any or all of them

    Return
    ------
    `obj` not provided (i.e. there's only one positional argument)
        Type-checking function taking one argument
    Else
        Result of the type check

    Examples
    --------
    Functions:

    >>> funcs = [
    ...     None,
    ...     lambda a: None,
    ...     lambda a, b: None,
    ...     lambda a, b=1: None,
    ...     lambda *args, **kwargs: None,
    ...     lambda *, c=None: None,
    ... ]
    >>> is_single_arg_callable = takes_positional_args(1)
    >>> [is_single_arg_callable(obj) for obj in funcs]
    [False, True, False, True, True, False]
    >>> [takes_positional_args(obj, n=2) for obj in funcs]
    [False, False, True, True, True, False]
    >>> [takes_positional_args(obj, n=[1, 2]) for obj in funcs]
    [False, True, True, True, True, False]
    >>> [takes_positional_args(obj, n=[1, 2], agg=all) for obj in funcs]
    [False, False, False, True, True, False]

    Methods:

    >>> class Foo:
    ...     def bar(self, x: int) -> None:
    ...         ...
    ...
    >>> class Bar:
    ...     def baz(self, x: int, y: int) -> None:
    ...         ...
    ...
    ...     def __call__(self, x: int, y: int, z: int = 1) -> None:
    ...         ...
    ...
    >>> methods = [
    ...     Foo.bar,
    ...     Foo().bar,
    ...     Bar.baz,
    ...     Bar().baz,
    ...     Bar.__call__,
    ...     Bar().__call__,
    ...     Bar(),
    ... ]
    >>> [takes_positional_args(obj, 2) for obj in methods]
    [True, False, False, True, False, True, True]
    """
    if not callable(obj):
        return False
    try:
        n = set(n)
    except TypeError:  # Integer n
        n = {n}
    agg = dict(any=any, all=all).get(agg, agg)

    inf = float('inf')
    try:
        sig = inspect.signature(obj)
    except ValueError as e:  # no signature found for builtin ...
        msg, *_ = e.args
        for pattern in UNSUPPORTED_CALLABLES_VALUE_ERROR_MESSAGES:
            if pattern in msg:
                return False
        raise
    min_nargs: int = 0
    max_nargs: typing.Union[int, Literal[inf]] = 0
    for param in sig.parameters.values():
        if 'POSITIONAL' not in param.kind.name:
            continue
        if param.kind is param.VAR_POSITIONAL:
            max_nargs = inf
            continue
        max_nargs += 1
        if param.default is param.empty:
            min_nargs += 1
    return agg(min_nargs <= nn <= max_nargs for nn in n)


@logical
@partial_leading_args(placeholder=None, min_nargs=1)
@type_checked(
    checkers=dict(
        kwargs=is_collection(item_checker=is_keyword, allow_item=True),
    ),
)
def takes_keyword_args(
    obj: typing.Any,
    kwargs: typing.Union[Keyword, typing.Collection[Keyword]],
    *,
    agg: Literal['any', 'all', any, all] = all,
) -> bool:
    """
    Check if the object is a callable which can be called with the given
    keyword arguments.

    Parameters
    ----------
    obj()
        Presumed callable to be type-checked
    kwargs
        Keyword argument(s) `obj` should be able to take
    agg
        If there are multiple values for `kwargs`, whether `obj` is to
        be compatible with any or all of them

    Return
    ------
    `obj` not provided (i.e. there's only one positional argument)
        Type-checking function taking one argument
    Else
        Result of the type check

    Examples
    --------
    >>> takes_foo = takes_keyword_args('foo')
    >>> takes_bar = takes_keyword_args('bar')
    >>> takes_foo_and_bar = takes_keyword_args(['foo', 'bar'])
    >>> takes_foo_or_bar = takes_keyword_args(['foo', 'bar'], agg=any)
    >>> funcs = [
    ...     None,
    ...     lambda foo: None,
    ...     lambda foo, bar: None,
    ...     lambda bar: None,
    ...     lambda *args, foo=None: None,
    ...     lambda *args, foo=None, bar=None: None,
    ...     lambda *args, **kwargs: None,
    ... ]
    >>> [takes_foo(func) for func in funcs]
    [False, True, True, False, True, True, True]
    >>> [takes_bar(func) for func in funcs]
    [False, False, True, True, False, True, True]
    >>> [takes_foo_and_bar(func) for func in funcs]
    [False, False, True, False, False, True, True]
    >>> [takes_foo_or_bar(func) for func in funcs]
    [False, True, True, True, True, True, True]

    Handling of positional-only arguments (python 3.8+ only):

    >>> import sys
    >>> import pytest
    >>>
    >>> if sys.version_info >= (3, 8):
    ...     results = [
    ...         takes_keyword_args(eval(func_def), ['foo', 'bar'])
    ...         for func_def in [
    ...             'lambda foo, /, bar: None',
    ...             'lambda foo, /, bar, **kwargs: None',
    ...         ]
    ...     ]
    ...     assert results == [False, True]
    ... else:
    ...     pytest.skip('Positional-onlys not available pre-3.8')
    """
    try:
        params = inspect.signature(obj).parameters
    except TypeError:  # Not callable
        return False
    except ValueError as e:  # no signature found for builtin ...
        msg, *_ = e.args
        for pattern in UNSUPPORTED_CALLABLES_VALUE_ERROR_MESSAGES:
            if pattern in msg:
                return False
        raise
    if any(
        param.kind == param.VAR_KEYWORD for param in params.values()
    ):
        return True
    if is_keyword(kwargs):
        kwargs = {kwargs}
    agg = dict(any=any, all=all).get(agg, agg)
    args_kinds = [
        getattr(params.get(kwarg), 'kind', None) for kwarg in kwargs
    ]
    kwargs_kinds = (
        inspect.Parameter.POSITIONAL_OR_KEYWORD, inspect.Parameter.KEYWORD_ONLY,
    )
    return agg(arg_kind in kwargs_kinds for arg_kind in args_kinds)
