"""
Utilities for runtime/dynamic type checking.

API
---
Numeric-type checkers:

is_finite_real()
    Check for finite (i.e. non-`nan`, non-`inf`) real numbers

is_[non]{positive|negative}_{real|integer}()
    Checks for real numbers/integers to either side of the number line

Container-type checkers:

is_collection()
    Check for collections with optional requirements on their items and
    size

is_sequence()
    Check for sequences, incl. `numpy` arrays

is_mapping()
    Check for mappings with optional requirements on their keys and/or
    values

is_keyword_mapping()
    Check for mappings which can be unpacked into e.g. the keyword
    arguments to a function call

items_fulfill()
    Build a check for tuple-likes whose items satisfy the respective
    checks

Class-relationship checkers:

is_instance()
    Check for instance--class relation

is_subclass()
    Check for child--parent relation

Callable-type checkers:

takes_positional_args()
    Check for callables taking a given number of positional arguments

takes_keyword_args()
    Check for callables taking a given collection of keyword arguments

Equivalence-relationship checkers:

{eq|ne|gt|ge|lt|le}()
    Checks for =, !=, etc.

Identity checkers:

[not_]identical_to()
    Checks for object identity

Membership checkers:

{is|not}_in()
    Checks for collection membership

[not_]contains()
    Checks for collection content

Access-based checkers:

check_{attr|item}()
    Checks for the results of attribute/item access

check_call()
    Check for the results of method calls

String(-like) checkers:

is_keyword()
    Check for identifiers/keywords which can be used as e.g. attribute
    or callable-parameter names

is_{ascii|alphanumeric|alphabetic|digit|space}()
    Character-type checks (common to both `str` and `bytes`)

is_{decimal|numeric|printable|identifier(= keyword)}()
    Character-type checks (`str`-only)

is_{lower|upper|title}case()
    Case checks (common to both `str` and `bytes`)

{starts|ends}_with()
    Affix checks (common to both `str` and `bytes`)

matches()
    Regex-based check (common to both `str` and `bytes`)

Misc. checkers:

is_strict_boolean()
    Check for boolean types excluding integer aliases like 0 and 1

is_numeric_format_spec()
    Check for format specifiers for numbers (e.g. '.3f')

is_type_checkable()
    Check for entities (e.g. types, tuples of types) which can be used
    in type checks

Decorator functions:

@logical
    Equip a checker callable with logical operations (see Logical
    operations)

@check()
    Impose type checks on the arguments passed to the callable

@annotate()
    Set the annotations of a callable

@add_annotations()
    Add annotations to a callable

@set_defaults()
    Set the default values of the parameters of the callable

@add_defaults()
    Add default values to the parameters of the callable

@type_checked()
    Impose type checks, defaults, and/or parameter annotations on the
    arguments passed to the callable

Currying
--------
The following relations can be used to construct curried type checks:

is_coll(coll, **kwargs) == is_coll(**kwargs)(coll),
    where `is_coll()` = `is_collection()`, `is_sequence()`, or
    `is_mapping()`

is_instance(inst, cls) == is_instance(cls)(inst)

is_subclass(child, parent) == is_subclass(parent)(child)

takes_positional_args(func, n[, agg=...])
== takes_positional_args(n[, agg=...])(func)

takes_keyword_args(func, kwargs[, agg=...])
== takes_keyword_args(kwargs[, agg=...])(func)

equiv(x, y) == equiv(y)(x),
    where `equiv()` = `eq()`, `ne()`, `gt()`, `ge()`, `lt()`, or `le()`

[not_]identical_to(x, y) == [not_]identical_to(y)(x)

{is|not}_in(x, y) == {is|not}_in(y)(x)

[not_]contains(x, y) == [not_]contains(y)(x)

check_{attr|item}(obj, acc[, checker=...])
== check_{attr|item}(acc[, checker=...])(obj)

str_check(string, type=...) == str_check(type=...)(string)
    where `str_check`
    = `is_ascii()`, `is_alphanumeric()`, `is_alphabetic()`,
    `is_digit()`, `is_space()`, `is_decimal()`, `is_numeric()`,
    `is_printable()`, `is_identifier()`, `is_keyword()`,
    `is_lowercase()`, `is_uppercase()`, or `is_titlecase()`

affix_check(string, affix[, **kwargs])
== affix_check(affix[, **kwargs])(string)
    where `affix_check()` = `starts_with()` or `ends_with()`

matches(string, pattern[, **kwargs])
== matches(pattern[, **kwargs])(string)

Logical operations
------------------
All the final checker functions (i.e. those with all other requisite
arguments supplied via currying, leaving only the argument to be
checked) are equipped with the following logical operations, which
return another checker function equivalent to doing the logical
operations on the return value(s) of the original checker(s):

NOT
    (~checker)(x) == (x: x -> not checker(x))

AND
    (c1 & c2)(x) == (x: x -> (c1(x) and c2(x)))

XOR
    (c1 ^ c2)(x) == (x: x -> (c1(x) ^ c2(x)))

OR
    (c1 | c2)(x) == (x: x -> (c1(x) or c2(x)))

Note that:

- In the binary operations, the other operand can be an arbitrary
  checker callable (e.g. `math.isfinite()`), or even the string
  definition of the appropriate lambda function (e.g.
  `'lambda x: (x % 3) == 2'`).

- The AND and OR checks are by default lazy/short-circuited;
  to create eager (resp. lazy) versions of the current checker function,
  one can call its `.eager()` (resp. `.lazy()`) method.

- One can check the structure of composite checkers arising from logical
  operations on checkers by looking at its `.tree`;
  the truth table tabulating the output truth value as a function of the
  truth values of the component checkers can be generated by calling
  `.get_truth_table()`.

Example
-------
Application of checks:

>>> from typing import Callable, Optional, Sequence, TypeVar
>>> from _MODULE_ import check, takes_positional_args, is_sequence
>>>
>>> T = TypeVar('T')
>>> Addable = TypeVar('Addable', float, Sequence[T])
>>>
>>> @check(func=takes_positional_args(2), seq=is_sequence)
... def reduce(
...     func: Callable[[T, T], T],
...     seq: Sequence[T],
...     init: Optional[T] = None,
... ) -> T:
...     if init is not None:
...         seq = (init, *seq)
...     iterator = iter(seq)
...     value = next(iterator)
...     for next_value in iterator:
...         value = func(value, next_value)
...     return value
...
>>> def add(x: Addable, y: Addable) -> Addable:
...     return x + y
...
>>> reduce(add, range(5))
10
>>> reduce(add, ['a', 'b', 'c'])
'abc'
>>> reduce(lambda x: x, range(5))  # doctest: +NORMALIZE_WHITESPACE
Traceback (most recent call last):
  ...
TypeError: ...reduce(func=<function <lambda> at 0x...>):
    expected type typing.Callable[[~T, ~T], ~T]
    (type check <function takes_positional_args(2) at 0x...>
    failed normally)
>>> reduce(add, 1)  # doctest: +NORMALIZE_WHITESPACE
Traceback (most recent call last):
  ...
TypeError: ...reduce(seq=1):
    expected type typing.Sequence[~T]
    (type check <function is_sequence at 0x...> failed normally)

Composition of checks:

>>> from _MODULE_ import is_instance, is_nonnegative_real, le
>>> is_seq_of_strings = is_sequence(item_type=str)
>>> is_between_0_and_1 = is_nonnegative_real & le(1)
>>> is_in_unit_hypercube = is_sequence(item_checker=is_between_0_and_1)
>>> my_checker = is_seq_of_strings | is_in_unit_hypercube
>>> my_checker  # doctest: +NORMALIZE_WHITESPACE
<function is_sequence(item_type=<class 'str'>) at 0x...>
or <function
   is_sequence(item_checker=<function is_nonnegative_real at 0x...>
                            and <function le(1) at 0x...>)
   at 0x...>
>>> my_checker([.5, 1])  # Sequence of numbers between 0 and 1
True
>>> my_checker(.1)
False
>>> my_checker('string')  # String -> sequence of strings
True
>>> my_checker(['string', .1])
False
"""
from .accessors import check_attr, check_call, check_item
from .callables import takes_positional_args, takes_keyword_args
from .comparators import (
    eq, ne, gt, ge, lt, le,
    identical_to, not_identical_to,
    is_in, not_in, contains, not_contains,
)
from .core import (
    is_type_checkable, is_instance, is_subclass,
    is_collection, is_sequence, is_mapping, is_keyword_mapping,
)
from .decorators import (
    type_checked, check, annotate, add_annotations, set_defaults, add_defaults,
)
from .logical_ops import logical
from .numerics import (
    is_numeric_format_spec, is_strict_boolean, is_finite_real,
    is_positive_integer, is_positive_real,
    is_nonpositive_integer, is_nonpositive_real,
    is_negative_integer, is_negative_real,
    is_nonnegative_integer, is_nonnegative_real,
)
from .strings import (
    is_ascii, is_alphanumeric, is_alphabetic, is_digit, is_space,
    is_lowercase, is_uppercase, is_titlecase, starts_with, ends_with,
    is_decimal, is_numeric, is_printable, is_identifier, is_keyword,
    matches,
)
from .tuples import items_fulfill

__all__ = (
    # Numeric-type checkers
    'is_finite_real',
    'is_positive_integer', 'is_positive_real',
    'is_nonpositive_integer', 'is_nonpositive_real',
    'is_negative_integer', 'is_negative_real',
    'is_nonnegative_integer', 'is_nonnegative_real',
    # Container-type checkers
    'is_collection',
    'is_sequence',
    'is_mapping',
    'is_keyword_mapping',
    'items_fulfill',
    # Class-relationship checkers
    'is_instance',
    'is_subclass',
    # Callable-type checkers
    'takes_positional_args',
    'takes_keyword_args',
    # Equivalence-relationship checkers
    'eq', 'ne', 'gt', 'ge', 'lt', 'le',
    # Identity checkers
    'identical_to', 'not_identical_to',
    # Membership checkers
    'is_in', 'not_in', 'contains', 'not_contains',
    # Access checkers
    'check_attr', 'check_item', 'check_call',
    # String checkers ((byte-)string methods)
    'is_ascii', 'is_alphanumeric', 'is_alphabetic', 'is_digit', 'is_space',
    'is_lowercase', 'is_uppercase', 'is_titlecase',
    'starts_with', 'ends_with',
    # String-only checkers
    'is_decimal', 'is_numeric', 'is_printable',
    'is_identifier', 'is_keyword',
    # Other string checkers
    'matches',
    # Misc. checkers
    'is_strict_boolean',
    'is_numeric_format_spec',
    'is_type_checkable',
    # Checker decorators
    'logical',
    # Other decorators
    'type_checked',
    'check',
    'annotate',
    'add_annotations',
    'set_defaults',
    'add_defaults',
)

__version__ = '1.3.0'
__doc__ = __doc__.replace('_MODULE_', (lambda: None).__module__)
