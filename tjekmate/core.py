"""
Core checks (class relationships, collection checks)
"""
import functools
import numbers
import typing
from keyword import iskeyword

import numpy as np

from . import _helper_decorators
from .decorators import check
from .logical_ops import logical

__all__ = (
    # Basic checks
    'is_type_checkable', 'is_instance', 'is_subclass',
    # Collection checks
    'is_keyword',
    'is_collection',
    'is_sequence',
    'is_mapping',
    'is_keyword_mapping',
)


@logical
@_helper_decorators.partial_leading_args(min_nargs=1)
def identical_to(obj: typing.Any, other: typing.Any) -> bool:
    """
    Forward porting of `~.comparators.identical_to()`, defined here so
    that we can import `.core` from `.comparators` instead of the other
    way round.
    """
    return obj is other


@logical
def is_keyword(obj: typing.Any) -> bool:
    """
    Return
    ------
    Whether `obj` is a string identifier which can e.g. be an attribute
    or argument name

    Example
    -------
    >>> [
    ...     # Note: 'with' and 'return' are reserved words, hence they
    ...     # can't be keyword arguments, attribute names, etc.
    ...     is_keyword(word)
    ...     for word in '1 word with return _'.split()
    ... ]
    [False, True, False, False, True]
    """
    # Note: cannot be a usual "keyword" if it is a system keyword
    return isinstance(obj, str) and obj.isidentifier() and not iskeyword(obj)


@logical
def is_keyword_mapping(obj: typing.Any) -> bool:
    """
    Return
    ------
    Whether `obj` is a mapping from string identifiers to values;
    such a mapping can be unpacked into valid keyword arguments for a
    callable, or valid attributes for an object

    Examples
    --------
    Checking against non-mappings:

    >>> is_keyword_mapping([]), is_keyword_mapping(1)
    (False, False)

    Checking against non-keyword mappings:

    >>> test = lambda **kw: None
    >>> for kwargs in (
    ...     {},  # Empty mapping
    ...     dict(foo=None, bar=None),  # Mapping with keywords
    ...     {'in': None},  # Mapping with a non-keyword
    ... ):
    ...     repr_kwargs = ', '.join(
    ...         f'{k}={v!r}' for k, v in kwargs.items()
    ...     )
    ...     is_kw = is_keyword_mapping(kwargs)
    ...     try:
    ...         eval(f'test({repr_kwargs})')
    ...     except Exception as e:
    ...         assert not is_kw
    ...         print(kwargs, type(e).__name__)
    ...     else:
    ...         assert is_kw
    ...         print(kwargs, 'ok')
    {} ok
    {'foo': None, 'bar': None} ok
    {'in': None} SyntaxError
    """
    return isinstance(obj, typing.Mapping) and all(is_keyword(k) for k in obj)


@logical
def is_type_checkable(obj: typing.Any) -> bool:
    """
    Return
    ------
    Whether `obj` looks like something which can be used in a type
    (instance or subclass) check.

    Example
    -------
    >>> import typing
    >>> try:
    ...     from typing import Protocol, runtime_checkable
    ... except ImportError:
    ...     from typing_extensions import Protocol, runtime_checkable
    ...
    >>> @runtime_checkable
    ... class MyTemplate(Protocol):
    ...     def method(self, *args, **kwargs) -> None:
    ...         ...
    ...
    ...     attr: float
    ...
    >>> [
    ...     is_type_checkable(t)
    ...     for t in [
    ...         None, type(None), int,
    ...         (), (int, str), [int, str], (int, None),
    ...         typing.Sequence, MyTemplate,
    ...     ]
    ... ]
    [False, True, True, True, True, False, False, True, True]
    """
    if isinstance(obj, tuple):
        return all(is_type_checkable(item) for item in obj)
    try:
        isinstance(object(), obj)
    except TypeError:
        return False
    return True


@typing.overload
def is_instance(
    obj: typing.Union[type, typing.Tuple[type, ...]],
) -> typing.Callable[[typing.Any], bool]:
    ...


@typing.overload
def is_instance(
    obj: typing.Any, cls: typing.Union[type, typing.Tuple[type, ...]],
) -> bool:
    ...


@logical
@_helper_decorators.partial_leading_args(placeholder=None, min_nargs=1)
def is_instance(
    obj: typing.Any, cls: typing.Union[type, typing.Tuple[type, ...]],
) -> bool:
    """
    Checker for class membership.

    Call signatures
    ---------------
    (type_or_types) -> Callable[[object], is_instance_of_type_or_types]
    (object, type_or_types) -> is_instance_of_type_or_types

    Example
    -------
    >>> is_int = is_instance(int)
    >>> objs = [None, 1, -2, 3.4, 4.]
    >>> [is_int(obj) for obj in objs]
    [False, True, True, False, False]
    >>> [is_instance(obj, (float, int)) for obj in objs]
    [False, True, True, True, True]
    """
    def _is_instance(obj, cls) -> bool:
        return isinstance(obj, cls)

    # Note: pre-do type checks where possible for speed
    if cls is None:
        if not is_type_checkable(obj):
            raise TypeError(
                f'*args = {(obj,)!r}: expected a class or tuple of classes'
            )
        return functools.partial(_is_instance, cls=obj)
    return check(_is_instance, cls=is_type_checkable)(obj, cls)


@typing.overload
def is_subclass(
    obj: typing.Union[type, typing.Tuple[type, ...]],
) -> typing.Callable[[typing.Any], bool]:
    ...


@typing.overload
def is_subclass(
    obj: typing.Any, cls: typing.Union[type, typing.Tuple[type, ...]],
) -> bool:
    ...


@logical
@_helper_decorators.partial_leading_args(placeholder=None, min_nargs=1)
def is_subclass(
    obj: typing.Any, cls: typing.Union[type, typing.Tuple[type, ...]],
) -> bool:
    """
    Checker for class membership.

    Call signatures
    ---------------
    (type_or_types) -> Callable[[object], is_subclass_of_type_or_types]
    (object, type_or_types) -> is_subclass_of_type_or_types

    Example
    -------
    >>> import numbers
    >>>
    >>> is_real_num_class = is_subclass(numbers.Real)
    >>> objs = [None, 1, float, int, str]
    >>> [is_real_num_class(obj) for obj in objs]
    [False, False, True, True, False]
    >>> [is_subclass(obj, (float, str)) for obj in objs]
    [False, False, True, False, True]

    Notes
    -----
    Instead of raising a `TypeError` if `obj` is not a type, we just
    return `False`.
    """
    def _is_subclass(obj, cls) -> bool:
        try:
            return issubclass(obj, cls)
        except TypeError:
            return False

    # Note: pre-do type checks where possible for speed
    if cls is None:
        if not is_type_checkable(obj):
            raise TypeError(
                f'*args = {(obj,)!r}: expected a class or tuple of classes'
            )
        return functools.partial(_is_subclass, cls=obj)
    return check(_is_subclass, cls=is_type_checkable)(obj, cls)


@logical
@_helper_decorators.partial_sole_positional_arg
@check(
    length=lambda x: (
        # Note: we would've been able to do the equivalent with
        # >>> length=(
        # ...     identical_to(None)
        # ...     | is_instance(numbers.Integral)
        # ...     | is_collection(item_type=numbers.Integral)
        # ... )
        # but we haven't defined is_collection() yet... so it has to be
        # a lambda
        x is None or
        isinstance(x, numbers.Integral) or
        isinstance(x, typing.Collection) and
        all(isinstance(n, numbers.Integral) for n in x)
    ),
    item_checker=(identical_to(None) | callable),
    collection_type=(identical_to(None) | is_type_checkable),
)
def is_collection(
    x: typing.Any,
    *,
    collection_type: typing.Union[
        typing.Type[typing.Collection],
        typing.Tuple[typing.Type[typing.Collection], ...],
    ] = typing.Collection,
    length: typing.Optional[
        typing.Union[numbers.Integral, typing.Collection[numbers.Integral]]
    ] = None,
    item_checker: typing.Optional[typing.Callable[[typing.Any], bool]] = None,
    item_type: typing.Optional[
        typing.Union[type, typing.Tuple[type, ...]]
    ] = None,
    allow_item: bool = False,
) -> bool:
    """
    Checker for collections.

    Parameters
    ----------
    x
        Object to check
    collection_type
        Type(s) against which `x` is checked
    length
        Optional permitted length or lengths;
        default is to allow sequences of all lengths
    item_checker()
        Optional callable returning for each item in `x` whether it is
        allowed (if true) or not (if false or if an error is raised);
        cannot be given together with `item_type`
    item_type
        Optional type(s) against which each item in `x` is checked, as a
        shorthand for

        >>> item_checker = is_instance(  # noqa # doctest: +SKIP
        ...     item_type
        ... );

        cannot be given together with `item_checker`
    allow_item
        Whether to allow objects themselves passing the `item_checker()`
        or `item_type` tests to pass the checker;
        if true, either `item_checker()` or `item_type` is required

    Return
    ------
    `x` passed
        Whether it is a collection passing the checks
    else
        Checker with the supplied defaults

    Examples
    --------
    Parametrize and use:

    >>> is_small_set_with_odd_cardinality = is_collection(
    ...     collection_type=(set, frozenset),
    ...     length=(1, 3, 5),
    ... )
    >>> [
    ...     is_small_set_with_odd_cardinality(obj)
    ...     for obj in [
    ...         None, {}, {1}, {1, 2}, [1], frozenset({1, 2, 3}),
    ...     ]
    ... ]
    [False, False, True, False, False, True]

    Normal use:

    >>> [is_collection(obj, length=2) for obj in [None, {1, 2}, [3]]]
    [False, True, False]

    Checks for item type:

    >>> [
    ...     is_collection(obj, item_type=int, allow_item=True)
    ...     for obj in [None, [1, 2], [1, 2.], 1]
    ... ]
    [False, True, False, True]

    See also
    --------
    `is_sequence()`, `is_mapping()`
    """
    if item_checker and item_type:
        raise TypeError(
            f'item_checker = {item_checker!r}, '
            f'item_type = {item_type!r}: '
            'at most one of these to be passed'
        )
    if item_type is not None:
        item_checker = is_instance(item_type)
    # Escape route: object passes item check
    if allow_item:
        if item_checker is None:
            raise ValueError(
                f'allow_item = {allow_item!r}, '
                'item_checker = item_type = None: '
                'cannot check item instances without a checking criterion'
            )
        try:
            if item_checker(x):
                return True
        except Exception:
            pass
    if isinstance(length, numbers.Integral):
        length = (length, )
    if not isinstance(x, collection_type):
        return False
    # Now that we're here we SHOULD be able to assume x to be a
    # collection, but the case remains that someone could've passed a
    # non-collection class to `collection_type`
    if collection_type not in (typing.Collection, (typing.Collection,)):
        if not isinstance(x, typing.Collection):  # Sanity check
            raise TypeError(
                f'x = {x!r}, collection_type = {collection_type!r}: '
                'object passed instance check with `collection_type` but '
                'failed instance check with `Collection`'
            )
    if length is not None:
        if len(x) not in length:
            return False
    if item_checker is None:
        return True
    try:
        return all(item_checker(item) for item in x)
    except Exception:
        return False


@logical
@_helper_decorators.partial_sole_positional_arg
def is_sequence(x, **kwargs) -> bool:
    """
    Sequence checker which also works with numpy arrays.

    Parameters
    ----------
    **kwargs
        See `is_collection()`

    Return
    ------
    `x` passed
        Whether it is a sequence passing the checks
    else
        Checker with the supplied defaults

    Examples
    --------
    Length checking:

    >>> from numbers import Real, Integral
    >>> from numpy import array
    >>>
    >>> is_3_seq = is_sequence(length=3)
    >>> [
    ...     is_3_seq(obj)
    ...     for obj in (
    ...         [],  # Empty list -> wrong length
    ...         array([1, 2, 3]),  # numpy array of shape (3,) -> ok
    ...         'abc',  # String of length 3 = seq of 3 characters -> ok
    ...         {1, 2, 3},  # Set of length 3 -> not indexable
    ...         None,  # None -> not iterable
    ...     )
    ... ]
    [False, True, True, False, False]
    >>> is_small_seq = is_sequence(length=range(4))
    >>> [is_small_seq(range(i + 1)) for i in range(5)]
    [True, True, True, False, False]

    Item checking via type:

    >>> is_real_vector = is_sequence(item_type=Real)
    >>> [is_real_vector(v) for v in ([1, 2], [1, 1j], '123')]
    [True, False, False]
    >>> is_sequence('123', item_type=(str, Real))
    True

    Item checking via checker:

    >>> def is_odd(i) -> bool:
    ...     return isinstance(i, Integral) and bool(i % 2)
    ...
    >>> is_odd_seq = is_sequence(item_checker=is_odd)
    >>> [is_odd_seq(s) for s in ([1], [1, 2], [1, 3], [2, 4])]
    [True, False, True, False]

    Errors in the checker:

    >>> def bad_checker(x):
    ...     raise Exception
    >>>
    >>> check_seq_bad = is_sequence(item_checker=bad_checker)
    >>> check_seq_bad([])  # Checker not called -> ok
    True
    >>> check_seq_bad([1])  # Checker called -> not ok
    False

    See also
    --------
    `is_collection()`
    """
    if isinstance(x, np.ndarray):
        x = x.tolist()
    return is_collection(
        x, collection_type=typing.Sequence, **kwargs,
    )


@logical
@_helper_decorators.partial_sole_positional_arg
@check(**dict.fromkeys(
    ['key_checker', 'value_checker', 'item_checker'],
    identical_to(None) | callable,
))
def is_mapping(
    x: typing.Any,
    *,
    key_checker: typing.Optional[
        typing.Callable[[typing.Hashable], bool]
    ] = None,
    key_type: typing.Optional[
        typing.Union[type, typing.Tuple[type, ...]]
    ] = None,
    value_checker: typing.Optional[typing.Callable[[typing.Any], bool]] = None,
    value_type: typing.Optional[
        typing.Union[type, typing.Tuple[type, ...]]
    ] = None,
    item_checker: typing.Optional[
        typing.Callable[[typing.Tuple[typing.Hashable, typing.Any]], bool]
    ] = None,
    allow_key: bool = False,
    allow_keys: bool = False,
    allow_value: bool = False,
    allow_values: bool = False,
    mapping_must_match_values: bool = True,
) -> bool:
    """
    Mapping checker with separate handling for keys and values;
    see `is_collection()` for the semantics.

    Parameters
    ----------
    x
        Object to check
    key_checker(), key_type
        Optional checker (callable, type, or types) for each key in
        `x.keys()`;
        mutally exclusive
    value_checker(), value_type
        Optional checker (callable, type, or types) for each value in
        `x.values()`;
        mutally exclusive
    item_checker()
        Optional checker callable for each item in `x.items()`;
        mutually exclusive with the `{key|value}_{checker|type}`
        arguments
    allow_key (resp. allow_keys)
        Whether to allow objects passing (resp. collections whose items
        pass) the `key_checker()` or `key_type` tests to pass the
        checker;
        if true, either `key_checker()` or `key_type` is required
    allow_value (resp. allow_values)
        Whether to allow objects passing (resp. collections whose items
        pass) the `value_checker()` or `value_type` tests to pass the
        checker;
        if true, either `value_checker()` or `value_type` is required
    mapping_must_match_values
        Whether to limit the scope of `allow_keys`, not allowing
        mappings of the requisite key type but would otherwise have
        failed the value check to pass the check just because it is also
        a passing collection of keys;
        recommended to be kept on

    Return
    ------
    `x` passed
        Whether it is a mapping passing the checks
    else
        Checker with the supplied defaults

    Examples
    --------
    >>> import operator
    >>> from functools import partial
    >>> from numbers import Integral
    >>> from typing import Any, Tuple
    >>>
    >>> def kvp_is_capitalizing(kvp: Tuple[Any, Any]) -> bool:
    ...     key, value = kvp
    ...     if not all(isinstance(kv, str) for kv in kvp):
    ...         return False
    ...     if value.upper() != value:
    ...         return False
    ...     return key.casefold() == value.casefold()
    ...

    Normal use:

    >>> is_int_to_int_dict = is_mapping(
    ...     key_type=Integral, value_type=Integral,
    ... )
    >>> is_pos_to_neg_dict = is_mapping(
    ...     key_checker=partial(operator.lt, 0),
    ...     value_checker=partial(operator.gt, 0),
    ... )
    >>> is_str_keyed_dict = is_mapping(key_type=str)
    >>> is_capitalizer = is_mapping(item_checker=kvp_is_capitalizing)
    >>> [is_mapping(d) for d in [None, {}, {1: 2}, {3: '4'}]]
    [False, True, True, True]
    >>> [is_int_to_int_dict(d) for d in [None, {}, {1: 2}, {3: '4'}]]
    [False, True, True, False]
    >>> [is_pos_to_neg_dict(d) for d in [{1: -1}, {1: .5}, {-1: -1}]]
    [True, False, False]
    >>> [
    ...     is_str_keyed_dict(d)
    ...     for d in [{}, {1: 2}, {'3': 4}, {'': '', None: 1}]
    ... ]
    [True, False, True, False]
    >>> [
    ...     is_capitalizer(d)
    ...     for d in [{1: 2}, {'Foo': 'FOO'}, {'foo': 'Foo'}]
    ... ]
    [False, True, False]

    Checks allowing key(s) and/or value(s) to pass:

    >>> from typing import Collection, Union
    >>> try:
    ...     from typing import Literal
    ... except ImportError:
    ...     from typing_extensions import Literal
    >>> args_to_resolve = 'foo', 'bar', 'baz'
    >>> is_boolean = functools.partial(operator.contains, (True, False))
    >>> is_resolution_target = functools.partial(
    ...     operator.contains, args_to_resolve,
    ... )
    >>> is_resolvable, is_resolvable_no_mapping_guard = (
    ...     is_mapping(
    ...         key_checker=is_resolution_target,
    ...         value_checker=is_boolean,
    ...         allow_key=True, allow_keys=True, allow_value=True,
    ...         mapping_must_match_values=boolean
    ...     )
    ...     for boolean in (True, False)
    ... )
    >>> Args = Union[
    ...     Literal[args_to_resolve],  # e.g. 'foo'
    ...     bool,  # e.g. True
    ...     Collection[Literal[args_to_resolve]],  # e.g. 'foo', 'bar'
    ...     typing.Mapping[  # e.g. dict(foo=True, bar=False)
    ...         Literal[args_to_resolve], bool
    ...     ],
    ... ]
    >>>
    >>> def resolve_args(
    ...     args: Args,
    ... ) -> typing.Dict[Literal[args_to_resolve], bool]:
    ...     if not is_resolvable(args):
    ...         raise TypeError(f'args = {args!r}: expected {Args!r}')
    ...     if is_boolean(args):  # True -> {...: True}
    ...         return dict.fromkeys(args_to_resolve, args)
    ...     if is_resolution_target(args):
    ...         # 'foo' -> {'foo': True, ...: False}
    ...         args = args,
    ...     if is_mapping(args, key_checker=is_resolution_target):
    ...         # 'foo': bool, 'bar': bool -> {..., 'baz': False}
    ...         return {
    ...             arg: args.get(arg, False)
    ...             for arg in args_to_resolve
    ...         }
    ...     # 'foo', 'bar' -> {'foo': True, 'bar': True, ...: False}
    ...     return {arg: arg in args for arg in args_to_resolve}
    ...
    >>> for obj in [
    ...     None,  # Fails the check
    ...     True,
    ...     'foo',
    ...     ['foo', 'bar'],
    ...     dict(foo=True, foobar=True),  # Extra key fails check
    ...     dict(bar=True),
    ...     dict(foo=True, bar='string'),  # Wrong value fails check
    ... ]:
    ...     try:
    ...         result = resolve_args(obj)
    ...     except Exception:
    ...         print('{!r} -> error'.format(obj))
    ...     else:
    ...         print('{!r} -> {!r}'.format(obj, result))
    None -> error
    True -> {'foo': True, 'bar': True, 'baz': True}
    'foo' -> {'foo': True, 'bar': False, 'baz': False}
    ['foo', 'bar'] -> {'foo': True, 'bar': True, 'baz': False}
    {'foo': True, 'foobar': True} -> error
    {'bar': True} -> {'foo': False, 'bar': True, 'baz': False}
    {'foo': True, 'bar': 'string'} -> error
    >>> is_resolvable(  # This (this check used above) fails
    ...     dict(foo=True, bar='string'),
    ... )
    False
    >>> is_resolvable_no_mapping_guard(
    ...     # This passes because the malformed dict counts as a
    ...     # collection with permitted values
    ...     dict(foo=True, bar='string'),
    ... )
    True

    See also
    --------
    `is_collection()`
    """
    def combined_checker(
        kvp: typing.Tuple[typing.Hashable, typing.Any],
    ) -> bool:
        for index, checker in [
            (0, key_checker), (1, value_checker), (slice(None), item_checker),
        ]:
            checked = kvp[index]
            if checker is None:
                continue
            try:
                if not checker(checked):
                    return False
            except Exception:
                return False
        return True

    for pair in [
        dict(key_checker=key_checker, key_type=key_type),
        dict(value_checker=value_checker, value_type=value_type),
    ]:
        if all(pair.values()):
            pair_repr = ', '.join(
                '{} = {!r}'.format(*kvp) for kvp in pair.items()
            )
            raise TypeError(f'{pair_repr}: at most one of these to be passed')
    if key_type is not None:
        key_checker = is_instance(key_type)
    if value_type is not None:
        value_checker = is_instance(value_type)
    if item_checker is not None and (
        key_checker is not None or value_checker is not None
    ):
        raise TypeError(
            f'key_checker = {key_checker!r}, '
            f'value_checker = {value_checker!r}, '
            f'item_checker = {item_checker!r}: the latter cannot be provided '
            'together with any of the former'
        )
    # Escape routes: object passes key/value checks
    for (
        name, checker, allow_one, allow_collection,
        collection_check_ignore_mappings,
    ) in (
        ('key', key_checker, allow_key, allow_keys, mapping_must_match_values),
        ('value', value_checker, allow_value, allow_values, False),
    ):
        if not (allow_one or allow_collection):
            continue
        if checker is None:
            raise ValueError(
                f'allow_{name} = {allow_one!r}, '
                f'allow_{name}s = {allow_collection!r}, '
                f'{name}_checker = {name}_type = None: '
                f'cannot check instances of {name}s without a {name} checker'
            )
        if allow_one:
            try:
                if checker(x):
                    return True
            except Exception:
                pass
        if allow_collection:
            if (
                collection_check_ignore_mappings and
                isinstance(x, typing.Mapping)
            ):
                continue
            try:
                if is_collection(x, item_checker=checker):
                    return True
            except Exception:
                pass
    if not isinstance(x, typing.Mapping):
        return False
    return is_collection(x.items(), item_checker=combined_checker)
