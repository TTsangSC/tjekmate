"""
Base decorators the checkers depend on
"""
import collections
import functools
import inspect
import itertools
import numbers
import typing
try:
    from typing import Literal
except ImportError:  # < python3.8
    from typing_extensions import Literal
try:
    from typing import ParamSpec, Concatenate
except ImportError:  # < python3.10
    from typing_extensions import ParamSpec, Concatenate

__all__ = (
    'partial_leading_args',
    'partial_sole_positional_arg',
    'replace_in_docs',
)

Annotation = typing.TypeVar('Annotation')
T = typing.TypeVar('T')
T1 = typing.TypeVar('T1')
T2 = typing.TypeVar('T2')
PS1 = ParamSpec('PS1')
PS2 = ParamSpec('PS2')
FunctionLike = typing.TypeVar(
    'FunctionLike', typing.Callable, staticmethod, classmethod,
)
FunctionLikeOrType = typing.TypeVar('FunctionLikeOrType', FunctionLike, type)

EMPTY = inspect.Parameter.empty
OMITTABLE = type('_Omittable', (), dict(__repr__=lambda self: '<OMITTABLE>'))()


def partial_sole_positional_arg(
    func: typing.Callable[[T1], T2],
    *,
    placeholder: typing.Any = OMITTABLE,
) -> typing.Callable[..., typing.Union[T2, typing.Callable[[T1], T2]]]:
    """
    Make the single positional argument in a callable omittable;
    if omitted, a `functools.partial` object is returned.

    Parameters
    ----------
    func()
        Callable with:
        - EXACTLY ONE positional(-only) parameter, without a default
        - NO variadic positional parameter
        - OPTIONAL (variadic) keyword-only arguments
    placeholder
        Placeholder value to be spliced into the signature of the
        returned callable;
        should NOT be a value normally passed to the positional
        parameter

    Notes
    -----
    - `placeholder` isn't really explicitly used in the function
      wrapper, but beware when `func()` uses introspection to resolve
      its own arguments.

    - If a function is to use this decorator, its annotations should
      apply to the UN-decorated version;
      the annotations for the return type and the first parameter are
      transformed here.

    - This decorator handles the signature, but the user is responsible
      for writing the docs suitably.

    Example
    -------
    >>> import inspect
    >>> import typing
    >>>
    >>> @partial_sole_positional_arg(placeholder=None)
    ... def foo(x: int, *, y: int = 10) -> int: return x + y
    ...
    >>> sig = inspect.signature(foo)
    >>> assert sig.parameters['x'].annotation == typing.Union[int, None]
    >>> assert sig.return_annotation == typing.Union[
    ...     int, typing.Callable[[int], int],
    ... ]
    >>> foo(1, y=2)
    3
    >>> foo(y=5)(4)
    9
    """
    sig = inspect.signature(func)
    param0, *kw_params = sig.parameters.values()
    has_one_pos_arg = param0.kind in (
        param0.POSITIONAL_OR_KEYWORD, param0.POSITIONAL_ONLY,
    )
    pos_arg_no_default = param0.default is EMPTY
    other_args_kwonly = all(
        param.kind in (param.KEYWORD_ONLY, param.VAR_KEYWORD)
        for param in kw_params
    )
    if not (has_one_pos_arg and pos_arg_no_default and other_args_kwonly):
        raise TypeError(
            f'func: {func.__name__}{sig}: '
            'expected exactly one positional parameter without a default, and '
            'all others to be keyword-only'
        )

    @typing.overload
    def wrapper(**kwargs) -> typing.Callable[
        [param0.annotation], sig.return_annotation
    ]:
        ...

    @typing.overload
    def wrapper(arg: param0.annotation, **kwargs) -> sig.return_annotation: ...

    @functools.wraps(func)
    def wrapper(*arg, **kwargs):
        if not arg:
            # Note: do the partial bind to guard against bad arguments
            sig.bind_partial(**kwargs)
            return functools.partial(func, **kwargs)
        return func(*arg, **kwargs)

    # Fix the signature
    if sig.return_annotation is EMPTY:
        ReturnType = typing.Any
    else:
        ReturnType = sig.return_annotation
    if param0.annotation is EMPTY:
        ArgType = typing.Any
    else:
        ArgType = param0.annotation
    if ArgType is typing.Any:
        ApparentArgType = ArgType
    else:
        ApparentArgType = typing.Union[
            ArgType, (None if placeholder is None else Literal[placeholder]),
        ]
    new_param0 = param0.replace(annotation=ApparentArgType, default=placeholder)
    wrapper.__signature__ = sig.replace(
        parameters=[new_param0, *kw_params],
        return_annotation=typing.Union[
            ReturnType, typing.Callable[[ArgType], ReturnType],
        ],
    )
    return wrapper


partial_sole_positional_arg = partial_sole_positional_arg(
    partial_sole_positional_arg, placeholder=None,
)


@partial_sole_positional_arg
def partial_leading_args(
    func: typing.Callable[Concatenate[PS1, PS2], T],
    *,
    placeholder: typing.Any = OMITTABLE,
    min_nargs: typing.Optional[numbers.Integral] = None,
) -> typing.Callable[
    ..., typing.Union[T, typing.Callable[PS1, T]]
    # Note: either
    # >>> typing.Callable[Concatenate[PS1, PS2], T]
    # or
    # >>> typing.Callable[PS2, typing.Callable[PS1, T]]
]:
    """
    Make the leading positional arguments in a callable omittable;
    if omitted a callable is returned which is the inverse of
    `functools.partial`, appending instead of prepending the passed
    positional arguments.

    Parameters
    ----------
    func()
        Callable with:
        - AT LEAST ONE positional(-only) parameter(s), ALL without
          defaults
        - NO variadic positional parameter
        - OPTIONAL (variadic) keyword-only arguments
    placeholder
        Placeholder value to be spliced into the signature of the
        returned callable;
        should NOT be a value normally passed to the positional
        parameters
    min_nargs
        Optional non-negative number;
        if provided, require at least this many arguments to trigger
        partial processing

    Notes
    -----
    - `placeholder` isn't really explicitly used in the function
      wrapper, but beware when `func()` uses introspection to resolve
      its own arguments.

    - If a function is to use this decorator, its annotations should
      apply to the UN-decorated version;
      the annotations for the return type and the positional
      parameter(s) are transformed here.

    - This decorator handles the signatures (of both the decorated
      method and of the returned partial-like callable), but the user is
      responsible for writing the docs suitably.

    Example
    -------
    >>> import inspect
    >>> import typing
    >>>
    >>> @partial_leading_args(placeholder=None, min_nargs=1)
    ... def foo(x: int, y: float, z: str, *, a: float = 1.) -> None:
    ...     print('{:{}}'.format(a * (x - y), z))
    ...

    Automatic re-resolution of signatures:

    >>> sig = inspect.signature(foo)
    >>> assert [sig.parameters[xyz].annotation for xyz in 'xyz'] == [
    ...     typing.Union[int, float, str],
    ...     typing.Union[float, str, None],
    ...     typing.Union[str, None],
    ... ]
    >>> assert [sig.parameters[xyz].default for xyz in 'xyz'] == [
    ...     inspect.Parameter.empty, None, None,
    ... ]
    >>> assert sig.return_annotation == typing.Union[
    ...     None,
    ...     typing.Callable[[int], None],
    ...     typing.Callable[[int, float], None],
    ... ]

    >>> foo_with_default_yz = foo(1.5, '.2f')
    >>> new_sig = inspect.signature(foo_with_default_yz)
    >>> assert new_sig.parameters['y'].default == 1.5
    >>> assert new_sig.parameters['z'].default == '.2f'

    Invocation of partial-like callable:

    >>> foo_with_default_yz(5)  # = '{:.2f}'.format(1. * ((5) - 1.5))
    3.50
    >>> foo_with_default_yz(5, 6)  # = '{:2f}'.format(1. * ((5) - (6)))
    -1.00
    >>> foo_with_default_yz(
    ...     3, 5, a=2,  # = '{:2f}'.format((2.) * ((3) - (5)))
    ... )
    -4.00
    """
    def get_error_msg():
        return (
            f'func: {func.__name__}{sig}: '
            'expected (1) at least one positional parameter(s) all without '
            'defaults, (2) no variadic positional parameter, and (3) all others'
            'to be keyword-only'
        )

    # Handling the signature
    sig = inspect.signature(func)
    pos_params: typing.List[inspect.Parameter] = []
    kw_params: typing.List[inspect.Parameter] = []
    for param in sig.parameters.values():
        if param.kind in (param.POSITIONAL_OR_KEYWORD, param.POSITIONAL_ONLY):
            pos_params.append(param)
        elif param.kind == param.VAR_POSITIONAL:
            raise TypeError(get_error_msg())
        else:
            kw_params.append(param)
    if not pos_params:
        raise TypeError(get_error_msg())
    if any(param.default is not EMPTY for param in pos_params):
        raise TypeError(get_error_msg())

    # Handling number of args passed
    nargs_pos = len(pos_params)
    if min_nargs is None:
        min_nargs = 0
    if not isinstance(min_nargs, numbers.Integral) and min_nargs >= 0:
        raise TypeError(
            f'min_nargs = {min_nargs!r}: expected a nonnegative integer'
        )
    if min_nargs > nargs_pos:
        raise ValueError(
            (
                'func: {}{}, min_nargs = {!r}: '
                'more arguments required than `func` has positional parameters'
            ).format(func.__name__, sig, min_nargs)
        )

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        ba = sig.bind_partial(*args, **kwargs)
        if min_nargs <= len(ba.args) < nargs_pos:
            # Note: we can't fully reuse the partial-binding results
            # because the bindings on the positional args are wrong

            @functools.wraps(func)
            def inner_wrapper(*args, **kwargs):
                ba = inner_wrapper.__signature__.bind(*args, **kwargs)
                ba.apply_defaults()
                return func(*ba.args, **ba.kwargs)

            # Fix the signature
            ba_kwargs_only = sig.bind_partial(**kwargs)
            new_pos_params, new_kw_params = (
                collections.OrderedDict((param.name, param) for param in params)
                for params in (pos_params, kw_params)
            )
            # Insert new params and add/fix defaults from kwargs
            for arg_name, arg_value in kwargs.items():
                if arg_name in ba_kwargs_only.kwargs:
                    param = new_kw_params.get(arg_name)
                    if not param:
                        _, kwargs_param = new_kw_params.popitem()
                        assert kwargs_param.kind is kwargs_param.VAR_KEYWORD
                        # (Mapped to kwargs, insert a new named keyword-
                        # only param for it)
                        new_kw_params[arg_name] = inspect.Parameter(
                            arg_name, inspect.Parameter.KEYWORD_ONLY,
                            default=arg_value,
                        )
                        new_kw_params[kwargs_param.name] = kwargs_param
                    elif param.kind is not param.VAR_KEYWORD:
                        # Update default of keyword-only param
                        new_kw_params[arg_name] = param.replace(
                            default=arg_value,
                        )
                    else:  # Clash with kwargs, move kwargs elsewhere
                        _, kwargs_param = new_kw_params.popitem()
                        assert kwargs_param.kind is kwargs_param.VAR_KEYWORD
                        new_kw_params[arg_name] = inspect.Parameter(
                            arg_name, inspect.Parameter.KEYWORD_ONLY,
                            default=arg_value,
                        )
                        for new_name in itertools.chain(
                            [
                                'kwargs', 'keywords', 'kw',
                                'more_kwargs', 'more_keywords', 'more_kw',
                            ],
                            ('_' for i in itertools.count(1)),
                        ):
                            if new_name not in {
                                *new_kw_params, *new_pos_params,
                            }:
                                new_kw_params[new_name] = kwargs_param.replace(
                                    name=new_name,
                                )
                                break
                else:
                    # Positional args with new defaults supplied by
                    # kwargs
                    new_pos_params[arg_name] = (
                        new_pos_params[arg_name].replace(default=arg_value)
                    )
            # Sanity check: can we unambiguously do something like
            # func(*incoming_args, *args, **kwargs)?
            # (This requires all the keyword-supplied pos args to trail
            # the list of pos args)
            pos_args_have_no_defaults = [
                param.default is EMPTY for param in new_pos_params.values()
            ]
            if not all(
                pos_args_have_no_defaults[:sum(pos_args_have_no_defaults)]
            ):
                msg = (
                    'func: {}{}, func(..., {}): '
                    'positional arguments supplied via keywords should '
                    'trail other positional arguments'
                ).format(
                    func.__name__,
                    sig,
                    ', '.join('{}={!r}'.format(*kvp) for kvp in kwargs.items()),
                )
                raise TypeError(msg)
            # Add defaults to the pos args from args
            remaining_pos_param_names_reversed = (
                param.name for param in list(new_pos_params.values())[::-1]
                if param.default is EMPTY
            )
            for name, pos_value in zip(
                remaining_pos_param_names_reversed, args[::-1],
            ):
                new_pos_params[name] = new_pos_params[name].replace(
                    default=pos_value,
                )
            inner_wrapper.__signature__ = sig.replace(
                parameters=[*new_pos_params.values(), *new_kw_params.values()],
            )
            return inner_wrapper
        return func(*args, **kwargs)

    # Fix the signature
    base_annotations: typing.List[Annotation] = [
        typing.Any if param.annotation is EMPTY else param.annotation
        for param in pos_params
    ]
    revised_annotations: typing.List[typing.List[Annotation]] = [
        [] for _ in base_annotations
    ]
    return_annotations: typing.List[Annotation] = []
    new_pos_params: typing.List[inspect.Parameter] = []
    Placeholder = None if placeholder is None else Literal[placeholder]
    # E.g.
    # For the function
    # >>> @partial_leading_args(placeholder=None, min_nargs=1)
    # ... def foo(x: str, y: int, z: float) -> int: ...
    # The following signatures should be supported:
    # - (x: str, y: int, z: float) -> int
    # - (x: int, y: float) -> Callable[[str], int]
    # - (x: float) -> Callable[[str, int], int]
    if sig.return_annotation is EMPTY:
        ReturnType: Annotation = typing.Any
    else:
        ReturnType: Annotation = sig.return_annotation
    for i, nargs_passed in enumerate(range(min_nargs, nargs_pos + 1)[::-1]):
        passed_annotations = base_annotations[-nargs_passed:]
        remaining_annotations = base_annotations[:-nargs_passed]
        if i:  # Partial call signature -> partial-like method
            return_annotations.append(
                typing.Callable[remaining_annotations, ReturnType]
            )
        else:  # Full call signature -> function-call result
            return_annotations.append(ReturnType)
        for ann_list, annotation in zip(
            revised_annotations, passed_annotations,
        ):
            ann_list.append(annotation)
    assert all(revised_annotations)
    for i, (param, annotations) in enumerate(
        zip(pos_params, revised_annotations)
    ):
        if i >= min_nargs:
            param = param.replace(default=placeholder)
            annotations.append(Placeholder)
        new_pos_params.append(
            param.replace(annotation=typing.Union[tuple(annotations)])
        )
    wrapper.__signature__ = sig.replace(
        parameters=[*new_pos_params, *kw_params],
        return_annotation=typing.Union[tuple(return_annotations)],
    )
    return wrapper


@typing.overload
def replace_in_docs(
    replacements: typing.Optional[typing.Mapping[str, str]] = None,
    **more_replacements: str,
) -> typing.Callable[[FunctionLikeOrType], FunctionLikeOrType]:
    ...


@typing.overload
def replace_in_docs(
    # Note: python3.7 doesn't have positional-onlys ('/')
    method_or_cls: FunctionLikeOrType,
    replacements: typing.Optional[typing.Mapping[str, str]] = None,
    **more_replacements: str,
) -> FunctionLikeOrType:
    ...


def replace_in_docs(*args, **kwargs: str) -> None:
    r"""
    Replace occurrences of the specified patterns in the `.__doc__` of
    the argument (method or class) with target values.

    Call signatures
    ---------------
    (moc[, {pat_str: tar_str}][, **{pat_str: tar_str}]) -> moc
    ([{pat_str: tar_str}][, **{pat_str: tar_str}]) -> (deco(moc) -> moc)

    Examples
    --------
    >>> import textwrap
    >>>
    >>> def foo():
    ...     '''
    ...     This is a FOO function.
    ...     '''
    ...     pass
    ...
    >>> class Foo:
    ...     '''
    ...     This is a FOO class.
    ...     '''
    ...     def bar(self):
    ...         '''
    ...         This is a FOO instance method.
    ...         '''
    ...         pass
    ...
    ...     @classmethod
    ...     def baz(cls):
    ...         '''
    ...         This is a FOO class method.
    ...         '''
    ...
    ...     @staticmethod
    ...     def foobar(cls):
    ...         '''
    ...         This is a FOO static method.
    ...         '''

    Single-word replacement:

    >>> assert foo is replace_in_docs(foo, FOO='foo')

    Multi-target replacements (done sequentially):

    >>> assert Foo is replace_in_docs(
    ...     Foo, {'FOO': 'Foo', 'Foo class': 'Foo'}
    ... )

    Usage as a decorator:

    >>> @replace_in_docs({'replaced': 'decorated'})
    ... def print_doc(obj):
    ...     '''Show how the doc is replaced.'''
    ...     print(textwrap.dedent(obj.__doc__).strip())
    ...
    >>> @replace_in_docs({'ham': 'eggs'}, function='method')
    ... def spam():
    ...     '''
    ...     This function works on ham.
    ...     '''
    ...     pass

    Results:

    >>> print_doc(foo)
    This is a foo function.
    >>> print_doc(Foo)  # "FOO class" -> "Foo class" -> "Foo"
    This is a Foo.
    >>> print_doc(Foo.bar)
    This is a Foo instance method.
    >>> print_doc(Foo.baz)  # See print_doc(Foo)
    This is a Foo method.
    >>> print_doc(Foo.foobar)
    This is a Foo static method.
    >>> print_doc(print_doc)
    Show how the doc is decorated.
    >>> print_doc(spam)
    This method works on eggs.
    """
    def decorator(
        method_or_cls: FunctionLikeOrType,
        replacements: typing.Mapping[str, str],
    ) -> FunctionLikeOrType:
        for method_wrapper in staticmethod, classmethod:
            if isinstance(method_or_cls, method_wrapper):
                return method_wrapper(
                    decorator(method_or_cls.__func__, replacements),
                )
        if isinstance(method_or_cls, type):
            for method in vars(method_or_cls).values():
                if not (
                    # Static/class methods
                    isinstance(method, (staticmethod, classmethod)) or
                    # Instance methods
                    callable(method) and
                    callable(getattr(method, '__get__', None))
                ):
                    continue
                try:
                    decorator(method, replacements)
                except Exception:
                    pass
        if isinstance(getattr(method_or_cls, '__doc__', None), str):
            for pattern, replacement in replacements.items():
                try:
                    method_or_cls.__doc__ = (
                        method_or_cls.__doc__.replace(pattern, replacement)
                    )
                except Exception:
                    pass
        return method_or_cls

    nargs = len(args)
    assert nargs < 3
    if nargs == 2:
        method_or_cls, replacements = args
        assert callable(method_or_cls)
        return decorator(method_or_cls, dict(replacements, **kwargs))
    if nargs == 1:
        if callable(args[0]):
            return decorator(*args, kwargs)
        return functools.partial(
            decorator, replacements=dict(args[0], **kwargs),
        )
    return functools.partial(decorator, replacements=kwargs)
