"""
Decorators for performing type checks
"""
import functools
import inspect
import types
import typing
from keyword import iskeyword
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal
try:
    from typing import ParamSpec
except ImportError:
    from typing_extensions import ParamSpec

from ._helper_decorators import partial_sole_positional_arg

__all__ = (
    'type_checked',
    'check',
    'annotate', 'add_annotations',
    'set_defaults', 'add_defaults',
)

Keyword = typing.TypeVar('Keyword', bound=str)
T = typing.TypeVar('T')
PS = ParamSpec('PS')
Function = typing.Callable[PS, T]
Checker = typing.Callable[[typing.Any], bool]
Decorator = typing.Callable[[Function], Function]

# ************************* Helper functions ************************* #


def _is_keyword(obj: typing.Any) -> bool:
    return isinstance(obj, str) and obj.isidentifier() and not iskeyword(obj)


def _is_optional_mapping(
    obj: typing.Any,
    key_checker: typing.Optional[
        typing.Callable[[typing.Hashable], bool]
    ] = None,
) -> bool:
    if obj is None:
        return True
    if not isinstance(obj, typing.Mapping):
        return False
    if key_checker is None:
        return True
    return all(key_checker(key) for key in obj)


def _is_optional_keyword_mapping(obj: typing.Any) -> bool:
    return _is_optional_mapping(obj, _is_keyword)


def _check_annotations(obj: typing.Any) -> bool:
    def checker(key: typing.Hashable) -> bool:
        if key == 'return':
            return True
        return _is_keyword(key)

    return _is_optional_mapping(obj, checker)


def _has_truth_value(obj: typing.Any) -> bool:
    try:
        not obj
    except Exception:
        return False
    return True


def _convenience_func_sig_helper(func: Function) -> Function:
    """
    Update (cosmetically) the signature of the convenience wrappers
    around `@type_checked()` so that the first parameter is correctly
    denoted as optional positional-only instead of variadic-positional.

    Notes
    -----
    This is done for backwards compatibility: python 3.7's syntax did
    not allow functions to be defined with positional only arguments
    (e.g. `lambda a, b, /: ...`), and signatures with parameter kinds
    set to `inspect.Parameters.POSITIONAL_ONLY` does not bind correctly:

    >>> from inspect import Parameter, Signature
    >>> from sys import version_info
    >>> sig = Signature(
    ...     parameters=[
    ...         Parameter(
    ...             'x', kind=Parameter.POSITIONAL_ONLY, default=0,
    ...         ),
    ...         Parameter('k', kind=Parameter.VAR_KEYWORD),
    ...     ],
    ... )
    >>> try:
    ...     ba = sig.bind(1, x=2)
    ... except TypeError:  # "multiple values for argument 'x'"
    ...     assert version_info < (3, 8)
    ... else:
    ...     assert version_info >= (3, 8)
    ...     assert ba.arguments['x'] == 1
    ...     assert ba.arguments['k'] == {'x': 2}
    """
    @functools.wraps(func)
    def wrapper(*a, **k):
        return func(*a, **k)

    sig = inspect.signature(func)
    param, *params = sig.parameters.values()
    ann = param.annotation
    ann = typing.Optional[typing.Any if ann is param.empty else ann]
    assert param.kind == param.VAR_POSITIONAL
    wrapper.__signature__ = sig.replace(
        parameters=[
            param.replace(
                kind=param.POSITIONAL_ONLY, annotation=ann, default=None,
            ),
            *params,
        ],
    )
    return wrapper


# ************************** Main decorator ************************** #


def type_checked(
    func: Function,
    *,
    checkers: typing.Optional[typing.Mapping[Keyword, Checker]] = None,
    annotations: typing.Optional[
        typing.Mapping[typing.Union[Keyword, Literal['return']], typing.Any]
    ] = None,
    defaults: typing.Optional[typing.Mapping[Keyword, typing.Any]] = None,
    overwrite: typing.Optional[bool] = None,
    overwrite_annotations: typing.Optional[bool] = None,
    overwrite_defaults: typing.Optional[bool] = None,
) -> Function:
    r"""
    Decorator for type-checked callables.

    Parameters
    ----------
    func()
        Callable to be type-checked
    checkers
        Mapping from parameter names to checker functions, each of which
        returning either true or false for when an argument passes or
        fails the check
    annotations
        Mapping from parameter names (or 'return' for the return
        annotation) to annotations to add thereto
    defaults
        Mapping from parameter names to default values to add thereto
    overwrite, overwrite_annotations, overwrite_defaults
        Whether to overwrite the annotations and/or defaults of the
        callable if they already exist;
        where the more specific argument is not given, defer to
        `overwrite`

    Return
    ------
    func() provided
        Wrapped `func()` performing the checks when called
    else
        Decorator for type-checking with the given `checkers`

    Caveats
    -------
    - The decorator relies on the signature of the decorated function to
      work;
      as such, beware of using this as a decorator on a function and
      only later editing the signature.

    - It is also recommended for this decorator to be placed innermost
      in a chain of decorators.

    Examples
    --------
    Default argument manipulation:

    >>> import numbers
    >>> import re
    >>>
    >>> func1 = lambda a, b=1: a + b
    >>> add_b_default = type_checked(defaults=dict(b=2))
    >>> add_b_default_force = type_checked(
    ...     defaults=dict(b=2), overwrite=True,
    ... )
    >>> add_b_default(func1)(5)  # 5 + 1 (original default)
    6
    >>> add_b_default_force(func1)(5)  # 5 + 2 (overridden default)
    7

    Default value validation:

    >>> @type_checked(  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
    ...     checkers=dict(x=lambda x: isinstance(x, str)),
    ...     annotations=dict(x=str),
    ... )
    ... def func2(x=None): ...
    Traceback (most recent call last):
      ...
    TypeError: Erroneous default value: ...:
        ....func2(x=None):
        expected type <class 'str'> (type check ... failed normally)

    Normal validation:

    >>> @type_checked(
    ...     checkers=dict(
    ...         args=lambda args: all(
    ...             isinstance(a, numbers.Real) for a in args
    ...         ),
    ...     ),
    ... )
    ... def real_sum(*args: numbers.Real) -> numbers.Real:
    ...     return sum(args)
    >>> real_sum(1, 2, 3, 4)
    10
    >>> real_sum(  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
    ...     1, 2, (1+1j),
    ... )
    Traceback (most recent call last):
      ...
    TypeError: ...:
        ....real_sum(*args=(1, 2, (1+1j))):
        expected type <class 'numbers.Real'>
        (type check ... failed normally)

    Failed validation:

    >>> is_numeric_type = numbers.Number.__subclasscheck__
    >>> get_num_type_names = type_checked(
    ...     lambda t1, t2: ', '.join(t.__name__ for t in (t1, t2)),
    ...     checkers=dict.fromkeys(('t1', 't2'), is_numeric_type),
    ... )
    >>> get_num_type_names(float, int)  # OK
    'float, int'
    >>> get_num_type_names(  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
    ...     str, bytes,  # Non-numeric type -> fails type check
    ... )
    Traceback (most recent call last):
      ...
    TypeError: ...:
        ....<lambda>(t1=<class 'str'>):
        (type check ... failed normally)

    Erroneus validation:

    >>> try:
    ...    get_num_type_names(
    ...        int, 1,  # Non-type -> exception in type check
    ...    )
    ... except TypeError as e:
    ...     # Note: doctest can't handle chained exceptions, so we
    ...     # examine the exception manually
    ...     cause = getattr(e, '__cause__', None)
    ...     assert isinstance(cause, TypeError)
    ...     assert 'issubclass' in cause.args[0]
    ...     assert isinstance(e, TypeError)
    ...     assert re.search(r'\.<lambda>\(t2=1\)', e.args[0])
    ...     assert re.search(
    ...         r'\(type check .+ failed with error type TypeError\)',
    ...         e.args[0],
    ...     )
    ... else:
    ...     assert False
    """
    def check_arguments(
        arguments: typing.Mapping[Keyword, typing.Any],
        error_prefix: str = '',
    ) -> None:
        """
        Perform the type checks.
        """
        for name, value in arguments.items():
            if name not in checkers:
                continue
            try:
                is_ok = checkers[name](value)
            except Exception as e:  # Check failed
                check_status = 'failed with error type ' + type(e).__name__
                cause = e
            else:
                if is_ok:
                    continue
                check_status = 'failed normally'
                cause = None
            param = sig.parameters[name]
            if param.kind is param.VAR_POSITIONAL:
                param_name_prefix = '*'
            elif param.kind is param.VAR_KEYWORD:
                param_name_prefix = '**'
            else:
                param_name_prefix = ''
            msg = ' '.join(
                (call_pattern.format(param_name_prefix + name, value), )
                + (
                    (f'expected type {param.annotation!r}', )
                    if param.annotation is not param.empty else
                    ()
                )
                + (f'(type check {checkers[name]!r} {check_status})', )
            )
            raise TypeError(error_prefix + msg) from cause

    def resolve_param_attr(
        param: inspect.Parameter,
        attr: Keyword,
        mapping: typing.Mapping[Keyword, T],
        overwrite_if_exists: bool,
    ) -> typing.Union[T, typing.Any]:
        """
        Get the value of an attribute from either the parameter or the
        mapping.
        """
        existing = getattr(param, attr)
        new = mapping.get(param.name, existing)
        if existing is param.empty or overwrite_if_exists:
            return new
        return existing

    # Setup
    if checkers is None:
        checkers = {}
    if annotations is None:
        annotations = {}
    if defaults is None:
        defaults = {}
    if overwrite_annotations is None:
        overwrite_annotations = overwrite
    if overwrite_defaults is None:
        overwrite_defaults = overwrite
    sig = inspect.signature(func)
    try:
        inner_func = inspect.unwrap(func)
        call_pattern = (
            (
                '{}:{}: {}.{}'.format(
                    inner_func.__code__.co_filename,
                    inner_func.__code__.co_firstlineno,
                    func.__module__,
                    func.__qualname__,
                )
                .replace('{', '{{')
                .replace('}', '}}')
            )
            + '({}={!r}):'
        )
    except Exception:
        call_pattern = (
            repr(func).replace('{', '{{').replace('}', '}}') + ': {} = {!r}:'
        )
    # Edit signature
    for attr, mapping, overwrite_if_exists in [
        ('default', defaults, overwrite_defaults),
        ('annotation', annotations, overwrite_annotations),
    ]:
        if set(sig.parameters) & set(mapping):
            new_params = [
                p.replace(**{
                    attr:
                    resolve_param_attr(p, attr, mapping, overwrite_if_exists),
                })
                for p in sig.parameters.values()
            ]
            sig = sig.replace(parameters=new_params)
    if 'return' in annotations:
        existing = sig.return_annotation
        new = annotations.get('return', existing)
        if overwrite_annotations or existing is inspect.Parameter.empty:
            sig = sig.replace(return_annotation=new)
    # Sanity check: do the default values pass the checkers?
    defaults = {
        p.name: p.default for p in sig.parameters.values()
        if p.default is not p.empty
    }
    check_arguments(defaults, 'Erroneous default value: ')

    # Actual wrapper
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        ba = sig.bind(*args, **kwargs)
        ba.apply_defaults()
        check_arguments(ba.arguments)
        return func(*ba.args, **ba.kwargs)

    wrapper.__signature__ = sig
    wrapper.checkers = types.MappingProxyType(checkers)
    return wrapper


# Note: post-apply the decorators in order here;
# if we applied @partial_sole_positional_arg above, the self-check will
# fail because the placeholder None doesn't pass the type check
type_checked = type_checked(
    type_checked,
    checkers=dict(
        func=callable,
        checkers=_is_optional_keyword_mapping,
        defaults=_is_optional_keyword_mapping,
        annotations=_check_annotations,
        overwrite=_has_truth_value,
        overwrite_annotations=_has_truth_value,
        overwrite_defaults=_has_truth_value,
    ),
)
type_checked = partial_sole_positional_arg(type_checked, placeholder=None)


# ********************** Convenience decorators ********************** #


@_convenience_func_sig_helper
def check(
    *func: Function, **checks: Checker
) -> typing.Union[Decorator, Function]:
    """
    Thin wrapper around `@type_checked` as a shortcut to apply checkers,
    equivalent to

    >>> type_checked(  # noqa: F821 # doctest: +SKIP
    ...     func, checkers=checks,
    ... )
    """
    return type_checked(*func, checkers=checks)


@_convenience_func_sig_helper
def annotate(
    *func: Function, **annotations: typing.Any
) -> typing.Union[Decorator, Function]:
    """
    Thin wrapper around `@type_checked` as a shortcut to apply
    annotations, equivalent to

    >>> type_checked(  # noqa: F821 # doctest: +SKIP
    ...     func, annotations=annotations, overwrite=True,
    ... )
    """
    return type_checked(*func, annotations=annotations, overwrite=True)


@_convenience_func_sig_helper
def add_annotations(
    *func: Function, **annotations: typing.Any
) -> typing.Union[Decorator, Function]:
    """
    Thin wrapper around `@type_checked` as a shortcut to apply
    annotations, equivalent to

    >>> type_checked(  # noqa: F821 # doctest: +SKIP
    ...     func, annotations=annotations, overwrite=False,
    ... )
    """
    return type_checked(*func, annotations=annotations, overwrite=False)


@_convenience_func_sig_helper
def set_defaults(
    *func: Function, **defaults: typing.Any
) -> typing.Union[Decorator, Function]:
    """
    Thin wrapper around `@type_checked` as a shortcut to apply defaults,
    equivalent to

    >>> type_checked(  # noqa: F821 # doctest: +SKIP
    ...     func, defaults=defaults, overwrite=True,
    ... )
    """
    return type_checked(*func, defaults=defaults, overwrite=True)


@_convenience_func_sig_helper
def add_defaults(
    *func: Function, **defaults: typing.Any
) -> typing.Union[Decorator, Function]:
    """
    Thin wrapper around `@type_checked` as a shortcut to apply defaults,
    equivalent to

    >>> type_checked(  # noqa: F821 # doctest: +SKIP
    ...     func, defaults=defaults, overwrite=False,
    ... )
    """
    return type_checked(*func, defaults=defaults, overwrite=False)
