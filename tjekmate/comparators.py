"""
Comparison-based checks.
"""
import ast
import functools
import inspect
import operator
import textwrap
import typing
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

import numpy as np

from ._helper_decorators import partial_leading_args
from .core import is_keyword, is_instance, is_sequence
from .decorators import type_checked
from .logical_ops import logical


__all__ = (
    # Numeric comparisons
    'eq', 'ne', 'gt', 'ge', 'lt', 'le',
    # Identity comparisons
    'identical_to', 'not_identical_to',
    # Membership comparisons
    'is_in', 'not_in', 'contains', 'not_contains',
)

Keyword = typing.TypeVar('Keyword', bound=str)
LiteralEvaluable = typing.TypeVar(
    'LiteralEvaluable',
    bound=typing.Union[
        str, bytes, int, float, complex, tuple, list, dict, set,
    ]
)

# ************************* Helper functions ************************* #


def _is_bool(obj: typing.Any) -> bool:
    """
    Example
    -------
    >>> [_is_bool(obj) for obj in [None, 0, 1, True, False]]
    [False, False, False, True, True]
    """
    try:
        if obj not in (True, False):
            return False
    except Exception:  # E.g. ValueError on numpy arrays of .size > 1
        return False
    return repr(obj) == repr(bool(obj))


def _is_literal_evaluable(obj: typing.Any) -> bool:
    """
    Example
    -------
    >>> from numpy import array
    >>> class Foo:
    ...     pass
    ...
    >>> [
    ...     _is_literal_evaluable(obj)
    ...     for obj in [None, 1, 's', [1, 2], array([1, 2]), Foo(), str]
    ... ]
    [True, True, True, True, False, False, False]
    """
    try:
        result = (ast.literal_eval(repr(obj)) == obj)
        return not (not result)
    except Exception:
        return False


def _is_catchable(obj: typing.Any) -> bool:
    """
    Example
    -------
    >>> items = [
    ...     Exception, TypeError, str, None, (TypeError, ValueError),
    ...     (Exception, int), [TypeError, ValueError],
    ... ]
    >>> expected = [True, True, False, False, True, False, False]
    >>> assert [_is_catchable(item) for item in items] == expected
    """
    if not isinstance(obj, tuple):
        return _is_catchable((obj,))
    return all(
        isinstance(cls, type) and issubclass(cls, Exception)
        for cls in obj
    )


@partial_leading_args(placeholder=None, min_nargs=1)
def _takes_n_args(obj: typing.Any, n: int) -> bool:
    """
    Example
    -------
    >>> Foo = type('Foo', (), dict(bar=lambda self, x, y: None))
    >>> Bar = type('Foo', (), dict(baz=lambda self, x, y, z: None))
    >>> items = [
    ...     None,
    ...     lambda a, b: None,
    ...     lambda a, b, c: None,
    ...     lambda a, b, c, d: None,
    ...     lambda a, b, c, d=1: None,
    ...     lambda a, *b: None,
    ...     lambda a, b, c, *d: None,
    ...     lambda a, b, c, d, *e: None,
    ...     Foo.bar,
    ...     Foo().bar,
    ...     Bar.baz,
    ...     Bar().baz,
    ... ]
    >>> takes_3_args = _takes_n_args(3)
    >>> assert [
    ...     _takes_n_args(func, 3) for func in items
    ... ] == [
    ...     False,
    ...     False, True, False, True, True, True, False,
    ...     True, False, False, True,
    ... ] == [takes_3_args(func) for func in items]
    """
    try:
        inspect.signature(obj).bind(*((None,) * n))
    except TypeError:
        return False
    else:
        return True
    return False


@partial_leading_args(placeholder=None, min_nargs=1)
def _format_string_takes_n_str_args(obj: typing.Any, n: int) -> bool:
    """
    Example
    -------
    >>> items = [
    ...     None, 1,  # These aren't even strings
    ...     '', '{}', '{} {}',
    ...     '{} {:.2f}',  # This fails because it expects a float
    ...     '{} {} {}',  # This takes at least 3 arguments
    ...     '{} {',  # This is malformed
    ... ]
    >>> is_3_strings_formatter = _format_string_takes_n_str_args(3)
    >>> assert [
    ...     _format_string_takes_n_str_args(obj, 2) for obj in items
    ... ] == [
    ...     False, False, True, True, True, False, False, False
    ... ] == [is_3_strings_formatter(obj) for obj in items]
    """
    if not isinstance(obj, str):
        return False
    try:
        obj.format('', '')
    except Exception:
        return False
    return True


def _is_in(obj: typing.Any, other: typing.Any) -> bool:
    """
    Example
    -------
    >>> _is_in(1, (1, 2, 3))
    True
    >>> _is_in(1, (2, 3))
    False
    >>> _is_in(1, 2)
    False
    """
    try:
        return obj in other
    except Exception:  # E.g. TypeError('argument ... not iterable')
        return False


def _join_doc_chunks(*chunks: str, sep: str = '\n') -> str:
    return sep.join(textwrap.dedent(chunk).strip() for chunk in chunks)


@type_checked(
    checkers=dict(
        name=is_keyword,
        base_comparator=_takes_n_args(2),
        bin_op_format=_format_string_takes_n_str_args(2),
        test_value=_is_literal_evaluable,
        test_values=is_sequence(item_checker=_is_literal_evaluable),
        test_arrays=is_sequence(
            item_checker=is_sequence(item_checker=_is_literal_evaluable),
        ),
        # Note: we can't use `identical_to(None)` here and below for
        # `negated_format` because `identical_to()` isn't defined yet;
        # fall back to `is_instance(type(None))`
        partial_comparator_name=(is_instance(type(None)) | is_keyword),
        exceptions=_is_catchable,
        default_on_caught=_is_bool,
        partial_leading=_is_bool,
        negate=_is_bool,
        negated_format=(
            is_instance(type(None)) | _format_string_takes_n_str_args(2)
        ),
    )
)
def _build_comparator(
    base_comparator: typing.Callable[[typing.Any, typing.Any], bool],
    name: Keyword,
    bin_op_format: str,
    *,
    test_value: LiteralEvaluable = 1,
    test_values: typing.Sequence[LiteralEvaluable] = (None, 0, 1, 1., 2, 3),
    test_arrays: typing.Sequence[typing.Sequence[LiteralEvaluable]] = ((1, 2),),
    partial_comparator_name: typing.Optional[Keyword] = None,
    exceptions: typing.Union[
        typing.Type[Exception], typing.Tuple[typing.Type[Exception], ...]
    ] = Exception,
    default_on_caught: bool = False,
    partial_leading: bool = True,
    negate: bool = False,
    negated_format: typing.Optional[str] = None,
) -> typing.Callable[
    [typing.Any, typing.Any],
    typing.Union[bool, typing.Callable[[typing.Any], bool]]
]:
    """
    Build a comparator function with its doctests.

    Parameters
    ----------
    base_comparator()
        Callable taking two positional arguments and returning a truey
        or falsy value (e.g. `operator.lt`)
    name
        Identifier name of the function
    bin_op_format
        Two-field format string representing `base_comparator()` (e.g.
        `'{} < {}'`)
    test_value
        Literal value (e.g. `1`, `True`, `'foo'`) to test `test_values`
        against;
        it appears as the SECOND (if `partial_leading`) or FIRST (else)
        argument to `base_comparator()`
    test_values
        Sequence of literal values to test `test_value` against;
        each appears as the FIRST (if `partial_leading`) or SECOND
        (else) argument to `base_comparator()`
    test_arrays
        Sequence of sequences of literal values to test `test_value`
        against;
        each is converted to a numpy array, and then appears as the
        FIRST argument to `base_comparator()`
    partial_comparator_name
        Optional identifier name of the partial-ed function formed by
        calling the comparator on just `test_value`
    default_on_caught
        Default boolean value to be returned when a caught error occurs
        during the check
    partial_leading
        If only one argument is provided to the comparator, whether to
        return a partial function object APPENDING (if true) or
        PREPENDING (if false) said argument
    negate
        Whether to negate the return values of `base_comparator()`;
        does not affect `default_on_caught`
    negated_format
        Optional two-field format string representing the negation of
        `base_comparator()`, to be used in docstrings if `negate` is
        true (e.g. `'{} >= {}'`; default is `f'~({bin_op_format})'`)

    Example
    -------
    >>> import contextlib
    >>> import doctest
    >>> import io
    >>> from numpy import array
    >>>
    >>> def y_mod_x(x: int, y: int) -> int:
    ...     return y % x
    ...
    >>> test_value = 12
    >>> test_values = None, '', 1, 2, 3, 5, 13, 24
    >>> test_arrays = [1, 2], [2, 4, 5]
    >>> all_test_values = (
    ...     list(test_values) + [array(a) for a in test_arrays]
    ... )
    >>> expected_results = [
    ...     # Invalid operations
    ...     False, False,
    ...     # Valid integer operations
    ...     True, True, True, False, False, False,
    ...     # Invalid operations
    ...     False, False,
    ... ]
    >>> expected_results_prepended = [
    ...     # Invalid operations
    ...     False, False,
    ...     # Valid integer operations
    ...     False, False, False, False, False, True,
    ...     # Invalid operations
    ...     False, False,
    ... ]
    >>> divides, divides_prepended = (
    ...     _build_comparator(
    ...         y_mod_x, name, 'bool({1} % {0})',
    ...         test_value=test_value,
    ...         test_values=test_values,
    ...         test_arrays=test_arrays,
    ...         partial_leading=b,
    ...         negate=True,
    ...     )
    ...     for b, name in (
    ...         (True, 'divides'), (False, 'divides_prepended'),
    ...     )
    ... )
    >>> # Check the docstrings
    >>> for func in divides, divides_prepended:
    ...     with io.StringIO() as error_stream:
    ...         with contextlib.redirect_stdout(error_stream):
    ...             doctest.run_docstring_examples(
    ...                 func, {func.__name__: func},
    ...             )
    ...         _ = error_stream.seek(0)
    ...         error_output = error_stream.read()
    ...         assert (not error_output), error_output
    >>> # Direct behavior checks
    >>> divides_test_value = divides(test_value)
    >>> is_multiple_of_test_value = divides_prepended(test_value)
    >>> assert [
    ...     divides(value, test_value) for value in all_test_values
    ... ] == expected_results
    >>> assert [
    ...     divides_test_value(value) for value in all_test_values
    ... ] == expected_results
    >>> assert [
    ...     divides_prepended(test_value, value)
    ...     for value in all_test_values
    ... ] == expected_results_prepended
    >>> assert [
    ...     is_multiple_of_test_value(value)
    ...     for value in all_test_values
    ... ] == expected_results_prepended
    """
    def test_helper(
        value: LiteralEvaluable,
        expected: typing.Union[bool, typing.Type[Exception]],
        partial: bool,
    ) -> None:
        if partial:
            test_result = comparator(test_value)
            error_format = '{}({!r})({!r}) {} {}: expected {}'
        elif partial_leading:
            test_result = test_partial_append
            error_format = '{0}({2!r}, {1!r}) {3} {4}: expected {5}'
        else:
            test_result = test_partial_prepend
            error_format = '{}({!r}, {!r}) {} {}: expected {}'
        try:
            result = test_result(value)
        except Exception as e:
            result = type(e)
        if result != expected:
            if is_exception_class(result):
                to_result, repr_result = '->', repr_exception_class
            else:
                to_result, repr_result = '=', repr
            if is_exception_class(expected):
                repr_expected = repr_exception_class
            else:
                repr_expected = repr
            raise AssertionError(
                error_format.format(
                    name,
                    test_value,
                    value,
                    to_result,
                    repr_result(result),
                    repr_expected(expected),
                )
            )

    def is_exception_class(obj: typing.Any) -> bool:
        return isinstance(obj, type) and issubclass(obj, Exception)

    def repr_exception_class(obj: typing.Type[Exception]) -> str:
        return f'<{obj.__name__}>'

    def test_partial_prepend(value: LiteralEvaluable) -> bool:
        return comparator(test_value, value)

    def test_partial_append(value: LiteralEvaluable) -> bool:
        return comparator(value, test_value)

    if partial_leading:
        @partial_leading_args(min_nargs=1)
        def comparator(obj: typing.Any, other: typing.Any) -> bool:
            try:
                return (not negate) ^ (not base_comparator(obj, other))
            except exceptions:
                return default_on_caught

    else:
        class _Omittable:
            def __repr__(self) -> str:
                return '<OMITTABLE>'

        OMITTABLE = _Omittable()

        def comparator(
            obj: typing.Any,
            other: typing.Union[typing.Any, Literal[OMITTABLE]] = OMITTABLE,
        ) -> typing.Union[bool, typing.Callable[[typing.Any], bool]]:
            if other is OMITTABLE:
                return functools.partial(comparator, obj)
            try:
                return (not negate) ^ (not base_comparator(obj, other))
            except exceptions:
                return default_on_caught

    doc_template_leading_chunk = """
    Return
    ------
    `other` not provided
        Checker `(arg) -> bool` checking if `{PARTIAL_CHECK}`
    Exception occurs
        False
    Else
        Whether `{FULL_CHECK}`

    Example
    -------
    """
    doc_template_conditional_import = """
    >>> from numpy import array
    >>>
    """
    doc_template_rest_of_example = """
    >>> y = {TEST_VALUE!r}
    >>> args = [{TEST_VALUES}]
    >>> expected_results = [
    ...     {EXPECTED_RESULTS}
    ... ]
    >>> assert [{NAME}({FULL_SIG}) for x in args] == expected_results
    >>> {PARTIAL_NAME} = {NAME}(y)
    >>> assert [{PARTIAL_NAME}(x) for x in args] == expected_results
    """
    # Massage the arguments
    if partial_comparator_name is None:
        partial_comparator_name = f'{name}_{test_value}'
        if not is_keyword(partial_comparator_name):
            raise TypeError(
                f'name = {name!r}, test_value = {test_value!r}, '
                'partial_comparator_name = None'
                f' -> {partial_comparator_name!r} (default): '
                'default not an identifier owing to `test_value`, supply an '
                'explicit value to override the inapplicable default'
            )
    if negated_format is None:
        negated_format = f'~({bin_op_format})'
    test_values = list(test_values)
    test_arrays = [list(ar) for ar in test_arrays]
    all_test_values = test_values + [np.array(ar) for ar in test_arrays]

    # Verify that the checks are correct
    expected_results: typing.List[
        typing.Union[bool, typing.Type[Exception]]
    ] = []
    for value in all_test_values:
        args = (value, test_value)[::(1 if partial_leading else -1)]
        try:
            result = not (not base_comparator(*args))
        except exceptions:
            result = default_on_caught
        except Exception as e:
            result = type(e)
        else:
            if negate:
                result = not result
        expected_results.append(result)
    for value, expected in zip(all_test_values, expected_results):
        for partial in True, False:
            test_helper(value, expected, partial)

    # Manage metadata
    format_call = (negated_format if negate else bin_op_format).format
    comparator.__name__ = comparator.__qualname__ = name
    if test_arrays:
        doc_template = _join_doc_chunks(
            doc_template_leading_chunk,
            doc_template_conditional_import,
            doc_template_rest_of_example,
        )
    else:
        doc_template = _join_doc_chunks(
            doc_template_leading_chunk, doc_template_rest_of_example,
        )
    comparator.__doc__ = doc_template.format(
        PARTIAL_CHECK=format_call(
            *('arg', 'obj')[::(1 if partial_leading else -1)],
        ),
        FULL_CHECK=format_call('obj', 'other'),
        TEST_VALUE=test_value,
        TEST_VALUES=', '.join(
            [repr(value) for value in test_values]
            + [f'array({ar!r})' for ar in test_arrays]
        ),
        EXPECTED_RESULTS=', '.join(repr(result) for result in expected_results),
        NAME=name,
        FULL_SIG=('x, y' if partial_leading else 'y, x'),
        PARTIAL_NAME=partial_comparator_name,
    )
    return logical(comparator)


# *********************** Numeric comparisons ************************ #

eq, ne, gt, ge, lt, le = (
    _build_comparator(getattr(operator, name), name, bin_op)
    for name, bin_op in (
        ('eq', '{} == {}'),
        ('ne', '{} != {}'),
        ('gt', '{} > {}'),
        ('ge', '{} >= {}'),
        ('lt', '{} < {}'),
        ('le', '{} <= {}'),
    )
)

# *********************** Identity comparisons *********************** #

# Note: the docstring built by _build_comparator() mainly concerns
# value tests, so replace with something more relevant
identical_to, not_identical_to = (
    _build_comparator(
        operator.is_, name, '{} is {}',
        test_values=(),
        test_arrays=(),
        exceptions=(),  # Identity checks should NEVER throw
        partial_leading=False,
        negate=negate,
    )
    for name, negate in (('identical_to', False), ('not_identical_to', True))
)
identical_to.__doc__, not_identical_to.__doc__ = (
    """
    Return
    ------
    `other` provided
        Whether `obj {BIN_OP} other`
    Else
        Checker `(arg) -> bool` checking if `obj {BIN_OP} arg`

    Example
    -------
    >>> l1 = [1, 2, 3]
    >>> l2 = l1
    >>> l3 = list(l1)
    >>> assert l1 == l2 == l3
    >>> assert {NAME}(l1, l2) == {NAME}(l1)(l2) == {RESULTS[0]}
    >>> assert {NAME}(l1, l3) == {NAME}(l1)(l3) == {RESULTS[1]}
    """.format(NAME=name, RESULTS=results, BIN_OP=bin_op)
    for name, results, bin_op in (
        ('identical_to', (True, False), 'is'),
        ('not_identical_to', (False, True), 'is not'),
    )
)

# ********************** Membership comparisons ********************** #

contains, not_contains = (
    _build_comparator(
        operator.contains, name, '{1} in {0}',
        test_value=1,
        test_values=([1, 2], (1, 2, 3), {2, 3}, [], None),
        test_arrays=(),
        negate=negate,
        negated_format='{1} not in {0}',
    )
    for name, negate in (('contains', False), ('not_contains', True))
)

is_in, not_in = (
    _build_comparator(
        _is_in, name, '{0} in {1}',
        test_value=[1, 2, 3],
        test_values=(0, 1, 2, [1, 2], None),
        test_arrays=([1, 2],),
        negate=negate,
        negated_format='{0} not in {1}',
        partial_comparator_name=partial_name,
    )
    for name, partial_name, negate in (
        ('is_in', 'is_in_list', False), ('not_in', 'not_in_list', True),
    )
)
