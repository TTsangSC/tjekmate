"""
Extra tests for `~.takes_*_args()` and related checks.
"""
import inspect
import operator
import types
import typing
try:
    from typing import ParamSpec
except ImportError:
    from typing_extensions import ParamSpec

import pytest

from .. import takes_positional_args, takes_keyword_args, logical_ops


MISSING_SIG_EXAMPLES = {
    types.BuiltinFunctionType: print,
    types.MethodDescriptorType: str.count,
    operator.itemgetter: operator.itemgetter(0),
    operator.attrgetter: operator.attrgetter('real'),
}

PS = ParamSpec('PS')
T = typing.TypeVar('T')
Callable = typing.Callable[PS, T]

takes_one_arg = takes_positional_args(1)
takes_foo = takes_keyword_args('foo')


def check_type_and_sig(
    func: typing.Callable,
    type: type = typing.Callable,
) -> typing.Callable[[Callable], Callable]:
    """
    Convenience decorator for skipping a test if `func()` is not a
    callable of the requisite type and if it has a signature.
    """
    def decorator(test_func: Callable) -> Callable:
        return skip_wrong_type(skip_has_sig(test_func))

    skip_wrong_type = pytest.mark.skipif(
        not isinstance(func, type),
        reason=f'{func!r}: not of type {type!r}',
    )
    try:
        sig = inspect.signature(func)
    except ValueError:  # Callable but signature not parsable
        has_sig = False
        sig = None
    else:
        has_sig = True
    skip_has_sig = pytest.mark.skipif(
        has_sig,
        reason=f'{func!r}: has a signature {sig}; pick another example',
    )

    return decorator


def take_args_missing_sig_example(name: str, type: type):
    """
    Build a test checking that the `takes_positional_args()` and
    `takes_keyword_args()` checkers contain the `ValueError` raised by
    `inspect.signature()` on callables with missing signatures.

    Parameters
    ----------
    name
        Name of the test (should start with 'test_')
    type
        Type of callableto be tested  (should be within
        `MISSING_SIG_EXAMPLES`)

    Return
    ------
    Unit-test callable
    """
    assert name.startswith('test_')
    tested_func = MISSING_SIG_EXAMPLES[type]

    @check_type_and_sig(tested_func, type)
    def test_func():
        assert not takes_one_arg(tested_func)
        assert not takes_foo(tested_func)

    test_func.__name__ = test_func.__qualname__ = name
    return test_func


def logical_missing_sig_example(
    name: str,
    type: type,
    args: typing.Sequence = (),
    allow_no_error: bool = False,
):
    """
    Build a test checking that the `@logical` decorator intercepts
    callables with missing signatures.

    Parameters
    ----------
    name
        Name of the test (should start with 'test_')
    type
        Type of callable to be tested  (should be within
        `MISSING_SIG_EXAMPLES`)
    args
        Arguments to pass to the `logical` object
    allow_no_error
        Whether to raise an error if `TypeError` hasn't been raised, or

    Return
    ------
    Unit-test callable
    """
    assert name.startswith('test_')
    tested_func = MISSING_SIG_EXAMPLES[type]

    @pytest.mark.skipif(
        logical_ops._WARN_BUT_ACCEPT_UNSUPPORTED_SIGS,
        reason='`@logical` now only warns against callables with missing sigs',
    )
    @check_type_and_sig(tested_func, type)
    def test_func():
        try:
            checker = logical_ops.logical(tested_func)
            checker(*args)
        except TypeError:
            return
        else:
            if not allow_no_error:
                raise AssertionError(
                    (
                        'logical({!r})({}): '
                        'expected a TypeError, did not get any exceptions'
                    ).format(tested_func, ', '.join(repr(a) for a in args))
                )

    test_func.__name__ = test_func.__qualname__ = name
    return test_func


for test_type, args_checker_test, logical_test, logical_args in [
    (
        types.BuiltinFunctionType,
        'test_builtin_args',
        'test_builtin_logical',
        (),
    ),
    (
        types.MethodDescriptorType,
        'test_method_desc_args',
        'test_method_desc_logical',
        ('abc', 'a'),
    ),
    (operator.itemgetter, 'test_getitem_args', 'test_getitem_logical', ([1],)),
    (operator.attrgetter, 'test_getattr_args', 'test_getattr_logical', (1+1j,)),
]:
    locals()[args_checker_test] = take_args_missing_sig_example(
        args_checker_test, test_type,
    )
    locals()[logical_test] = logical_missing_sig_example(
        logical_test, test_type, logical_args,
    )
