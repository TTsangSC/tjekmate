"""
Some extra tests for the decorators
"""
import functools
import inspect
import itertools
import typing

import pytest

from .. import decorators

parametrize_param_test = pytest.mark.parametrize(
    'provide,overwrite,expected_new',
    [
        (provide, overwrite, (overwrite or not provide))
        for provide, overwrite in itertools.product(*(((True, False),) * 2))
    ],
)


@parametrize_param_test
def test_set_return_ann(
    provide: bool,
    overwrite: bool,
    expected_new: bool,
    old_ann: typing.Any = int,
    new_ann: typing.Any = float,
) -> None:
    assert old_ann != new_ann
    decorator = decorators.type_checked(
        annotations={'return': new_ann}, overwrite=overwrite,
    )
    if provide:
        @decorator
        def foo() -> old_ann:
            pass
    else:
        @decorator
        def foo():
            pass

    final_ann = inspect.signature(foo).return_annotation
    if expected_new:
        assert final_ann == new_ann
    else:
        assert final_ann == old_ann


@parametrize_param_test
def test_set_param_ann(
    provide: bool,
    overwrite: bool,
    expected_new: bool,
    old_ann: typing.Any = int,
    new_ann: typing.Any = float,
    use_wrapper: bool = False,
) -> None:
    assert old_ann != new_ann
    anns = {'x': new_ann}
    if not use_wrapper:
        decorator = decorators.type_checked(
            annotations=anns, overwrite=overwrite,
        )
    elif overwrite:
        decorator = decorators.annotate(**anns)
    else:
        decorator = decorators.add_annotations(**anns)
    if provide:
        @decorator
        def foo(x: old_ann) -> None:
            pass
    else:
        @decorator
        def foo(x) -> None:
            pass

    final_ann = inspect.signature(foo).parameters['x'].annotation
    if expected_new:
        assert final_ann == new_ann
    else:
        assert final_ann == old_ann


@parametrize_param_test
@functools.wraps(test_set_param_ann, updated=())
def test_set_param_ann_wrapper(*args, **kwargs) -> None:
    return test_set_param_ann(*args, **{**dict(use_wrapper=True), **kwargs})


@parametrize_param_test
def test_set_param_default(
    provide: bool,
    overwrite: bool,
    expected_new: bool,
    old_def: typing.Any = 1,
    new_def: typing.Any = 2,
    use_wrapper: bool = False,
) -> None:
    assert old_def != new_def
    defs = {'y': new_def}
    if not use_wrapper:
        decorator = decorators.type_checked(defaults=defs, overwrite=overwrite)
    elif overwrite:
        decorator = decorators.set_defaults(**defs)
    else:
        decorator = decorators.add_defaults(**defs)
    if provide:
        @decorator
        def foo(x, y=old_def) -> None:
            return x + y
    else:
        @decorator
        def foo(x, y) -> None:
            return x + y

    # 1. Correct default appears on the signature
    final_def = inspect.signature(foo).parameters['y'].default
    if expected_new:
        assert final_def == new_def
    else:
        assert final_def == old_def
    # 2. Function calls behave as expected
    assert foo(2, 4) == 6
    assert foo(3) == 3 + final_def


@parametrize_param_test
@functools.wraps(test_set_param_default, updated=())
def test_set_param_default_wrapper(*args, **kwargs) -> None:
    return test_set_param_default(*args, **{**dict(use_wrapper=True), **kwargs})
