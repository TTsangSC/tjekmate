import functools
import inspect
import operator
import traceback
import typing

import numpy as np
import pytest

from .. import is_collection, is_instance, takes_positional_args, logical
from .._helper_decorators import partial_leading_args
from ..logical_ops import _WARN_BUT_ACCEPT_UNSUPPORTED_SIGS


try:
    inspect.signature(np.isnan)
except ValueError:
    UFUNC_HAS_SIGNATURE = False
else:
    UFUNC_HAS_SIGNATURE = True


@partial_leading_args(min_nargs=1)
def has_factor(x: int, y: int) -> bool:
    """
    Example
    -------
    >>> is_even = has_factor(2)
    >>> [is_even(i) for i in range(5)]
    [True, False, True, False, True]
    """
    return isinstance(x, int) and isinstance(y, int) and (x % y == 0)


@pytest.mark.parametrize(
    'func, args, kwargs',
    [
        # Bad keyword arg (@partial_sole_positional_arg func)
        (is_collection, (), dict(foo=True)),
        # Bad keyword arg (@partial_leading_args func)
        (takes_positional_args, (2,), dict(bar=1)),
        # Too many positional args (@logical instantiation)
        (
            logical,
            (has_factor,),
            dict(args=(2, 3, 4)),
        ),
    ],
)
def test_type_error_raised_at_binding(
    func: typing.Callable,
    args: typing.Sequence,
    kwargs: typing.Mapping[str, typing.Any],
) -> None:
    """
    Check that when supplying bad arguments to checkers or when
    constructing checkers:
    - The raised `TypeError` pertains to the binding of the supplied
      arguments, instead of the generic "expected a single-arg
      callable" message in `~.logical_ops`
    - The traceback leads back to a frame in
      `inspect.Signature.{[_]bind|bind_partial}()`
    """
    with pytest.raises(TypeError) as info:
        func(*args, **kwargs)
    msg, *_ = info.value.args
    assert 'single-argument callable' not in msg
    frame_names = [
        frame.name for frame in traceback.StackSummary.extract(
            traceback.walk_tb(info.tb),
        )
    ]
    assert {'_bind', 'bind', 'bind_partial'} & set(frame_names)


def test_pass_bad_partial_to_logical() -> None:
    """
    Check that the `ValueError` raised in `inspect.signature()` when a
    malformed `functools.partial` object is passed to `~.logical` is
    propagated.
    """
    def func(x: int, y: int) -> bool:
        return False

    error_msg = 'partial object .* has incorrect arguments'
    with pytest.raises(ValueError, match=error_msg):
        logical(functools.partial(func, a=1))


@pytest.mark.skipif(
    not _WARN_BUT_ACCEPT_UNSUPPORTED_SIGS,
    reason='`@logical` now raises error against callables with missing sigs',
)
@pytest.mark.skipif(
    UFUNC_HAS_SIGNATURE,
    reason='ufuncs have become signature-compatible',
)
def test_promote_ufuncs_to_logical() -> None:
    """
    Check that despite `numpy.ufunc` objects' being incompatible with
    `inspect.signature()`, we can still construct `~.logical` objects
    from them.
    """
    catch_sig_warnings = pytest.warns(
        UserWarning, match='isnan.*: cannot read signature',
    )
    # Instantiation
    with catch_sig_warnings:
        isnan = logical(np.isnan)
        assert [isnan(float('nan')), isnan(1)] == [True, False]
    # Logical op with another logical
    with catch_sig_warnings:
        isnan_or_int = isnan | is_instance(int)
        assert isnan_or_int(1)
    # Logical op with naked function
    with catch_sig_warnings:
        isint_or_nan = is_instance(int) | np.isnan
        assert isint_or_nan(1)


def test_partialed_logical_no_double_call() -> None:
    """
    Check that we've fixed the gratuitous double call of the
    parametrized checker function when it returns a checker function
    instead of the result of a check.
    """
    @logical
    @functools.wraps(has_factor)
    def divided_by(*args, **kwargs):
        _invocations.append(None)
        return has_factor(*args, **kwargs)

    _invocations = []

    # Fully-parametrized invocations
    assert not divided_by(4, 5)
    assert divided_by(6, 3)
    assert len(_invocations) == 2
    # Partially-parametrized invocations
    is_even = divided_by(2)
    assert len(_invocations) == 3
    assert is_even.args == (2,)
    # Now that the partial-ed inner checker is returned, `divided_by()`
    # is no longer called
    assert is_even(4)
    assert not is_even(5)
    assert len(_invocations) == 3


@pytest.mark.parametrize('short_circuit', (True, False))
def test_logical_ops_preserves_short_circuit(
    short_circuit: bool,
) -> None:
    """
    Check whether (1) partial-ed calls and (2) logical operations
    preserve the `.short_circuit` attribute.
    """
    get_sc_version = operator.methodcaller('lazy' if short_circuit else 'eager')
    # Test parametrization
    takes_n_args = get_sc_version(takes_positional_args)
    assert takes_n_args.short_circuit == short_circuit
    assert takes_n_args(5).short_circuit == short_circuit
    assert takes_n_args((1, 2), agg=all).short_circuit == short_circuit
    # Test binary logical operations
    is_coll = get_sc_version(is_collection)
    is_even = has_factor(2)
    is_multiple_of_three = logical(
        has_factor, args=(3,), short_circuit=short_circuit,
    )
    for logical_func in is_coll, is_multiple_of_three:
        assert logical_func.short_circuit == short_circuit
        for op in operator.or_, operator.xor, operator.and_:
            # Forward methods
            assert op(logical_func, is_even).short_circuit == short_circuit
            # Reflected methods
            assert op(is_even, logical_func).short_circuit == short_circuit
        # Test unary logical operation (negation)
        assert (~logical_func).short_circuit == short_circuit


def test_logical_hashing():
    """
    Check that functions decorated with `@logical` and their partial-ed
    calls with hashable arguments can be hashed.
    """
    hash(takes_positional_args)  # No args
    hash(takes_positional_args((1, 2)))  # Positional args
    hash(  # Positional + keyword args
        takes_positional_args((1, 2, 3), agg=all),
    )
    with pytest.raises(TypeError, match=r'unhashable type in .*\.args'):
        # Unhashable if the arguments aren't
        hash(takes_positional_args([1, 2]))
    hash(is_collection | takes_positional_args(3))  # Logical op result
