"""
Decorator allowing checkers to be combined using logical operations.
"""
import abc
import ast
import enum
import functools
import inspect
import itertools
import numbers
import operator
import types
import typing
import warnings
from keyword import iskeyword
try:  # python3.8+
    from typing import Literal
except ImportError:
    from typing_extensions import Literal
try:  # python3.10+
    from typing import Concatenate, ParamSpec
except ImportError:
    from typing_extensions import Concatenate, ParamSpec
try:  # python3.11+
    from typing import Self
except ImportError:
    from typing_extensions import Self

from ._helper_decorators import replace_in_docs, partial_leading_args

__all__ = 'logical',

T = typing.TypeVar('T')
PS = ParamSpec('PS')

Checker = typing.Callable[[typing.Any], bool]
PartialCapableChecker = typing.Callable[
    Concatenate[typing.Any, PS], typing.Union[bool, Checker]
]
SyntaxTree = typing.TypeVar('SyntaxTree', bound=typing.Sequence)
RegularizedSyntaxTree = typing.TypeVar(
    'RegularizedSyntaxTree', bound=SyntaxTree,
)
_IntermediateSyntaxTree = typing.TypeVar(
    # All callables replaced by integer IDs
    '_IntermediateSyntaxTree', bound=typing.Union[tuple, int],
)
Keyword = typing.TypeVar('Keyword', bound=str)
Dunder = typing.TypeVar('Dunder', bound=Keyword)
LambdaDef = typing.TypeVar('LambdaDef', bound=str)

_LogicalChecker = typing.TypeVar('_LogicalChecker', bound='_LogicalChecker')
logical = typing.TypeVar(
    'logical', bound='logical',
)

_update_single_func_wrapper = functools.partial(
    functools.update_wrapper,
    updated=(),
)


class Op(enum.Enum):
    """
    Types of operations
    """
    NOT = '~'  # = invert
    AND = '&'
    XOR = '^'
    OR = '|'
    # Note: implementing these could be convenient, but messing with
    # equality also has ramifications like collection membership test
    # EQ = '=='
    # NE = '!='


CheckerOrTree = typing.Union[Checker, SyntaxTree]
UnaryOp = Literal[Op.NOT]
BinaryOp = Literal[Op.AND, Op.XOR, Op.OR]
UnaryNode = typing.Tuple[UnaryOp, Checker]
BinaryNode = typing.Tuple[BinaryOp, Checker, Checker]
ExpectChecker = typing.Union[Checker, typing.Any]
ExpectCheckerOrTree = typing.Union[CheckerOrTree, typing.Any]
ExpectUnaryOp = typing.Union[UnaryOp, typing.Any]
ExpectBinaryOp = typing.Union[BinaryOp, typing.Any]
ExpectUnaryNode = typing.Tuple[ExpectUnaryOp, ExpectCheckerOrTree]
ExpectBinaryNode = typing.Tuple[
    ExpectUnaryOp, ExpectCheckerOrTree, ExpectCheckerOrTree,
]
MaybeChecker = typing.Union[Checker, None]
MaybeUnaryOp = typing.Union[UnaryOp, None]
MaybeBinaryOp = typing.Union[UnaryOp, None]
MaybeUnaryNode = typing.Union[UnaryNode, None]
MaybeBinaryNode = typing.Union[BinaryNode, None]
MaybeCheckerOrTree = typing.Union[CheckerOrTree, None]

_OPERATIONS: typing.Dict[Op, typing.Callable[[bool, bool], bool]] = {
    Op.NOT: operator.not_,
    Op.AND: operator.and_,
    Op.XOR: operator.xor,
    Op.OR: operator.or_,
}

_SHORT_CIRCUITED_OP_REPRS: typing.Dict[Op, str] = {
    # No short-circuiting for `not`, but changed for visual congruence
    # with AND/OR
    Op.NOT: 'not ',
    Op.AND: 'and',
    Op.OR: 'or',
}

DEBUG = True
PARENT_MODULE = '.'.join((lambda: None).__module__.split('.')[:-1])
CHECKER_CLASS_NAME = 'logical.LogicHandler'

UNSUPPORTED_CALLABLES_VALUE_ERROR_MESSAGES = (
    # See `inspect._signature_from_callable()`
    # Note: these used to be in `tjekmate.callables`, but since we need
    # these and that module imports this one, we have to migrate it here
    'no signature found for', 'not supported by signature',
)
_WARN_BUT_ACCEPT_UNSUPPORTED_SIGS = True
_BOUND_METHOD_TYPES = (
    types.MethodType, types.MethodWrapperType, types.BuiltinMethodType,
)

# ************************* Helper functions ************************* #


def _get_type_fullname(t: type) -> str:
    """
    Example
    -------
    >>> import inspect
    >>> import types
    >>>
    >>> assert _get_type_fullname(str) == 'str'
    >>> assert _get_type_fullname(types.FunctionType) == 'function'
    >>> assert _get_type_fullname(
    ...     inspect.Signature,
    ... ) == 'inspect.Signature'
    """
    if t.__module__ == 'builtins':
        return t.__qualname__
    return '{0.__module__}.{0.__qualname__}'.format(t)


def _warn_unsupported_func(
    func: typing.Callable,
    error: ValueError,
    nargs: typing.Union[int, str] = 'single',
    stacklevel: int = 2,
) -> None:
    if isinstance(func, _BOUND_METHOD_TYPES):
        callable_type = 'method'
        callable_repr = '`{}.{}()`'.format(
            _get_type_fullname(type(func.__self__)), func.__name__,
        )
    else:
        callable_type = 'callable'
        callable_repr = repr(func)
    msg = (
        '{}: cannot read signature of the {} ({}), '
        'assume it to be a valid {}-argument function'
    ).format(callable_repr, callable_type, error, nargs)
    warnings.warn(msg, stacklevel=stacklevel)


def _takes_n_args(
    obj: typing.Any,
    n: numbers.Integral,
    *,
    indeterminate: typing.Union[bool, Literal['error']] = 'error',
    warn_indeterminate: bool = False,
) -> bool:
    """
    Examples
    --------
    General usage:

    >>> Foo = type('Foo', (), dict(bar=lambda self, x, y: None))
    >>> Bar = type('Foo', (), dict(baz=lambda self, x, y, z: None))
    >>> [  # doctest: +NORMALIZE_WHITESPACE
    ...     _takes_n_args(func, 3) for func in [
    ...         None,
    ...         lambda a, b: None,
    ...         lambda a, b, c: None,
    ...         lambda a, b, c, d: None,
    ...         lambda a, b, c, d=1: None,
    ...         lambda a, *b: None,
    ...         lambda a, b, c, *d: None,
    ...         lambda a, b, c, d, *e: None,
    ...         Foo.bar,
    ...         Foo().bar,
    ...         Bar.baz,
    ...         Bar().baz,
    ...     ]
    ... ]
    [False,
     False, True, False, True, True, True, False,
     True, False, False, True]

    Handling indeterminate forms (e.g. C-defined functions):

    >>> import inspect
    >>> import warnings
    >>>
    >>> try:
    ...     inspect.signature(str.count)
    ... except ValueError:  # Expected
    ...     pass
    ... else:
    ...     assert False
    >>> _takes_n_args(str.count, 2)  # Default: propagate the error
    Traceback (most recent call last):
      ...
    ValueError: no signature found ...
    >>> _takes_n_args(str.count, 2, indeterminate=False)
    False
    >>> _takes_n_args(str.count, 2, indeterminate=True)
    True
    >>> with warnings.catch_warnings():
    ...     warnings.simplefilter('error', UserWarning)
    ...     try:
    ...         assert _takes_n_args(
    ...             str.count, 2,
    ...             indeterminate=True, warn_indeterminate=True,
    ...         )
    ...     except UserWarning as w:
    ...         msg, *_ = w.args
    ...         assert 'cannot read signature' in msg
    ...     else:
    ...         assert False
    """
    if indeterminate == 'error':
        allowed_error_patterns = ()
        indeterminate_resolve_to = None
    else:
        # If we see these error messages, resolve to `True` or `False`
        allowed_error_patterns = UNSUPPORTED_CALLABLES_VALUE_ERROR_MESSAGES
        indeterminate_resolve_to = indeterminate
    try:
        sig = inspect.signature(obj)
    except TypeError:  # Not callable
        return False
    except ValueError as error:
        # `signature()` may raise `ValueError` for functions whose
        # signatures can't be parsed (C functions?)
        # Check explicitly for those messages because `ValueError` is
        # also raised by some random invalid stuff (e.g.
        # `functools.partial` object with disallowed arguments)
        msg, *_ = error.args
        if not any(
            pattern in msg for pattern in allowed_error_patterns
        ):
            raise
        if warn_indeterminate:
            _warn_unsupported_func(obj, error, nargs=n, stacklevel=3)
        return indeterminate_resolve_to
    try:
        sig.bind(*((None,) * n))
    except TypeError:  # Can't bind arguments
        return False
    return True


def _is_keyword(obj: typing.Any) -> bool:
    """
    Example
    -------
    >>> [_is_keyword(word) for word in '1 word with return _'.split()]
    [False, True, False, False, True]
    """
    return isinstance(obj, str) and obj.isidentifier() and not iskeyword(obj)


def _logical_bin_op_lc(
    name: Dunder, op: Op, inverted: bool,
) -> typing.Callable[[_LogicalChecker, Checker], _LogicalChecker]:
    """
    Helper function for incrementally constructing the syntax tree based
    on binary operations between a `_LogicalChecker` and an operand.
    """
    def bin_op(self: Self, other: typing.Union[Checker, LambdaDef]) -> Self:
        self_tree = self.tree
        if len(self_tree) == 1:  # Simple container of a callable
            self_tree, = self_tree
        if (
            isinstance(other, _LogicalChecker) and
            self.short_circuit == other.short_circuit
        ):  # Safe to combine
            other_tree = other.tree
            if len(other_tree) == 1:
                other_tree, = other_tree
        else:
            other_tree = other
        if type(other_tree) in (str,):  # Lambda defs -> callable
            # Note: guard against edge cases where a callable is a str
            # subclass
            try:
                other_tree = _lambda(  # Get names from calling frame
                    other_tree, frame=1,
                )
            except Exception:
                if DEBUG:
                    raise
                return NotImplemented
        try:
            return type(self)(
                (op,) + (self_tree, other_tree)[::step],
                short_circuit=self.short_circuit,
            )
        except TypeError:
            if DEBUG:
                raise
            return NotImplemented

    step = -1 if inverted else 1
    bin_op.__name__ = bin_op.__qualname__ = name
    return bin_op


def _logical_bin_op_lpc(
    name: Dunder, op: Op, inverted: bool,
) -> typing.Callable[[logical, Checker], _LogicalChecker]:
    """
    Helper function for binary operations between a `logical` (which
    should itself a valid checker) and an operand, deferring the
    handling of logic to `logical.LogicHandler`.
    """
    def bin_op(
        self: Self, other: typing.Union[Checker, LambdaDef],
    ) -> _LogicalChecker:
        logic_handler = type(self).LogicHandler(
            # Note: simplify the `.tree` of the resultant checker where
            # possible
            _get_base_callable_if_possible(self),
            short_circuit=self.short_circuit,
        )
        return operation(*((logic_handler, other)[::step]))

    operation = _OPERATIONS[op]
    step = -1 if inverted else 1
    bin_op.__name__ = bin_op.__qualname__ = name
    return bin_op


def _get_base_callable_if_possible(
    lg: logical,
) -> typing.Union[Checker, logical]:
    """
    Helper function to return the 'base callable' from a `@logical`
    checker if it is only a simple wrapper around a callable, without
    additionally supplying any arguments.
    This helps simplifying the `.tree` of the final callables.
    """
    if lg.args is None:
        return lg.source_checker
    return lg


def _walk(
    tree: CheckerOrTree,
    nullary_visitor: typing.Callable[[Checker], T],
    unary_visitor: typing.Callable[[Op, T], T],
    binary_visitor: typing.Union[
        typing.Callable[[Op, T, T], T],
        typing.Callable[
            [
                Op,
                CheckerOrTree,
                CheckerOrTree,
                typing.Callable[[CheckerOrTree], T],
            ],
            T
        ],
    ],
    *,
    default: typing.Any = None,
):
    """
    Walk the tree and return the result.
    """
    if _takes_n_args(binary_visitor, 4):
        defer_evaluation = True
    elif _takes_n_args(binary_visitor, 3):
        defer_evaluation = False
    else:
        raise AssertionError
    try:
        return _walk_inner(
            tree, nullary_visitor, unary_visitor, binary_visitor,
            defer_evaluation=defer_evaluation,
            _seen_object_ids=set(),
        )
    except Exception:
        if DEBUG:
            raise
        return default


@typing.overload
def _walk_inner(
    tree: CheckerOrTree,
    nullary_visitor: typing.Callable[[Checker], T],
    unary_visitor: typing.Callable[[Op, T], T],
    binary_visitor: typing.Callable[[Op, T, T], T],
    defer_evaluation: Literal[False],
    _seen_object_ids: typing.Set[int],
) -> T:
    ...


@typing.overload
def _walk_inner(
    tree: CheckerOrTree,
    nullary_visitor: typing.Callable[[Checker], T],
    unary_visitor: typing.Callable[[Op, T], T],
    binary_visitor: typing.Callable[
        # Note: add the walker() argument to allow for lazy evaluations
        [Op, CheckerOrTree, CheckerOrTree, typing.Callable[[CheckerOrTree], T]],
        T
    ],
    defer_evaluation: Literal[True],
    _seen_object_ids: typing.Set[int],
) -> T:
    ...


def _walk_inner(
    tree: CheckerOrTree,
    nullary_visitor: typing.Callable[[Checker], T],
    unary_visitor: typing.Callable[[Op, T], T],
    binary_visitor: typing.Union[
        typing.Callable[[Op, T, T], T],
        typing.Callable[
            [
                Op,
                CheckerOrTree,
                CheckerOrTree,
                typing.Callable[[CheckerOrTree], T],
            ],
            T
        ],
    ],
    defer_evaluation: bool,
    _seen_object_ids: typing.Set[int],
) -> T:
    """
    Parameters
    ----------
    tree
        Checker function or syntax(-like) tree to walk
    nullary_visitor()
        Callable converting a checker-function node into some result
    unary_visitor()
        Callable taking an unary operation and the result of the
        conversion of some node and converting them into some result
    binary_visitor()
        Callable taking a binary operation and (the results of the
        conversions of two nodes) and converting them into some result;
        see `defer_evaluation`
    defer_evaluation
        True
            `binary_visitor()` to take an additional positional argument
            (a walker function which converts nodes to some result);
            `binary_visitor()` will then be resposible for calling the
            walker on each binary-op node
        False
            `binary_visitor()` to take the nominal three arguments (an
            operation and the converted values of two nodes)
    _seen_object_ids
        Set keeping track of the objects seen (so that we don't have
        recursion)
    """
    walker = functools.partial(
        _walk_inner,
        nullary_visitor=nullary_visitor,
        unary_visitor=unary_visitor,
        binary_visitor=binary_visitor,
        defer_evaluation=defer_evaluation,
        _seen_object_ids=_seen_object_ids,
    )
    if defer_evaluation:
        def visit_binary(*args) -> T:
            return binary_visitor(*args, walker)

    else:
        def visit_binary(op: BinaryOp, *args) -> T:
            return binary_visitor(op, *(walker(subtree) for subtree in args))

    # Base case: the core callable
    if callable(tree) or not isinstance(tree, typing.Sequence):
        return nullary_visitor(tree)
    # Structured nodes
    id_tree = id(tree)
    if id_tree in _seen_object_ids:  # Recursion guard
        raise RecursionError(
            '{}: tree has a loop'.format(object.__repr__(tree))
        )
    _seen_object_ids.add(id_tree)
    if not (isinstance(tree, typing.Sequence) and len(tree) in range(1, 4)):
        raise TypeError(
            f'tree = {tree!r}: '
            'expected a callable or a sequence of length 1 to 3 (inclusive)'
        )
    nitems = len(tree)
    if nitems == 1:
        tree, = tree
        return walker(tree)
    elif nitems == 2:
        op, subtree = tree
        return unary_visitor(op, walker(subtree))
    else:
        op, subtree1, subtree2 = tree
        return visit_binary(op, subtree1, subtree2)


def _lambda(
    definition: LambdaDef,
    *,
    globals: typing.Optional[typing.Mapping[Keyword, typing.Any]] = None,
    locals: typing.Optional[typing.Mapping[Keyword, typing.Any]] = None,
    frame: typing.Optional[typing.Union[types.FrameType, int]] = None,
) -> types.FunctionType:
    """
    Convenience function for making lambda functions which retain its
    definition.

    Parameters
    ----------
    definition
        String definition of the lambda, e.g. 'lambda a, b: a + b'
    globals, locals
        Optional namespaces to be passed to `eval()`
    frame
        Optional specification for the frame to take the globals and
        locals from:
        Frame object
            Equivalent to `globals = frame.f_globals`, etc.
        Non-negative integer
            0 -> frame instantiating this class (e.g. where
            `my_lambda = _lambda('lambda a, b: a + b')` is executed,
            1 -> one frame back, etc.

    Examples
    --------
    Basic:

    >>> my_lambda = _lambda('lambda a, b: a + b')
    >>> my_lambda.definition
    'lambda a, b: a + b'
    >>> my_lambda(1, 2)
    3

    Propagation of namespaces:

    >>> another_lambda = _lambda(
    ...     'lambda a, b, c: my_lambda(a, b) * c',
    ...     frame=0,
    ... )
    >>> another_lambda(1, 2, 3)
    9
    >>> del my_lambda
    >>> another_lambda(1, 2, 3)
    Traceback (most recent call last):
      ...
    NameError: name 'my_lambda' ...
    >>> my_lambda = _lambda('lambda a, b: a / b')
    >>> another_lambda(1, 2, 3)
    1.5

    Protection against non-lambdas:

    >>> from typing import NoReturn
    ...
    >>> def bad_function() -> NoReturn:
    ...     raise RuntimeError('All your bases are belong to us')
    ...
    >>> not_a_lambda = 'bad_function'
    >>> lambda_w_trailing_statement = 'lambda: None; bad_function()'
    >>> lambda_w_trailing_expression = 'lambda: None, bad_function()'
    >>> for bad_definition in [
    ...     not_a_lambda,
    ...     lambda_w_trailing_statement,
    ...     lambda_w_trailing_expression,
    ... ]:
    ...     try:
    ...         _lambda(bad_definition)
    ...     except ValueError as e:
    ...         msg, = e.args
    ...         assert 'expected a string definition of a lambda' in msg
    ...     else:
    ...         raise AssertionError
    """
    ExceptionType = typing.TypeVar(
        'ExceptionType', bound=Exception,
    )

    def get_error(xc: typing.Type[ExceptionType] = ValueError) -> ExceptionType:
        return xc(
            f'definition = {definition!r}: '
            'expected a string definition of a lambda'
        )

    # Validate that the string is a lambda function
    if not isinstance(definition, str):
        raise get_error(TypeError)
    try:
        lambda_expr, = ast.parse(definition).body
    except ValueError:  # Too many/Not enough values to unpack
        raise get_error() from None
    if not (
        isinstance(lambda_expr, ast.Expr) and
        isinstance(lambda_expr.value, ast.Lambda)
    ):
        raise get_error()
    try:  # Clean up where possible
        definition = ast.unparse(lambda_expr)
    except AttributeError:  # ast.unparse() is python3.9+
        definition = definition.strip()
    # Resolve global/local names
    if isinstance(frame, int) and frame >= 0:
        frame_count = frame + 1
        frame = inspect.currentframe()
        for _ in range(frame_count):
            frame = frame.f_back
    if isinstance(frame, types.FrameType):
        if globals is None:
            globals = frame.f_globals
        if locals is None:
            locals = frame.f_locals
    func = eval(definition, globals, locals)
    func.definition = definition
    return func


@partial_leading_args(min_nargs=1)
def _wraps_excluding_attributes(
    wrapped: typing.Callable,
    exclude_from_update: typing.Collection[Keyword],
) -> typing.Callable[[typing.Callable[PS, T]], typing.Callable[PS, T]]:
    """
    `functools.wraps()` but with the specified `.__dict__` attributes
    excluded of the update.
    """
    def decorator(wrapper: typing.Callable[PS, T]):
        wrapper_dict_protected_names, wrapped_dict_forbidden_names = (
            {
                attr: value for attr, value in func.__dict__.items()
                if attr in exclude_from_update
            }
            for func in (wrapper, wrapped)
        )
        wrapper_dict_nonexistent_names = (
            set(exclude_from_update) - set(wrapper.__dict__)
        )
        names_to_restore = {
            attr: wrapper_dict_protected_names[attr]
            for attr in (
                set(wrapper_dict_protected_names)
                & set(wrapped_dict_forbidden_names)
            )
        }
        wrapper = functools.wraps(wrapped)(wrapper)
        wrapper.__dict__.update(names_to_restore)
        for attr in wrapper_dict_nonexistent_names:
            wrapper.__dict__.pop(attr, None)
        return wrapper

    return decorator


_impl_for_abstract_method = _wraps_excluding_attributes(
    ('__isabstractmethod__',),
)

# ************************** Helper classes ************************** #


class _CheckerBase(typing.Callable, metaclass=abc.ABCMeta):
    """
    Common API for `_LogicalChecker` and `logical`.
    """
    _slot: tuple
    __slots__ = '_slot', '__dict__'

    @abc.abstractmethod
    def __init__(
        self, checker: typing.Callable, *, short_circuit: bool = True,
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def __hash__(self) -> int:
        raise NotImplementedError

    def __eq__(self, other: typing.Any) -> bool:
        if isinstance(other, type(self)):
            return hash(self) == hash(other)
        return object.__eq__(self, other)

    def __getattr__(self, attr: Keyword) -> typing.Any:
        tree = self.tree
        if len(tree) == 1:
            return getattr(tree[0], attr)
        error = AttributeError(
            '{!r} object has no attribute {!r}'
            .format(type(self).__name__, attr)
        )
        for xc_attr, xc_value in dict(name=attr, obj=self).items():
            try:
                setattr(error, xc_attr, xc_value)
            except Exception:
                pass

    @typing.overload
    def __get__(
        self: Self, instance: T, owner: typing.Type[T],
    ) -> types.MethodType:
        ...

    @typing.overload
    def __get__(self: Self, instance: typing.Type[T], owner: None) -> Self:
        ...

    def __get__(
        self: Self,
        instance: typing.Union[T, typing.Type[T]],
        owner: typing.Union[typing.Type[T], None]
    ) -> typing.Union[types.MethodType, Self]:
        # Note: we don't really have a usecase for `.__get__()` because
        # the first positional argument to a checker is typically the
        # object being checked (i.e. `instance`);
        # but we need something to signal to `pydoc` that this object is
        # a method-like, and should therefore have its `help()`
        # formatted with its signature and docstring
        if owner is None:
            return self
        return types.MethodType(self, instance)

    @abc.abstractmethod
    def __call__(self, *args, **kwargs):
        raise NotImplementedError

    @abc.abstractmethod
    def __invert__(self: Self) -> _LogicalChecker:
        raise NotImplementedError

    @abc.abstractmethod
    def __and__(self: Self, other: typing.Union[Checker, LambdaDef]) -> Self:
        raise NotImplementedError

    @abc.abstractmethod
    def __rand__(self: Self, other: typing.Union[Checker, LambdaDef]) -> Self:
        raise NotImplementedError

    @abc.abstractmethod
    def __xor__(self: Self, other: typing.Union[Checker, LambdaDef]) -> Self:
        raise NotImplementedError

    @abc.abstractmethod
    def __rxor__(self: Self, other: typing.Union[Checker, LambdaDef]) -> Self:
        raise NotImplementedError

    @abc.abstractmethod
    def __or__(self: Self, other: typing.Union[Checker, LambdaDef]) -> Self:
        raise NotImplementedError

    @abc.abstractmethod
    def __ror__(self: Self, other: typing.Union[Checker, LambdaDef]) -> Self:
        raise NotImplementedError

    @abc.abstractmethod
    def lazy(self: Self, *, descend: bool = False) -> Self:
        """
        Parameters
        ----------
        descend
            Whether to descend into component logical checkers, also
            setting their `.short_circuit` values

        Return
        ------
        Copy with `short_circuit = True`

        See also
        --------
        `.eager()`
        """
        raise NotImplementedError

    @abc.abstractmethod
    def eager(self: Self, *, descend: bool = False) -> Self:
        """
        Parameters
        ----------
        descend
            Whether to descend into component logical checkers, also
            setting their `.short_circuit` values

        Return
        ------
        Copy with `short_circuit = False`

        See also
        --------
        `.lazy()`
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_truth_table(self, *, descend: bool = False) -> typing.Tuple[
        typing.List[Checker], typing.Dict[typing.Tuple[bool, ...], bool],
    ]:
        """
        Parameters
        ----------
        descend
            Whether to descend into component logical-checker instances,
            expanding their list of components

        Return
        ------
        Two-tuple of:
        - List of component checker callables
        - Dictionary with keys equal to the tuples of return values from
          the checkers, and values equal to the return value of the
          instance in such case
        """
        raise NotImplementedError

    @classmethod
    def new(
        cls: typing.Type[Self],
        checker: typing.Optional[typing.Callable] = None,
        **kwargs
    ) -> typing.Union[Self, typing.Callable[[Checker], Self]]:
        """
        Alternative, decorator-pattern-friendly constructor.

        Parameters
        ----------
        checker()
            Callable object (second argument to `cls()`)
        eager, lazy, short_circuit
            Optional boolean switches for whether to short-circuit
            AND/OR checks;
            `eager = True` is aliased to `short_circuit = False`,
            `lazy = True` is aliased to `short_circuit = True`, etc.;
            if more than one of these are provided and results in a
            conflict, a `ValueError` is raised
        **kwargs
            Passed to the default constructor `cls()`

        Return
        ------
        `checker()` not provided
            Decorator
        Else
            Instance wrapping `checker()`

        Examples
        --------
        >>> import inspect
        >>> from functools import partial
        >>> from numpy import array
        >>>
        >>> def is_odd(x: int) -> bool:
        ...     return bool(x % 2)
        ...
        >>> default_short_circuit = (
        ...     inspect.signature(logical.__init__)
        ...     .parameters['short_circuit'].default
        ... )

        Normal use:

        >>> is_odd_funcs = [
        ...     decorator(is_odd) for decorator in [
        ...         logical.new,
        ...         logical.new(),
        ...         logical.new(short_circuit=True),
        ...         logical.new(short_circuit=False),
        ...         logical.new(eager=True),
        ...         logical.new(eager=False),
        ...         logical.new(lazy=True),
        ...         logical.new(lazy=False),
        ...     ]
        ... ]
        >>> assert [
        ...     is_odd.short_circuit for is_odd in is_odd_funcs
        ... ] == [
        ...     default_short_circuit, default_short_circuit,
        ...     True, False, False, True, True, False,
        ... ]

        Input validation:

        >>> logical.new(lazy=array([1, 2]))(
        ...     is_odd  # doctest: +NORMALIZE_WHITESPACE
        ... )
        Traceback (most recent call last):
          ...
        TypeError: lazy = array([1, 2]):
          cannot be converted to a boolean
        >>> logical.new(  # This is an error due to contradictions
        ...     lazy=False, short_circuit=True,
        ... )(is_odd)
        Traceback (most recent call last):
          ...
        ValueError: lazy = False, short_circuit = True: conflicts ...
        >>> make_eager_logical_gratuitous_partial = partial(
        ...     logical.new, lazy=False,
        ... )
        >>> make_eager_logical_gratuitous_partial(  # This is also wrong
        ...     short_circuit=True,
        ... )(is_odd)
        Traceback (most recent call last):
          ...
        ValueError: lazy = False, short_circuit = True: conflicts ...
        >>> make_eager_logical = logical.new(lazy=False)
        >>> make_eager_logical(
        ...     # This however is okay because by supplying any of
        ...     # `eager`, `lazy`, and `short_circuit` the
        ...     # cached values (if any) of these three are invalidated
        ...     short_circuit=True,
        ... )(is_odd).short_circuit
        True
        """
        # Resolve short_circuit value
        short_circuit_source_names = 'eager', 'lazy', 'short_circuit'
        short_circuit_args = {
            arg_name: kwargs.pop(arg_name)
            for arg_name in short_circuit_source_names
            if arg_name in kwargs
        }
        resolved_short_circuit_values: typing.Dict[
            Literal[short_circuit_source_names], bool
        ] = {}
        for arg_name, value in short_circuit_args.items():
            try:
                value = not (not value)
            except Exception as e:
                raise TypeError(
                    '{} = {!r}: cannot be converted to a boolean'.format(
                        arg_name, value,
                    )
                ) from e
            to_short_circuit = functools.partial(
                operator.xor, (arg_name == 'eager'),
            )
            resolved_short_circuit_values[arg_name] = to_short_circuit(value)
        if len(set(resolved_short_circuit_values.values())) > 1:
            raise ValueError(
                '{}: conflicts among values provided'.format(
                    ', '.join(
                        f'{k} = {v!r}' for k, v in short_circuit_args.items()
                    )
                )
            )
        elif resolved_short_circuit_values:
            kwargs['short_circuit'], *_ = resolved_short_circuit_values.values()

        # Return decorated callable or decorator
        if checker is not None:
            return cls(checker, **kwargs)

        @functools.wraps(cls.new)
        def new_wrapper(checker=None, **kw):
            """
            We could have used `functools.partial`, but that does not
            allow for the invalidation of previously-supplied `kwargs`
            which are aliased to the same target argument;
            fix that here
            """
            previous_kwargs = dict(kwargs)
            if set(kw) & set(short_circuit_source_names):
                previous_kwargs = {
                    k: v for k, v in previous_kwargs.items()
                    if k not in short_circuit_source_names
                }
            return cls.new(checker, **{**previous_kwargs, **kw})

        return new_wrapper

    @property
    @abc.abstractmethod
    def short_circuit(self) -> bool:
        ...

    @property
    @abc.abstractmethod
    def tree(self) -> RegularizedSyntaxTree:
        ...


@replace_in_docs(
    _LogicalChecker=CHECKER_CLASS_NAME,
    _MODULE_=PARENT_MODULE,
)
class _LogicalChecker(_CheckerBase):
    """
    Helper decorator for checker callables which handles logical
    operations.

    Init parameters and attributes
    ------------------------------
    tree
        Single-argument checker callable or syntax tree;
        can also be the string definition for a lambda checker function,
        which is then converted into the former
    short_circuit
        Whether to short-circuit AND/OR checks;
        short-circuited checks behave like `and` and `or`;
        non-short-circuited (i.e. eager) checks behave like `&` and `|`;
        note that short-circuited-ness only affects `&` and `|`

    Methods
    -------
    .__call__()
        Apply the check
    .__invert__()
        Create an instance with an inverted check value
    .__[r]and__(), .__[r]xor__(), .__[r]or__()
        Create a new instance combining the check value and that of the
        other operand, which can be a checker, a lambda-definition
        string thereof, or (in `.__eq__` and `.__ne__`'s cases) a
        boolean
    .lazy() (resp. .eager())
        Create a copy with `short_circuit = True` (resp. `= False`)
    .get_truth_table()
        Get the truth table of the checker relative to that of the
        component checkers
    .regularize_tree() (class method)
        Regularize a syntax tree into the form it is stored at `.tree`
    .new() (class method)
        Alternative instantiator which also permits partial-like
        behavior (e.g. specifying only the `short_circuit` parameter and
        returning a decorator)

    Syntax tree
    -----------
    A valid syntax tree looks like any of:

    <single arg callable>
        Checker callable to be used to calculate the "tree value"
    (<single arg callable>,)
        Ditto
    (<special value NOT>, <subtree>)
        Value of subtree to be negated
    (<special value AND, XOR, or OR>, <subtree 1>, <subtree 2>)
        Values of subtrees to be combined with the specified logical
        operation

    Examples
    --------
    >>> @_LogicalChecker
    ... def is_odd(x: int) -> bool:
    ...     return bool(x % 2)
    ...
    >>> def is_lt_10(x: int) -> bool:
    ...     return x < 10
    ...

    Logical operations on other callables (and string definitions for
    lambdas!) are automatically converted into new
    `_MODULE_._LogicalChecker`
    instances:

    (Logical AND and OR)

    >>> my_checker = (  # Note: AND has precedence over OR in python
    ...     'lambda x: x % 3 == 2' | is_odd & is_lt_10
    ... )
    >>> [  # doctest: +NORMALIZE_WHITESPACE
    ...     my_checker(x) for x in range(25)
    ... ]
    [False, True, True, True, False, True, False, True, True, True,
     False, True, False,
     False, True, False,
     False, True, False,
     False, True, False,
     False, True, False]

    (Logical XOR and NOT)

    >>> new_checker = ~my_checker & (lambda x: x % 5 in (2, 3))
    >>> [  # doctest: +NORMALIZE_WHITESPACE
    ...     new_checker(x) for x in range(25)
    ... ]
    [False, False, False, False, False,
     False, False, False, False, False,
     False, False, True,
     True, False, False,
     False, False, True,
     False, False, False,
     True, False, False]

    Short-circuiting (i.e. lazy AND/OR evaluation) is on by default:

    >>> from typing import NoReturn
    >>>
    >>> def buggy_checker(x: int) -> NoReturn:
    ...     msg = '{} bug in `buggy_checker()`'.format(
    ...         'inevitable' if x % 2 else 'eager-only'
    ...     )
    ...     raise Exception(msg)
    ...
    >>> is_odd(0) and buggy_checker(0)  # No error because LHS false
    False
    >>> is_odd(0) & buggy_checker(0)  # Error because `&` is eager
    Traceback (most recent call last):
      ...
    Exception: eager-only bug in `buggy_checker()`
    >>> is_odd(1) and buggy_checker(1)
    Traceback (most recent call last):
      ...
    Exception: inevitable bug in `buggy_checker()`
    >>> (is_odd & buggy_checker)(0)
    False
    >>> (is_odd & buggy_checker).eager()(0)
    Traceback (most recent call last):
      ...
    Exception: eager-only bug in `buggy_checker()`
    >>> (is_odd & buggy_checker).eager().lazy()(
    ...     0,  # No error because we've made the operations lazy again
    ... )
    False
    >>> (is_odd & buggy_checker)(1)
    Traceback (most recent call last):
      ...
    Exception: inevitable bug in `buggy_checker()`

    Representation of the logic:

    >>> my_checker  # doctest: +NORMALIZE_WHITESPACE
    (lambda x: x % 3 == 2)
    or (<function is_odd at 0x...>
        and <function is_lt_10 at 0x...>)
    >>> my_checker.short_circuit
    True
    >>> my_checker.tree  # doctest: +NORMALIZE_WHITESPACE
    (<Op.OR: '|'>,
     <function <lambda> at 0x...>,
     (<Op.AND: '&'>,
      <function is_odd at 0x...>,
      <function is_lt_10 at 0x...>))
    >>> no_short_circuiting = _LogicalChecker(
    ...     my_checker.tree, short_circuit=False,
    ... )
    >>> no_short_circuiting
    (lambda x: x % 3 == 2) | (<function is_od...> & <function is_lt...>)

    Notes
    -----
    - Return values from the component checkers are implicitly converted
      into booleans except for the case that they are callable, where a
      `TypeError` is instead raised.
      This is to catch errors when working with partial-capable
      checkers (see the documentation of
      `_MODULE_.logical`).

    See also
    --------
    `_MODULE_.logical`
    """
    _slot: typing.Tuple[
        RegularizedSyntaxTree,  # .tree
        str,  # .__repr__() result
        bool,  # Whether to short-circuit the AND/OR checks
        typing.Dict[  # .get_truth_table() result
            bool,
            typing.Tuple[
                typing.Sequence[Checker],
                typing.Mapping[typing.Tuple[bool, ...], bool],
            ]
        ],
    ]

    def __init__(
        self,
        tree: typing.Union[SyntaxTree, Checker, LambdaDef],
        *,
        short_circuit: bool = True,
    ) -> None:
        if type(tree) in (str,):
            # Parse vanilla-string lambda definitions
            tree = _lambda(tree, frame=1)  # Names from calling frame
        reg_tree = self.regularize_tree(tree)
        if reg_tree:
            tree = reg_tree
        else:
            raise TypeError(
                f'tree = {tree!r}: expected a callable or a syntax tree'
            )
        short_circuit = not (not short_circuit)
        if (
            isinstance(tree, type(self)) and
            tree.short_circuit == short_circuit
        ):  # Safe to combine the trees
            tree = tree.tree
        repr_, _ = self._eval_tree_repr(tree, short_circuit)
        self._slot = tree, repr_, short_circuit, {}
        # Default metadata
        if len(self.tree) == 1:  # Holding a single callable
            func, = self.tree
            _update_single_func_wrapper(self, func)
        else:
            self.__name__ = self.__qualname__ = '<logical>'
        # Note: there seems to be some bug related to signatures that is
        # somehow triggered by the addition of `.__get__()`;
        # just assign one to circumvent that
        self.__signature__ = inspect.signature(self.__call__)

    def __repr__(self) -> str:
        return self._slot[1]

    def __hash__(self) -> int:
        return hash((type(self), self.tree, self.short_circuit))

    def __call__(self, obj: typing.Any) -> bool:
        return self._eval_tree_on(self.tree, obj, self.short_circuit)

    def __invert__(self: Self) -> Self:
        if len(self.tree) == 2:
            # assert self.tree[0] == Op.NOT
            return type(self)(self.tree[1])
        return type(self)((Op.NOT, self.tree), short_circuit=self.short_circuit)

    __and__ = _logical_bin_op_lc('__and__', Op.AND, False)
    __rand__ = _logical_bin_op_lc('__rand__', Op.AND, True)
    __xor__ = _logical_bin_op_lc('__xor__', Op.XOR, False)
    __rxor__ = _logical_bin_op_lc('__rxor__', Op.XOR, True)
    __or__ = _logical_bin_op_lc('__or__', Op.OR, False)
    __ror__ = _logical_bin_op_lc('__ror__', Op.OR, True)

    @_impl_for_abstract_method(_CheckerBase.lazy)
    def lazy(self, *_a, descend=False, **_k):
        tree = self.tree
        if descend:
            tree = self._descend_tree(tree)
        return type(self)(tree, short_circuit=True)

    @_impl_for_abstract_method(_CheckerBase.eager)
    def eager(self, *_a, descend=False, **_k):
        tree = self.tree
        if descend:
            tree = self._descend_tree(tree)
        return type(self)(tree, short_circuit=False)

    @_impl_for_abstract_method(_CheckerBase.get_truth_table)
    def get_truth_table(self, *, descend=False):
        """
        Parameters
        ----------
        descend
            Whether to descend into component logical checker instances,
            expanding their list of component checkers

        Return
        ------
        Two-tuple of:
        - List of component checker callables
        - Dictionary with keys equal to the tuples of return values from
          the checkers, and values equal to the return value of the
          instance in such case

        Example
        -------
        >>> @_LogicalChecker
        ... def foo(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def bar(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def baz(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> @_LogicalChecker.new(
        ...     eager=True,
        ... )
        ... def spam(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def ham(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> checker = (~(foo ^ bar) ^ (spam & ham)) | baz
        >>> base_checkers, table = checker.get_truth_table(
        ...     descend=False,
        ... )
        >>> base_checkers  # doctest: +NORMALIZE_WHITESPACE
        [<function foo at 0x...>,
         <function bar at 0x...>,
         <function spam at 0x...> & <function ham at 0x...>,
         <function baz at 0x...>]
        >>> dict(  # doctest: +NORMALIZE_WHITESPACE
        ...     sorted(table.items())
        ... )
        {(False, False, False, False): True,
         (False, False, False, True): True,
         (False, False, True, False): False,
         (False, False, True, True): True,
         (False, True, False, False): False,
         (False, True, False, True): True,
         (False, True, True, False): True,
         (False, True, True, True): True,
         (True, False, False, False): False,
         (True, False, False, True): True,
         (True, False, True, False): True,
         (True, False, True, True): True,
         (True, True, False, False): True,
         (True, True, False, True): True,
         (True, True, True, False): False,
         (True, True, True, True): True}
        >>> base_checkers, table = checker.get_truth_table(
        ...     descend=True,
        ... )
        >>> base_checkers  # doctest: +NORMALIZE_WHITESPACE
        [<function foo at 0x...>,
         <function bar at 0x...>,
         <function spam at 0x...>,
         <function ham at 0x...>,
         <function baz at 0x...>]
        >>> dict(  # doctest: +NORMALIZE_WHITESPACE
        ...     sorted(table.items())
        ... )
        {(False, False, False, False, False): True,
         (False, False, False, False, True): True,
         (False, False, False, True, False): True,
         (False, False, False, True, True): True,
         (False, False, True, False, False): True,
         (False, False, True, False, True): True,
         (False, False, True, True, False): False,
         (False, False, True, True, True): True,
         (False, True, False, False, False): False,
         (False, True, False, False, True): True,
         (False, True, False, True, False): False,
         (False, True, False, True, True): True,
         (False, True, True, False, False): False,
         (False, True, True, False, True): True,
         (False, True, True, True, False): True,
         (False, True, True, True, True): True,
         (True, False, False, False, False): False,
         (True, False, False, False, True): True,
         (True, False, False, True, False): False,
         (True, False, False, True, True): True,
         (True, False, True, False, False): False,
         (True, False, True, False, True): True,
         (True, False, True, True, False): True,
         (True, False, True, True, True): True,
         (True, True, False, False, False): True,
         (True, True, False, False, True): True,
         (True, True, False, True, False): True,
         (True, True, False, True, True): True,
         (True, True, True, False, False): True,
         (True, True, True, False, True): True,
         (True, True, True, True, False): False,
         (True, True, True, True, True): True}
        """
        IntermediateTruthTable = typing.Iterable[
            typing.Tuple[typing.Dict[int, bool], bool]
        ]

        def get_truth_table_callable(index: int) -> IntermediateTruthTable:
            for result in True, False:
                yield {index: result}, result

        def get_truth_table_unary(
            op: UnaryOp, table: IntermediateTruthTable,
        ) -> IntermediateTruthTable:
            # assert op == Op.NOT
            for inputs, output in table:
                yield inputs, (not output)

        def get_truth_table_binary(
            op: BinaryOp,
            table1: IntermediateTruthTable,
            table2: IntermediateTruthTable,
        ) -> IntermediateTruthTable:
            # assert op != Op.NOT
            operation = _OPERATIONS[op]
            for (inputs1, output1), (inputs2, output2) in itertools.product(
                table1, table2,
            ):
                # assert not set(inputs1) & set(inputs2)
                yield {**inputs1, **inputs2}, operation(output1, output2)

        try:
            checkers, truth_table = self._slot[3][descend]
        except KeyError:
            pass
        else:
            return list(checkers), dict(truth_table)

        checkers, intermediate_repr = (
            self._convert_tree_to_intermediate_expr(self.tree, descend=descend)
        )
        indices = range(len(checkers))
        truth_table: typing.Dict[typing.Tuple[bool, ...], bool] = {}
        for inputs, output in _walk(
            intermediate_repr,
            get_truth_table_callable,
            get_truth_table_unary,
            get_truth_table_binary,
        ):
            truth_table[tuple(inputs[i] for i in indices)] = output

        self._slot[3][descend] = (
            tuple(checkers), types.MappingProxyType(truth_table),
        )
        return checkers, truth_table

    @classmethod
    def regularize_tree(
        cls, tree: typing.Any, *, descend: bool = False,
    ) -> typing.Union[RegularizedSyntaxTree, None]:
        """
        Arguments
        ---------
        tree
            Checker callable or a syntax tree
        descend
            Whether to descend into component
            `_MODULE_._LogicalChecker`
            instances, grafting their `.tree`s onto the returned tree

        Return
        ------
        Regularized copy of a syntax tree if it is valid, or `None`
        otherwise
        """
        tree = cls._regularize_tree(tree)
        if not (tree is None or type(tree) in (tuple,)):
            # Tree is single callable -> make a vanilla tuple
            assert callable(tree)
            return tree,
        if tree is not None and descend:
            tree = cls._descend_tree(tree)
        return tree

    @staticmethod
    def _regularize_tree(
        tree: CheckerOrTree,
    ) -> typing.Union[Checker, RegularizedSyntaxTree, None]:
        def check_callable(checker: ExpectChecker) -> MaybeChecker:
            if _takes_n_args(
                checker, 1,
                **(
                    dict(indeterminate=True, warn_indeterminate=True)
                    if _WARN_BUT_ACCEPT_UNSUPPORTED_SIGS else
                    dict(indeterminate='error')
                ),
            ):
                return checker
            else:
                return None

        def check_unary_node(
            op: ExpectUnaryOp, subtree: ExpectCheckerOrTree,
        ) -> MaybeUnaryNode:
            if not (isinstance(op, Op) and op in (Op.NOT,)):
                return None
            if subtree is None:
                return None
            return op, subtree

        def check_binary_node(
            op: ExpectBinaryNode,
            subtree1: MaybeCheckerOrTree,
            subtree2: MaybeCheckerOrTree,
        ) -> MaybeBinaryNode:
            items: typing.List[typing.Union[MaybeBinaryNode, MaybeChecker]] = [
                op, subtree1, subtree2,
            ]
            if not (isinstance(op, Op) and op not in (Op.NOT,)):
                return None
            if any(item is None for item in items):
                return None
            return tuple(items)

        return _walk(tree, check_callable, check_unary_node, check_binary_node)

    @classmethod
    def _convert_tree_to_intermediate_expr(
        cls, tree: RegularizedSyntaxTree, descend: bool,
    ) -> typing.Tuple[typing.List[Checker], _IntermediateSyntaxTree]:
        """
        Example
        -------
        >>> @_LogicalChecker
        ... def foo(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def bar(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def baz(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> @_LogicalChecker.new(
        ...     eager=True,
        ... )
        ... def spam(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def ham(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> checker = (~(foo ^ bar) ^ (spam & ham)) | baz
        >>> checker._convert_tree_to_intermediate_expr(
        ...     checker.tree,
        ...     descend=False,  # doctest: +NORMALIZE_WHITESPACE
        ... )
        ([<function foo at 0x...>,
          <function bar at 0x...>,
          <function spam at 0x...> & <function ham at 0x...>,
          <function baz at 0x...>],
         (<Op.OR: '|'>,
          (<Op.XOR: '^'>, (<Op.NOT: '~'>, (<Op.XOR: '^'>, 0, 1)), 2),
          3))
        >>> checker._convert_tree_to_intermediate_expr(
        ...     checker.tree,
        ...     descend=True,  # doctest: +NORMALIZE_WHITESPACE
        ... )
        ([<function foo at 0x...>,
          <function bar at 0x...>,
          <function spam at 0x...>,
          <function ham at 0x...>,
          <function baz at 0x...>],
         (<Op.OR: '|'>,
          (<Op.XOR: '^'>,
           (<Op.NOT: '~'>, (<Op.XOR: '^'>, 0, 1)),
           (<Op.AND: '&'>, 2, 3)),
          4))
        """
        def apply_offset(
            intermediate_tree: _IntermediateSyntaxTree, offset: int,
        ) -> _IntermediateSyntaxTree:
            if isinstance(intermediate_tree, int):
                return intermediate_tree + offset
            if isinstance(intermediate_tree, Op):
                return intermediate_tree
            assert isinstance(intermediate_tree, tuple)
            return tuple(
                apply_offset(subtree, offset) for subtree in intermediate_tree
            )

        def index_checker(checker: Checker) -> _IntermediateSyntaxTree:
            offset = len(checkers)
            checkers.append(checker)
            return offset

        def index_unary_node(
            op: UnaryOp, subtree: _IntermediateSyntaxTree,
        ) -> _IntermediateSyntaxTree:
            # Note: callables in subtree should already have been
            # processsed by the nullnary visitor (index_checker()), so
            # this is essentially a no-op
            return op, subtree

        def index_binary_node(
            op: BinaryOp,
            subtree1: _IntermediateSyntaxTree,
            subtree2: _IntermediateSyntaxTree,
        ) -> _IntermediateSyntaxTree:
            # Note: ditto above
            return op, subtree1, subtree2

        # Walk the tree, gather the callables, and index them
        checkers: typing.List[typing.Callable] = []
        if descend:
            tree = cls._descend_tree(tree)
        intermediate_tree = _walk(
            tree, index_checker, index_unary_node, index_binary_node,
        )
        return checkers, intermediate_tree

    @staticmethod
    def _descend_tree(
        tree: typing.Union[Checker, RegularizedSyntaxTree],
    ) -> RegularizedSyntaxTree:
        """
        Get a tree where all the component
        `_MODULE_._LogicalChecker`
        objects are descended into.

        Example
        -------
        >>> @_LogicalChecker
        ... def foo(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def bar(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def baz(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> @_LogicalChecker.new(
        ...     short_circuit=False,
        ... )
        ... def spam(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> def ham(arg: typing.Any) -> bool:
        ...     ...
        ...
        >>> checker = (~(foo ^ bar) ^ (spam & ham)) | baz
        >>> checker.tree  # doctest: +NORMALIZE_WHITESPACE
        (<Op.OR: '|'>,
         (<Op.XOR: '^'>,
          (<Op.NOT: '~'>,
           (<Op.XOR: '^'>, <function foo ...>, <function bar ...>)),
          <function spam at 0x...> & <function ham at 0x...>),
         <function baz at 0x...>)
        >>> checker._descend_tree(  # doctest: +NORMALIZE_WHITESPACE
        ...     checker.tree,
        ... )
        (<Op.OR: '|'>,
         (<Op.XOR: '^'>,
          (<Op.NOT: '~'>,
           (<Op.XOR: '^'>, <function foo ...>, <function bar ...>)),
          (<Op.AND: '&'>, <function spam ...>, <function ham ...>)),
         <function baz at 0x...>)
        """
        CheckerOrRegTree = typing.Union[Checker, RegularizedSyntaxTree]

        def descend_callable(checker: Checker) -> CheckerOrRegTree:
            if isinstance(checker, _LogicalChecker):
                tree = checker.tree
                if len(tree) == 1:  # Single-callable tree
                    tree, = tree
                return tree
            return checker

        def descend_unary_node(
            op: UnaryOp, subtree: CheckerOrRegTree,
        ) -> RegularizedSyntaxTree:
            return op, subtree

        def descend_binary_node(
            op: BinaryOp,
            subtree1: CheckerOrRegTree,
            subtree2: CheckerOrRegTree,
        ) -> RegularizedSyntaxTree:
            return op, subtree1, subtree2

        tree = _walk(
            tree, descend_callable, descend_unary_node, descend_binary_node,
        )
        if type(tree) not in (tuple,):  # tree is single callable
            assert callable(tree)
            return tree,
        return tree

    @staticmethod
    def _eval_tree_on(
        tree: typing.Union[Checker, RegularizedSyntaxTree],
        obj: typing.Any,
        short_circuit: bool,
    ) -> bool:
        """
        Evaluate a syntax `tree` on `obj`.
        """
        def eval_callable(checker: Checker) -> bool:
            result = checker(obj)
            if callable(result):
                raise TypeError(
                    f'obj = {obj!r} -> {checker!r}(obj) = {result!r}: '
                    'expected a non-callable result convertible to a boolean'
                )
            return bool(result)

        def eval_unary_node(op: UnaryOp, subtree_result: bool) -> bool:
            # assert op == Op.NOT
            return not subtree_result

        def eval_binary_tree(
            op: BinaryOp,
            subtree1: CheckerOrTree,
            subtree2: CheckerOrTree,
            walker: typing.Callable[[CheckerOrTree], bool],
        ) -> bool:
            # assert op != Op.NOT
            if op in (Op.AND, Op.OR) and short_circuit:
                left_result = walker(subtree1)
                if left_result ^ (op == Op.AND):
                    return left_result
                return walker(subtree2)
            return _OPERATIONS[op](walker(subtree1), walker(subtree2))

        return _walk(tree, eval_callable, eval_unary_node, eval_binary_tree)

    @staticmethod
    def _eval_tree_repr(
        tree: CheckerOrTree, short_circuit: bool,
    ) -> typing.Tuple[str, bool]:  # Boolean flag for tree simplicity
        """
        Get a string representation of the syntax tree.
        """
        def get_lambda_def(func: typing.Callable) -> str:
            definition = func.definition
            if not (
                definition.startswith('lambda ') and ':' in definition
            ):
                raise AssertionError
            return f'({definition})'

        def get_nested_logical_repr(func: _LogicalChecker) -> str:
            # Note: this arises when we nest two logical objects with
            # different .short_circuit values
            if len(func.tree) > 1:
                # Any logical op (including 'not') requires bracketing
                return f'({func!r})'
            return repr(func)

        def get_obj_method_repr(func: typing.Union[_BOUND_METHOD_TYPES]) -> str:
            if not isinstance(func, _BOUND_METHOD_TYPES):
                raise TypeError
            return '{!r}.{}'.format(func.__self__, func.__name__)

        def eval_repr_callable(checker: Checker) -> typing.Tuple[str, bool]:
            for repr_getter in [
                get_lambda_def,
                get_nested_logical_repr,
                get_obj_method_repr,
                # Fallback
                repr,
                object.__repr__,
            ]:
                try:
                    return repr_getter(checker), True
                except Exception:
                    pass
            else:
                raise AssertionError

        def eval_repr_unary_node(
            op: UnaryOp, result_repr: typing.Tuple[str, bool],
        ) -> typing.Tuple[str, bool]:
            op_repr = (
                _SHORT_CIRCUITED_OP_REPRS if short_circuit else {}
            ).get(op, op.value)
            repr_, is_simple = result_repr
            if not is_simple:
                repr_ = f'({repr_})'
            return (op_repr + repr_), True

        def eval_repr_binary_node(
            op: UnaryOp,
            result_repr_1: typing.Tuple[str, bool],
            result_repr_2: typing.Tuple[str, bool],
        ) -> typing.Tuple[str, bool]:
            op_repr = (
                _SHORT_CIRCUITED_OP_REPRS if short_circuit else {}
            ).get(op, op.value)
            reprs: typing.List[str] = [
                repr_ if is_simple else f'({repr_})'
                for repr_, is_simple in (result_repr_1, result_repr_2)
            ]
            return '{1} {0} {2}'.format(op_repr, *reprs), False

        return _walk(
            tree,
            eval_repr_callable,
            eval_repr_unary_node,
            eval_repr_binary_node,
        )

    @property
    def tree(self) -> RegularizedSyntaxTree:
        return self._slot[0]

    @property
    def short_circuit(self) -> bool:
        return self._slot[2]


# ************************ Main wrapper class ************************ #


class logical(_CheckerBase):
    """
    Helper decorator wrapper for (partial-capable) checker callables
    which equips them with the logical operations NOT (`~`), AND (`&`),
    XOR (`^`), and OR (`|`).
    Logical operations on such a checker (with another checker except
    for NOT) returns a checker which transforms and/or combines the
    result(s) of the component check(s) with said operations.

    A checker callable is one that takes a single positional argument
    and returns a boolean value based on whether it satisfies the check
    (true) or not (false).

    A partial-capable checker callable returns either:
    - The boolean result of the check if all the required parameters are
      given, or
    - A checker callable remembering the supplied parameters if not.

    Non-attribute init parameters
    -----------------------------
    checker()
        (Partial-capable) Checker callable

    Init parameters and attributes
    ------------------------------
    args, kwargs
        Optional arguments passed to `checker()` so that a single-arg
        checker callable returning a boolean is returned
    short_circuit
        Whether to short-circuit AND/OR checks;
        short-circuited (i.e. lazy) checks behave like `and` and `or`;
        non-short-circuited (i.e. eager) checks behave like `&` and `|`;
        note that short-circuited-ness only affects `&` and `|`

    Other attributes
    ----------------
    checker()
        If `.args` or `.kwargs` were provided during instantiation, the
        checker callable returned by calling `.source_checker()` with
        them
    source_checker()
        The original `checker()` passed during instantiation
    tree
        Syntax tree (see below)
    LogicHandler (class attribute)
        Class to which the actual handling of logic is deferred to;
        logical operations on instances will return instances of this
        class

    Methods
    -------
    .__call__()
        Return either the boolean result of a check or a single-argument
        checker callable
    .__invert__(), .__[r]and__(), .__[r]xor__(), .__[r]or__()
        Return a single-argument checker callable (`.LogicHandler`
        instance) whose return value is the result of the resp. logical
        operation of the return values of the operends
    .lazy() (resp. .eager())
        Create a copy with `short_circuit = True` (resp. `= False`)
    .get_truth_table()
        Get the truth table of the checker relative to that of the
        component checkers (if any)
    .new() (class method)
        Alternative instantiator which also permits partial-like
        behavior (e.g. specifying only the `short_circuit` parameter and
        returning a decorator)

    Syntax tree
    -----------
    (<callable>,)
        Checker callable to be used to calculate the "tree value"
    (<special value NOT>, <subtree>)
        Value of subtree to be negated
    (<special value AND, XOR, or OR>, <subtree 1>, <subtree 2>)
        Values of subtrees to be combined with the specified logical
        operation

    Examples
    --------
    >>> import numbers
    >>> from functools import partial
    >>> from typing import overload, Any, Callable, NoReturn, Union

    Normal usage:

    >>> @overload
    ... def is_eq(obj: Any) -> Callable[[Any], bool]:
    ...     ...
    ...
    >>> @overload
    ... def is_eq(obj: Any, other: Any) -> bool:
    ...     ...
    ...
    >>> @logical  # = @logical.new(lazy=True)
    ... def is_eq(
    ...     obj: Any, *other,
    ... ) -> Union[Callable[[Any], bool], bool]:
    ...     if not other:
    ...         return partial(is_eq, obj)
    ...     other_obj, = other
    ...     return obj == other_obj
    ...
    >>> [is_eq(1, obj) for obj in [None, 1, 2]]
    [False, True, False]
    >>> [is_eq(1)(obj) for obj in [None, 1, 2]]
    [False, True, False]

    Metadata (see Notes though):

    >>> is_eq_1 = is_eq(1)
    >>> is_eq_1
    <function is_eq(1) at 0x...>
    >>> is_eq.args, is_eq.kwargs
    (None, None)
    >>> is_eq_1.args, is_eq_1.kwargs
    ((1,), mappingproxy({}))
    >>> is_eq_1.checker
    functools.partial(<function is_eq at 0x...>, 1)
    >>> is_eq_1.source_checker
    <function is_eq at 0x...>

    Logical operations:

    >>> @logical
    ... def is_integer(x: Any) -> bool:
    ...     return isinstance(x, numbers.Integral)
    ...
    >>> @logical
    ... def is_even(x: Any) -> bool:
    ...     if not is_integer(x):
    ...         return False
    ...     return not x % 2
    ...
    >>> is_odd = is_integer & ~is_even
    >>> either_falsy_or_odd = is_integer ^ 'lambda x: not x'
    >>> is_one_or_even_or_none = (
    ...     is_eq_1 | is_even | 'lambda x: x is None'
    ... )
    >>> is_odd
    <function is_integer at 0x...> and not <function is_even at 0x...>
    >>> either_falsy_or_odd
    <function is_integer at 0x...> ^ (lambda x: not x)
    >>> is_one_or_even_or_none  # doctest: +NORMALIZE_WHITESPACE
    (<function is_eq(1) at 0x...> or <function is_even at 0x...>)
     or (lambda x: x is None)
    >>> args = [None, *range(5)]
    >>> [is_odd(obj) for obj in args]
    [False, False, True, False, True, False]
    >>> [either_falsy_or_odd(obj) for obj in args]
    [True, False, True, True, True, True]
    >>> [is_one_or_even_or_none(obj) for obj in args]
    [True, True, True, True, False, True]

    Truth tables:

    >>> components, truth_table = (
    ...     is_one_or_even_or_none.get_truth_table()
    ... )
    >>> components  # doctest: +NORMALIZE_WHITESPACE
    [<function is_eq(1) at 0x...>,
     <function is_even at 0x...>,
     <function <lambda> at 0x...>]
    >>> dict(  # doctest: +NORMALIZE_WHITESPACE
    ...     sorted(truth_table.items())
    ... )
    {(False, False, False): False,
     (False, False, True): True,
     (False, True, False): True,
     (False, True, True): True,
     (True, False, False): True,
     (True, False, True): True,
     (True, True, False): True,
     (True, True, True): True}

    Eager vs. lazy checks:

    >>> def bad_checker(obj: Any) -> NoReturn:
    ...     if is_odd(obj):
    ...         msg = 'if only we were lazier'
    ...     else:
    ...         msg = "we'll get this error either way"
    ...     raise RuntimeError(msg)
    ...
    >>> def try_checker(checker: Callable[[Any], bool]) -> None:
    ...     for x in args:
    ...         try:
    ...             # This is okay for odd numbers, non-integers, etc.
    ...             # because we never got to the bad checker
    ...             print('{!r}: {}'.format(x, checker(x)))
    ...         except RuntimeError as e:
    ...             print('{!r}: {} (error)'.format(x, *e.args))
    ...
    >>> is_odd_lazy = is_odd | bad_checker
    >>> is_odd_eager = is_odd_lazy.eager()
    >>> is_odd_lazy  # doctest: +NORMALIZE_WHITESPACE
    (<function is_integer at 0x...> and not <function is_even at 0x...>)
     or <function bad_checker at 0x...>
    >>> is_odd_eager  # doctest: +NORMALIZE_WHITESPACE
    (<function is_integer at 0x...> & ~<function is_even at 0x...>)
     | <function bad_checker at 0x...>
    >>> try_checker(is_odd_lazy)
    None: we'll get this error either way (error)
    0: we'll get this error either way (error)
    1: True
    2: we'll get this error either way (error)
    3: True
    4: we'll get this error either way (error)
    >>> try_checker(is_odd_eager)
    None: we'll get this error either way (error)
    0: we'll get this error either way (error)
    1: if only we were lazier (error)
    2: we'll get this error either way (error)
    3: if only we were lazier (error)
    4: we'll get this error either way (error)

    Be careful when working with partial-capable checkers though:

    >>> is_ne_5_good = ~(is_eq(5))
    >>> is_ne_5_bad = (~is_eq)(5)  # doctest: +NORMALIZE_WHITESPACE
    Traceback (most recent call last):
      ...
    TypeError: obj = 5 -> <function is_eq at 0x...>(obj) = ...:
      expected a non-callable result convertible to a boolean
    >>> [is_ne_5_good(obj) for obj in [None, 1, 5]]
    [True, True, False]

    Notes
    -----
    - Return values are implicitly converted into booleans except for
      when they are callable, where:
      If the checker has `.args` or `.kwargs`:
          A `TypeError` is raised, since it is assumed that
          `.source_checker(*.args, **.kwargs)` should be a single-
          argument checker which returns a boolean
      Else, if `args` or `kwargs` is passed to the checker:
          A new checker based on `.source_checker(*args, **kwargs)`
          is returned, with the supplied arguments as its `.args` and
          `.kwargs`
      Else:
          A `TypeError` is raised, ditto the above reasoning.

    - Metadata fields (`.args`, `.kwargs`, `.checker`, and
      `.source_checker`) are not necessaily available to the result of
      logical operations on an instance because they are instances of
      `.LogicHandler` instead of this class.

    - `.args` and `.kwargs` are either:
      - BOTH `None` when neither is provided, or
      - A tuple and a keyword mapping if either is provided.

    - For compatibility (e.g. when automatically creating new checkers
      by logical operations with (presumably single-argument) C
      functions), it is permitted for the signature of `checker()` to be
      unavailable, BUT ONLY IF neither `.args` nor `.kwargs` is
      supplied;
      in such cases, the checker is assumed to be a single-argument
      callable which returns a boolean

    - No error handling is performed in `.__call__()`; the decorated
      function is responsible for that.
    """
    _slot: typing.Tuple[
        Checker,  # .checker
        PartialCapableChecker,  # .source_checker
        typing.Union[  # .args and .kwargs
            typing.Tuple[tuple, typing.Mapping[Keyword, typing.Any]],
            typing.Tuple[None, None],
        ],
        bool,  # .short_circuit
        typing.Union[
            int,  # hash()
            Keyword,  # (or the reason why it can't be hashed)
            None,  # (special value: hash not yet computed)
        ],
        inspect.Signature,  # .__signature__
    ]
    LogicHandler: typing.Type[_LogicalChecker] = _LogicalChecker

    def __init__(
        self,
        checker: typing.Union[PartialCapableChecker, LambdaDef],
        *,
        args: typing.Optional[typing.Sequence] = None,
        kwargs: typing.Optional[typing.Mapping[Keyword, typing.Any]] = None,
        short_circuit: bool = True,
    ) -> None:
        if type(checker) in (str,):
            checker = _lambda(checker, frame=1)
        if not callable(checker):
            raise TypeError(
                f'checker = {checker!r}: '
                'expected a callable or the string definition of a lambda'
            )
        no_args = args is None and kwargs is None
        if args is None:
            args = None if no_args else ()
        elif isinstance(args, typing.Sequence):
            args = tuple(args)
        else:
            raise TypeError(f'args = {args!r}: expected a sequence or `None`')
        if kwargs is None:
            kwargs = None if no_args else types.MappingProxyType({})
        elif (
            isinstance(kwargs, typing.Mapping) and
            all(_is_keyword(key) for key in kwargs)
        ):
            kwargs = types.MappingProxyType(dict(kwargs))
        else:
            raise TypeError(
                f'args = {args!r}: '
                'expected mapping of keyword arguments or `None`'
            )
        source_checker = checker
        if not no_args:
            checker = checker(*args, **kwargs)
            if not _takes_n_args(checker, 1):
                msg = (
                    'checker = {!r} -> checker{} = {!r}: '
                    'expected a single-argument (checker) callable to '
                    'be returned'
                ).format(
                    source_checker,
                    self._repr_call(*args, **kwargs),
                    checker,
                )
                raise NotACheckerError(msg, checker)
        short_circuit = not (not short_circuit)
        _update_single_func_wrapper(self, source_checker)
        # Precompute signature
        try:
            sig = inspect.signature(checker)
        except ValueError as error:
            if not no_args:
                raise
            # Note: if no signature can be recovered and no arguments
            # are given, assume the user knows what they're doing and
            # they're just passing a C-defined single-arg checker
            # function
            msg, *_ = error.args
            if (
                _WARN_BUT_ACCEPT_UNSUPPORTED_SIGS and
                any(
                    pattern in msg
                    for pattern in UNSUPPORTED_CALLABLES_VALUE_ERROR_MESSAGES
                )
            ):
                _warn_unsupported_func(checker, error)
                sig = inspect.Signature(
                    parameters=[
                        inspect.Parameter(
                            'x',
                            annotation=typing.Any,
                            kind=inspect.Parameter.POSITIONAL_OR_KEYWORD,
                        ),
                    ],
                    return_annotation=bool,
                )
            else:
                raise
        # Store everything in the slot
        self._slot = (
            checker,
            source_checker,
            (args, kwargs),
            short_circuit,
            sig,
            # Note: defer precomputation of the hash to `._stored_hash`
            # as an optimization
            None,
        )

    def __repr__(self) -> str:
        src_checker = self.source_checker
        if (
            isinstance(
                getattr(src_checker, 'definition', None), str,
            ) and
            src_checker.definition.startswith('lambda ') and
            ':' in src_checker.definition
        ):  # Lambdas from `_lambda()`
            func_type = 'function'
            func_name = f'({src_checker.definition})'
        elif isinstance(src_checker, _BOUND_METHOD_TYPES):  # Methods
            func_type = 'method'
            func_name = '{!r}.{}'.format(
                src_checker.__self__, src_checker.__name__,
            )
        else:  # Fallback
            func_type = _get_type_fullname(type(src_checker))
            if not isinstance(src_checker, types.FunctionType):
                func_type += ' object'
            try:
                func_name = self.__name__
            except AttributeError:
                func_name = '...'
        if self.args is None:
            args_repr = ''
        else:
            args_repr = self._repr_call(
                *(self.args or ()), **(self.kwargs or {}),
            )
        return '<{} {}{} at {:#x}>'.format(
            func_type, func_name, args_repr, id(self.checker),
        )

    def __hash__(self) -> int:
        hash_value = self._stored_hash
        if isinstance(hash_value, int):  # Precomputed
            return hash_value
        # If we're here, we know that the hash can't be computed and
        # `getattr(self, hash_value)` is to be blamed
        hash_obstacle = getattr(self, hash_value)
        raise TypeError(
            '{!r}: unhashable type in `.{} = {!r}`'.format(
                self, hash_value, hash_obstacle,
            ),
        )

    def __call__(self: Self, *args, **kwargs) -> typing.Union[bool, Self]:
        """
        Call signatures
        ---------------
        If `self.args` or `self.kwargs` provided at instantiation
            (checked_object) -> bool
        Else
            (*args, **kwargs) -> bool | logical
        """
        if self.args is None:
            # No pre-bound args -> may be a checker or a function which
            # returns a checker
            if not (args or kwargs):
                raise TypeError(f'{self!r}(): expected arguments')
            try:  # Partial-capable checker -> convert to pure checker
                return type(self)(
                    self.source_checker,
                    args=(args or None),
                    kwargs=(kwargs or None),
                    short_circuit=self.short_circuit,
                )
            except NotACheckerError as error:
                # Checker with sufficient arguments -> checker result
                result = error.value
        else:  # Pre-bound args -> use as a checker
            if kwargs or len(args) != 1:
                raise TypeError(
                    '{!r}{}: expected a single positional argument'.format(
                        self, self._repr_call(*args, **kwargs),
                    )
                )
            result = self.checker(*args)
        if callable(result):
            raise TypeError(
                (
                    '{!r}{} -> {!r}: '
                    'expected a non-callable result convertible to a boolean'
                ).format(
                    self, self._repr_call(*args, **kwargs), result,
                )
            )
        return bool(result)

    def __invert__(self: Self) -> _LogicalChecker:
        return ~type(self).LogicHandler(
            _get_base_callable_if_possible(self),
            short_circuit=self.short_circuit,
        )

    __and__ = _logical_bin_op_lpc('__and__', Op.AND, False)
    __rand__ = _logical_bin_op_lpc('__rand__', Op.AND, True)
    __xor__ = _logical_bin_op_lpc('__xor__', Op.XOR, False)
    __rxor__ = _logical_bin_op_lpc('__rxor__', Op.XOR, True)
    __or__ = _logical_bin_op_lpc('__or__', Op.OR, False)
    __ror__ = _logical_bin_op_lpc('__ror__', Op.OR, True)

    @_impl_for_abstract_method(_CheckerBase.lazy)
    def lazy(self, *_a, **_k):
        return type(self)(
            self.source_checker,
            args=self.args,
            kwargs=self.kwargs,
            short_circuit=True,
        )

    @_impl_for_abstract_method(_CheckerBase.eager)
    def eager(self, *_a, **_k):
        return type(self)(
            self.source_checker,
            args=self.args,
            kwargs=self.kwargs,
            short_circuit=False,
        )

    @_impl_for_abstract_method(_CheckerBase.get_truth_table)
    def get_truth_table(self, *_a, **_k):
        # Note: provided only for interface unity with `.LogicHandler`
        return [self.checker], {(True,): True, (False,): False}

    @staticmethod
    def _repr_call(*args, **kwargs) -> str:
        def _get_repr(obj: typing.Any) -> str:
            for func in repr, object.__repr__:
                try:
                    return func(obj)
                except Exception:
                    continue
            raise AssertionError

        return (
            '('
            + ', '.join(
                [_get_repr(a) for a in args]
                + [f'{k}={_get_repr(v)}' for k, v in kwargs.items()]
            )
            + ')'
        )

    @property
    def checker(self) -> typing.Callable:
        return self._slot[0]

    @property
    def source_checker(self) -> typing.Callable:
        return self._slot[1]

    @property
    def args(self) -> typing.Union[tuple, None]:
        return self._slot[2][0]

    @property
    def kwargs(self) -> typing.Union[typing.Mapping[Keyword, typing.Any], None]:
        return self._slot[2][1]

    @property
    def short_circuit(self) -> bool:
        return self._slot[3]

    @property
    def __signature__(self) -> inspect.Signature:
        return self._slot[4]

    @property
    def _stored_hash(self) -> typing.Union[int, Keyword]:
        def no_op(arg: T) -> T:
            return arg

        def kwargs_hash_helper(
            k: typing.Union[typing.Mapping[Keyword, T], None],
        ) -> typing.Union[typing.Tuple[typing.Tuple[Keyword, T], ...], None]:
            if k is None:
                return k
            return tuple(sorted(k.items()))

        slot_id = 5
        if self._slot[slot_id] is None:
            hash_items: typing.List[typing.Hashable] = [type(self)]
            for attr_name, attr, preprocessor in [
                ('source_checker', self.source_checker, no_op),
                ('args', self.args, no_op),
                ('kwargs', self.kwargs, kwargs_hash_helper),
                ('short_circuit', self.short_circuit, no_op),
            ]:
                attr = preprocessor(attr)
                try:
                    hash_items.append(hash(attr))
                except TypeError:
                    # Attribute can't be hashed -> remember the culprit
                    hash_value = attr_name
                    break
            else:  # All items hashable -> remember the hash
                hash_value = hash(tuple(hash_items))
            self._slot = (
                *self._slot[:slot_id], hash_value, *self._slot[slot_id + 1:],
            )
        return self._slot[slot_id]

    @property
    def tree(self) -> RegularizedSyntaxTree:
        # Note: provided only for interface unity with `.LogicHandler`
        return self.checker,


class NotACheckerError(TypeError):
    """
    Info-passing error class for when a checker callable is expected but
    not found.

    Attributes
    ----------
    value
        The value found instead of a checker callable
    """
    value: typing.Any

    def __init__(self, msg: str, value: typing.Any, *args, **kwargs) -> None:
        super().__init__(msg, value, *args, **kwargs)
        self.value = value
