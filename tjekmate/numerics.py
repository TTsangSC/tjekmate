"""
Checks for numeric and related types
"""
import ast
import itertools
import math
import numbers
import typing
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

from . import comparators
from .core import is_instance, is_subclass, is_keyword, is_sequence
from .decorators import type_checked
from .logical_ops import logical

__all__ = (
    'is_numeric_format_spec',
    'is_finite_real',
    'is_positive_integer', 'is_positive_real',
    'is_nonpositive_integer', 'is_nonpositive_real',
    'is_negative_integer', 'is_negative_real',
    'is_nonnegative_integer', 'is_nonnegative_real',
    'is_strict_boolean',
)

Identifier = typing.TypeVar('Identifier', bound=str)

NUMERIC_COMPARATORS: typing.Dict[
    Identifier, typing.Callable[[numbers.Real], bool]
] = dict(
    positive=comparators.gt(0),
    negative=comparators.lt(0),
    nonpositive=comparators.le(0),
    nonnegative=comparators.ge(0),
)


@logical
def is_numeric_format_spec(obj: typing.Any) -> bool:
    """
    Return
    ------
    Whether `obj` is a format specification string for a single number,
    e.g. '.2f'

    Example
    -------
    >>> [
    ...     is_numeric_format_spec(sp)
    ...     for sp in ('', '.2f', '2j', 'g', None, 0)
    ... ]
    [True, True, False, True, False, False]
    """
    if not isinstance(obj, str):
        return False
    try:
        f'{{:{obj}}}'.format(1.)
    except Exception:
        return False
    return True


@logical
def is_strict_boolean(obj: typing.Any) -> bool:
    """
    Return
    ------
    Whether `obj` is a boolean

    Notes
    -----
    This is designed to be stricter than equivalence;
    in python

    >>> 1 == True
    True

    and

    >>> 0 == False
    True

    but this function compares whether `obj` actually looks like a
    boolean:

    >>> from numpy import bool_
    >>>
    >>> objs = [None, 2, 1, 0, True, False, bool_(1 == 2)]
    >>> [obj in (True, False) for obj in objs]
    [False, False, True, True, True, True, True]
    >>> [is_strict_boolean(obj) for obj in objs]
    [False, False, False, False, True, True, True]
    """
    booleans = True, False
    bool_reprs = tuple(repr(boolean) for boolean in booleans)
    try:
        return (obj in booleans) and (repr(obj) in bool_reprs)
    except Exception:
        return False


@logical
def is_finite_real(x: typing.Any) -> bool:
    """
    Return
    ------
    Whether `x` is a finite real number

    Example
    -------
    >>> numbers = [float(n) for n in '1 1e9 -3.14 inf -inf nan'.split()]
    >>> non_numbers = [None, 'string']
    >>> [is_finite_real(x) for x in non_numbers + numbers]
    [False, False, True, True, True, False, False, False]
    """
    return isinstance(x, numbers.Real) and math.isfinite(x)


def _is_literal_evaluable(obj: typing.Any) -> bool:
    """
    Example
    -------
    >>> from numpy import array
    >>> class Foo:
    ...     pass
    ...
    >>> [
    ...     _is_literal_evaluable(obj)
    ...     for obj in [None, 1, 's', [1, 2], array([1, 2]), Foo(), str]
    ... ]
    [True, True, True, True, False, False, False]
    """
    try:
        result = (ast.literal_eval(repr(obj)) == obj)
        return not (not result)
    except Exception:
        return False


@type_checked(
    checkers=dict(
        comparison=comparators.is_in(NUMERIC_COMPARATORS),
        numeric_type=is_subclass(numbers.Real),
        number_name=(comparators.identical_to(None) | is_instance(str)),
        func_name=(comparators.identical_to(None) | is_keyword),
        test_values=(is_sequence | _is_literal_evaluable),
    ),
)
def _build_number_checker(
    comparison: Literal[tuple(NUMERIC_COMPARATORS)],
    numeric_type: typing.Type[numbers.Real],
    number_name: typing.Optional[str] = None,
    func_name: typing.Optional[Identifier] = None,
    test_values: typing.Sequence[typing.Any] = (1, 0, -1, 1.5, -1.5, None),
) -> typing.Callable[[typing.Any], bool]:
    """
    Build a checker for numbers.

    Parameters
    ----------
    comparison
        'positive', 'nonnegative', etc.
    numeric_type
        Subtype of `numbers.Real`
    number_name
        Optional name to call the number in the docstring;
        default is `numeric_type.__name__.lower() + ' number'`
    func_name
        Optional string idnetifier to name the function;
        default is `f'is_{comparison}_{numeric_type.__name__.lower()}'`
    test_values
        Sequence of values to test the function with;
        the `repr()` thereof must be able to be `ast.literal_eval()`-ed
        to an equilvaent object

    Example
    -------
    >>> import numpy as np
    >>>
    >>> check_positive_pyint = _build_number_checker('positive', int)
    >>> check_positive_pyint.__name__
    'is_positive_int'
    >>> [check_positive_pyint(value) for value in (1, 0, -1, 1.5, None)]
    [True, False, False, False, False]
    >>> check_positive_pyint(
    ...     # int64 not a python native integer -> False
    ...     np.int64(1),
    ... )
    False
    """
    def checker(x: typing.Any) -> bool:
        return isinstance(x, numeric_type) and comparator(x)

    comparator = NUMERIC_COMPARATORS[comparison]
    num_type_name = numeric_type.__name__.lower()
    if func_name is None:
        func_name = 'is_{}_{}'.format(comparison, num_type_name)
    if number_name is None:
        number_name = num_type_name + ' number'
    checker.__name__ = checker.__qualname__ = func_name
    checker.__doc__ = """
    Return
    ------
    Whether `obj` is a {comp} {num_name}

    Example
    -------
    >>> [{func_name}(x) for x in {values!r}]
    {results!r}
    """.format(
        comp=comparison.replace('non', 'non-'),
        num_name=number_name,
        func_name=func_name,
        values=test_values,
        results=[checker(value) for value in test_values],
    )
    return logical(checker)


(
    is_positive_integer, is_positive_real,
    is_nonpositive_integer, is_nonpositive_real,
    is_negative_integer, is_negative_real,
    is_nonnegative_integer, is_nonnegative_real,
) = (
    _build_number_checker(
        comparison, numeric_type,
        number_name=doc_name,
        func_name=f'is_{comparison}_{num_name}',
    )
    for comparison, (numeric_type, num_name, doc_name) in itertools.product(
        ['positive', 'nonpositive', 'negative', 'nonnegative'],
        [
            (numbers.Integral, 'integer', 'integer'),
            (numbers.Real, 'real', 'real number'),
        ],
    )
)
